/**
 * @file			gpio.c
 * @author			grli
 * @date			11.12.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include "inc/gpio.h"
#include "inc/gpioDefines.h"

/******************** defines *****************************/

/******************** prototypes **************************/

/******************** struct data type ********************/

/******************** private variables********************/
static bool *localPanic;
static bool *localCharging;
static LEDColour_t localColour;

/******************** private function ********************/

/******************** private callback ********************/
void panicCallback(uint8_t pin)
{
	if(gpio_getPanic())
		*localPanic = true;
	else
		*localPanic = false;
}

void chargingCallback(uint8_t pin)
{
	if(gpio_getCharging())
		*localCharging = true;
	else
		*localCharging = false;
}

/******************** global function *********************/
void gpio_init(bool *charging, bool *panic)
{
	localPanic = panic;
	localCharging = charging;

	/* Enable clock for GPIO by default */
	CMU_ClockEnable(cmuClock_GPIO, true);

	GPIOINT_Init();

	/* Configure BQ51050B_CHG_PIN as input. (PA9) */
	GPIO_PinModeSet(BQ51050B_CHG_PORT, BQ51050B_CHG_PIN, gpioModeInputPull, 1);
//	GPIO_IntConfig(BQ51050B_CHG_PORT, BQ51050B_CHG_PIN, true, true, true);
//	GPIOINT_CallbackRegister(BQ51050B_CHG_PIN, chargingCallback);

	/* Configure BQ51050B_CHG_PIN as input and enable interrupt. (PC10) */
	GPIO_PinModeSet(PANIC_BUTTON_PORT, PANIC_BUTTON_PIN, gpioModeInputPull, 0);
	GPIO_IntConfig(PANIC_BUTTON_PORT, PANIC_BUTTON_PIN, true, true, true);
	GPIOINT_CallbackRegister(PANIC_BUTTON_PIN, panicCallback);

	/* Configure LED_RED_PIN as push-pull. (PC11) */
	GPIO_PinModeSet(LED_RED_PORT, LED_RED_PIN, gpioModePushPull, 1);

	/* Configure LED_RED_PIN as push-pull. (PC12) */
	GPIO_PinModeSet(LED_GREEN_PORT, LED_GREEN_PIN, gpioModePushPull, 1);

	/* Configure ISL9230_CHG_PIN as input. (PE9) */
	GPIO_PinModeSet(ISL9230_CHG_PORT, ISL9230_CHG_PIN, gpioModeInputPull, 1);
	GPIO_IntConfig(ISL9230_CHG_PORT, ISL9230_CHG_PIN, true, true, true);
	GPIOINT_CallbackRegister(ISL9230_CHG_PIN, chargingCallback);


	panicCallback(PANIC_BUTTON_PIN);
//	chargingCallback(BQ51050B_CHG_PIN);
	chargingCallback(ISL9230_CHG_PIN);

	localColour = LED_OFF;
	return;
}

/******************** set function ************************/
void gpio_setLED(LEDColour_t colour)
{
	localColour = colour;

	switch(localColour)
	{
		case LED_RED:
			GPIO_PinOutClear(LED_RED_PORT, LED_RED_PIN);
			GPIO_PinOutSet(LED_GREEN_PORT, LED_GREEN_PIN);
			break;
		case LED_GREEN:
			GPIO_PinOutSet(LED_RED_PORT, LED_RED_PIN);
			GPIO_PinOutClear(LED_GREEN_PORT, LED_GREEN_PIN);
			break;
		case LED_YELLOW:
			GPIO_PinOutClear(LED_RED_PORT, LED_RED_PIN);
			GPIO_PinOutClear(LED_GREEN_PORT, LED_GREEN_PIN);
			break;
		case LED_OFF:
			GPIO_PinOutSet(LED_RED_PORT, LED_RED_PIN);
			GPIO_PinOutSet(LED_GREEN_PORT, LED_GREEN_PIN);
			break;
	}

	return;
}

void gpio_setBQ51050B_TS(void)
{
	GPIO_PinOutSet(BQ51050B_TS_CTRL_PORT, BQ51050B_TS_CTRL_PIN);
}

void gpio_clearBQ51050B_TS(void)
{
	GPIO_PinOutClear(BQ51050B_TS_CTRL_PORT, BQ51050B_TS_CTRL_PIN);
}

bool gpio_getPanic(void)
{
	if((GPIO_PinInGet(PANIC_BUTTON_PORT, PANIC_BUTTON_PIN) == 1))
		return true;

	return false;
}

bool gpio_getCharging(void)
{
	if((GPIO_PinInGet(ISL9230_CHG_PORT, ISL9230_CHG_PIN) == 0) || (GPIO_PinInGet(BQ51050B_CHG_PORT, BQ51050B_CHG_PIN) == 0))
		return true;

	return false;
}
