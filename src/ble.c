/**
 * @file			ble.c
 * @date			21.03.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include "inc/imu.h"
#include "inc/i2c.h"
#include "inc/ble.h"
#include "inc/gpio.h"
#include "inc/watchdog.h"

/******************** defines *****************************/

/******************** prototypes **************************/
static void ble_transmit(uint8_t *data, uint8_t dataLen);

static void ble_txCallback(UARTDRV_Handle_t handle, Ecode_t transferStatus, uint8_t *data, UARTDRV_Count_t transferCount);
static void ble_rxCallback(UARTDRV_Handle_t handle, Ecode_t transferStatus, uint8_t *data, UARTDRV_Count_t transferCount);

static callbackUART_t callbackRx = NULL;

/******************** struct data type ********************/
/**
 * struct for buffer
 */
typedef struct
{
	/*@{*/
	uint8_t write;		/**< write */
	uint8_t read;		/**< read */
	uint8_t size;		/**< size */
	bool overflow;		/**< overflow */
	struct{
		uint8_t bufLen;
		uint8_t buffer[TX_BUFFERSIZE];	/**< buffer */
	}data[EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS];
	/*@}*/
}Buffer_t;

/******************** private variables********************/
UARTDRV_HandleData_t bleHandleData;
UARTDRV_Handle_t bleHandle = &bleHandleData;

static Buffer_t txBuffer = {
		.write		= 0,
		.read		= 0,
		.overflow 	= false,
		.size		= EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS,
};

static bool initialized = false;
static uint8_t rxByte;
static uint8_t rxBuf[RX_BUFFERSIZE*2];
static uint16_t rxBufCounter = 0;

/******************** private function ********************/
void ble_transmit(uint8_t *data, uint8_t dataLen)
{
	if((txBuffer.overflow) && (txBuffer.write == txBuffer.read))
		return;

	txBuffer.data[txBuffer.write].bufLen = dataLen;
	memcpy(txBuffer.data[txBuffer.write].buffer, data, dataLen*sizeof(uint8_t) + 1);

	UARTDRV_Transmit(bleHandle, txBuffer.data[txBuffer.write].buffer, txBuffer.data[txBuffer.write].bufLen, ble_txCallback);

	txBuffer.write++;
	if(txBuffer.write >= txBuffer.size)
	{
		txBuffer.write = 0;
		txBuffer.overflow = true;
	}
}

/******************** callback function *******************/
void ble_txCallback(UARTDRV_Handle_t handle, Ecode_t transferStatus, uint8_t *data, UARTDRV_Count_t transferCount)
{
	(void)handle;
	(void)transferStatus;
	(void)data;
	(void)transferCount;

	if(transferStatus == ECODE_EMDRV_UARTDRV_OK)
	{
		if((!txBuffer.overflow) && (txBuffer.read == txBuffer.write))
			return;

		txBuffer.read++;
		if(txBuffer.read >= txBuffer.size)
		{
			txBuffer.read = 0;
			txBuffer.overflow = false;
		}
	}
}

void ble_rxCallback(UARTDRV_Handle_t handle, Ecode_t transferStatus, uint8_t *data, UARTDRV_Count_t transferCount)
{
	(void)handle;
	(void)transferStatus;
	(void)data;
	(void)transferCount;

	watchdog_feed();
	if(transferStatus == ECODE_EMDRV_UARTDRV_OK)
	{
		rxBuf[rxBufCounter++] = *data;
		if((*data == '\n') && (rxBufCounter > rxBuf[4]))
		{
			char* buf = strstr((char *)rxBuf, "AT+");
			volatile int8_t temp = (int8_t)((uint8_t *)buf - (uint8_t *)rxBuf);
			if(rxBufCounter >= rxBuf[temp + 4] + 5)
			{
			UARTDRV_FlowControlSet(bleHandle, uartdrvFlowControlOff);
			if(callbackRx != NULL) {
				callbackRx(rxBuf, temp);
			}
			rxBufCounter = 0;
			UARTDRV_FlowControlSet(bleHandle, uartdrvFlowControlOn);
			}
		}
	}

	/* RX the next byte */
	UARTDRV_Receive(bleHandle, &rxByte, 1, ble_rxCallback);
}

/******************** global function *********************/
BLE_RETURN ble_reset(bool reset)
{
	if(reset)
	{
		GPIO_PinModeSet(BLE_RESET_PORT, BLE_RESET_PIN, gpioModePushPull, 0);
		delay(500);
	}

	GPIO_PinModeSet(BLE_RESET_PORT, BLE_RESET_PIN, gpioModePushPull, 1);
	delay(500);

	return BLE_SUCCESS;
}

BLE_RETURN ble_init(callbackUART_t callback)
{
	initialized = false;
	callbackRx = callback;
	
	UARTDRV_InitUart_t initData;

	DEFINE_BUF_QUEUE(EMDRV_UARTDRV_MAX_CONCURRENT_RX_BUFS, rxBleBufferQueue);
	DEFINE_BUF_QUEUE(EMDRV_UARTDRV_MAX_CONCURRENT_TX_BUFS, txBleBufferQueue);

	initData.port = USART0;
	initData.baudRate = 38400;//9600;
	initData.portLocation = USART_ROUTE_LOCATION_LOC0;
	initData.stopBits = usartStopbits1;
	initData.parity = usartNoParity;
	initData.oversampling = usartOVS16;
	initData.mvdis = false;
	initData.fcType = uartdrvFlowControlHw;
	initData.ctsPort = BLE_USART_RTS_PORT;
	initData.ctsPin = BLE_USART_RTS_PIN;
	initData.rtsPort = BLE_USART_CTS_PORT;
	initData.rtsPin = BLE_USART_CTS_PIN;
	initData.rxQueue = (UARTDRV_Buffer_FifoQueue_t *)&rxBleBufferQueue;
	initData.txQueue = (UARTDRV_Buffer_FifoQueue_t *)&txBleBufferQueue;

	UARTDRV_InitUart(bleHandle, &initData);
	UARTDRV_Receive(bleHandle, &rxByte, 1, ble_rxCallback);

	initialized = true;
	delay(500);

	int16_t data = 0;
	ble_sendCmd(GET_CONNECTION, &data, 1);
	ble_sendCmd(GET_DEBUG_MODE, &data, 1);

	delay(500);

	return BLE_SUCCESS;
}

BLE_RETURN ble_sendCmd(BLE_CMD cmd, int16_t *data, uint8_t len)
{
	if(!initialized)
		return BLE_NO_INIT;

	static uint8_t tmp[32];
	strcpy((char *)tmp, "AT+C");
	tmp[4] = 2 + 2 * len;
	tmp[5] = cmd;
	uint8_t j = 6;
	for(uint8_t i = 0; i < len; i++) {
		tmp[j++] = (data[i] & 0xFF00) >> 8;
		tmp[j++] = (data[i] & 0x00FF);
	}
	tmp[4 + tmp[4]] = '\n';
	ble_transmit(tmp, tmp[4] + 5);

	return BLE_SUCCESS;
}

BLE_RETURN ble_sendIMUCmd(BLE_CMD cmd, uint32_t *time, float *data, uint8_t len)
{
	if(!initialized)
		return BLE_NO_INIT;

	static uint8_t tmp[32];
	strcpy((char *)tmp, "AT+C");
	tmp[4] = 2 + 4 + 4 * len;
	tmp[5] = cmd;
	tmp[6] = (*time & 0xFF000000) >> 24;
	tmp[7] = (*time & 0x00FF0000) >> 16;
	tmp[8] = (*time & 0x0000FF00) >> 8;
	tmp[9] = (*time & 0x000000FF);

	union {
		float float_variable;
		uint8_t temp_array[4];
	} u;

	uint8_t j = 10;
	for(uint8_t i = 0; i < len; i++) {
		u.float_variable = data[i];
		tmp[j++] = u.temp_array[0];
		tmp[j++] = u.temp_array[1];
		tmp[j++] = u.temp_array[2];
		tmp[j++] = u.temp_array[3];
	}
	tmp[4 + tmp[4]] = '\n';
	ble_transmit(tmp, tmp[4] + 5);

	return BLE_SUCCESS;
}

BLE_RETURN ble_returnUpdateCmd(uint8_t *cmd, int16_t ret)
{
	static uint8_t tmp[9];
	strcpy((char *)tmp, "AT+U");
	tmp[4] = 4;
	tmp[5] = *cmd;
	tmp[6] = (ret &0xFF00) >> 8;
	tmp[7] = (ret &0x00FF);
	tmp[8] = '\n';
	ble_transmit(tmp, 9);

	return BLE_SUCCESS;
}
