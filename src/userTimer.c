/**
 * @file			userTimer.c
 * @date			23.03.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include "inc/userTimer.h"

/******************** defines *****************************/

/******************** prototypes **************************/

/******************** struct data type ********************/

/******************** private variables********************/
static bool initialized = false;
volatile uint64_t msTicks; 			/* counts 1ms timeTicks */
volatile uint32_t time;
volatile uint32_t timeoutStartTicks;
volatile bool timeout = false;
volatile bool timeoutRun = false;

static void (*callbackSysTick)(void);

/******************** private function ********************/
void SysTick_Handler(void)
{
	msTicks++;       /* increment counter necessary in Delay()*/
	callbackSysTick();
}

/******************** global function *********************/
void sysTick_init(void (*userHandler)(void))
{
	/* Setup SysTick Timer for 1 msec interrupts  */
	if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000)) while (1) ;
	callbackSysTick = userHandler;

	initialized = true;
}

uint64_t time_InMs(void)
{
	return msTicks;
}

void delay(uint32_t dlyTicks)
{
	uint32_t curTicks;

	if(!initialized)
		return;

	curTicks = msTicks;
	while ((msTicks - curTicks) < dlyTicks)
		;
}

void timeoutSet(uint32_t timeoutTicks)
{
	if(!initialized)
		return;

	timeout = true;
	timeoutRun = true;
	timeoutStartTicks = msTicks;
	time = timeoutTicks;
}

void timeoutClear(void)
{
	if(!initialized)
		return;

	timeout = false;
	timeoutRun = false;
}

bool timeoutGet(void)
{
	if(!initialized)
		return false;

	if(((msTicks - timeoutStartTicks) < time) && timeoutRun)
		return false;

	timeoutRun = false;
	return timeout;
}
