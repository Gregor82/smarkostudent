#ifndef SWO_H_
#define SWO_H_
/*
 * swo.h
 *
 *  Created on: Nov 9, 2017
 *      Author: Simon
 */

#include "em_gpio.h"
#include "stdio.h"
#include "em_device.h"
#include "stdarg.h"
#include "string.h"

void swo_init(void);

void swo_print(const char * msg);

void swo_printf(const char * msg, ...);

#endif /* SWO_H_ */
