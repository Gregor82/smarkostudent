#ifndef DEBUG_H_
#define DEBUG_H_
/*
 * debug.h
 *
 *  Created on: Nov 9, 2017
 *      Author: Simon
 */

#include "stdarg.h"
#include "string.h"
#include "config.h"
#include "swo.h"

typedef enum {
	LOG_DEBUG,
	LOG_INFO,
	LOG_ERROR,
	LOG_MSG
} LOG_TYPE;

void debug_log(LOG_TYPE type, const char * msg);

void debug_logf(LOG_TYPE type, const char * msg, ...);

void log_debug(const char * msg, ...);

void log_info(const char * msg, ...);

void log_error(const char * msg, ...);

void log_msg(const char * msg, ...);

#endif /* DEBUG_H_ */
