/*
 * debug.c
 *
 *  Created on: Nov 9, 2017
 *      Author: Simon
 */

#include "debug.h"

void debug_log(LOG_TYPE type, const char * msg) {
	char logMsg[128];

#if (DEBUG)
	switch (type) {
		case LOG_DEBUG:
			sprintf(logMsg, "[DEBUG] %s", msg);
			break;
		case LOG_INFO:
			sprintf(logMsg, "[INFO] %s", msg);
			break;
		case LOG_ERROR:
			sprintf(logMsg, "[ERROR] %s", msg);
			break;
		case LOG_MSG:
			sprintf(logMsg, "%s", msg);
			break;
		default:
			break;
	}
#endif

#if (DEBUG_SWO)
	swo_print(logMsg);
#endif
}

void debug_logf(LOG_TYPE type, const char * msg, ...) {
#if (DEBUG)
	char formattedStr[128];
	va_list args;
	va_start(args, msg);
	vsprintf(formattedStr, msg, args);
	va_end(args);
	debug_log(type, formattedStr);
#endif
}

void log_debug(const char * msg, ...) {
#if (DEBUG)
	char formattedStr[128];
	va_list args;
	va_start(args, msg);
	vsprintf(formattedStr, msg, args);
	va_end(args);
	debug_log(LOG_DEBUG, formattedStr);
#endif
}

void log_info(const char * msg, ...) {
#if (DEBUG)
	char formattedStr[128];
	va_list args;
	va_start(args, msg);
	vsprintf(formattedStr, msg, args);
	va_end(args);
	debug_log(LOG_INFO, formattedStr);
#endif
}

void log_error(const char * msg, ...) {
#if (DEBUG)
	char formattedStr[128];
	va_list args;
	va_start(args, msg);
	vsprintf(formattedStr, msg, args);
	va_end(args);
	debug_log(LOG_ERROR, formattedStr);
#endif
}

void log_msg(const char * msg, ...) {
#if (DEBUG)
	char formattedStr[128];
	va_list args;
	va_start(args, msg);
	vsprintf(formattedStr, msg, args);
	va_end(args);
	debug_log(LOG_MSG, formattedStr);
#endif
}
