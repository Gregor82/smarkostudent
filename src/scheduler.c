/**
 * @file 			scheduler.c
 *
 * @createdBy		grli
 * @dateCreated		01.06.2019
 * $Revision: 29 $
 * $LastChangedBy: grli $
 * $LastChangedDate: 2019-06-27 11:06:20 +0200 (Thu, 27 Jun 2019) $
 *
 *
**/

/******************** include files ***********************/
#include "inc/scheduler.h"

/******************** defines *****************************/

/******************** prototypes **************************/

/******************** struct data type ********************/
struct task_s{
	uint32_t delayTime;
	uint32_t restTime;
	funcp func;
};

struct scheduler_s{
	struct task_s taskList[MAXTASK];
	uint8_t taskCount;
};

/******************** private variables********************/
static struct scheduler_s g_sched;

/******************** private function ********************/

/******************** interrupt function ******************/
void scheduler_interruptHandler(void)
{
	unsigned int i;

	for(i = 0; i < g_sched.taskCount; i++) {
		if (g_sched.taskList[i].restTime > 0) {
			--g_sched.taskList[i].restTime;
		}
		if ((g_sched.taskList[i].delayTime > 0) && (g_sched.taskList[i].restTime == 0)) {
			g_sched.taskList[i].restTime = g_sched.taskList[i].delayTime;
			g_sched.taskList[i].func();
		}
	}
}

/******************** global function *********************/
int  scheduler_init(void)
{
	unsigned int i;

	for(i = 0; i < MAXTASK; i++) {
		g_sched.taskList[i].delayTime = 0;
		g_sched.taskList[i].restTime = 0;
	}
	g_sched.taskCount = 0;

	return 0;
}

int scheduler_addTask(funcp function, uint32_t delaytime )
{
	if (g_sched.taskCount == MAXTASK) {
		return -1;
	}

	g_sched.taskList[g_sched.taskCount].delayTime = delaytime;
	g_sched.taskList[g_sched.taskCount].restTime  = delaytime;
	g_sched.taskList[g_sched.taskCount].func      = function;
	g_sched.taskCount++;

	return 0;
}

void scheduler_removeTask(funcp function)
{
	unsigned int i;
	unsigned int j;

	for(i = 0; i < g_sched.taskCount; i++){
		if (g_sched.taskList[i].func == function) {
			g_sched.taskList[i].restTime  = 0;
			g_sched.taskList[i].delayTime = 0;
			g_sched.taskList[i].func      = 0;
			--g_sched.taskCount;

			for (j = i; j < g_sched.taskCount; j++) {
				g_sched.taskList[j].restTime    = g_sched.taskList[j + 1].restTime;
				g_sched.taskList[j].delayTime   = g_sched.taskList[j + 1].delayTime;
				g_sched.taskList[j].func        = g_sched.taskList[j + 1].func ;

				g_sched.taskList[j + 1].restTime  = 0;
				g_sched.taskList[j + 1].delayTime = 0;
				g_sched.taskList[j + 1].func      = 0;
			}
		}
	}
}
