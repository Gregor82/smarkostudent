/**
 * @file			main.cpp
 * @date			15.08.2019
 *
 * @details
 */
/******************** include files ***********************/
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_rmu.h"
#include "em_i2c.h"
#include "em_gpio.h"

extern "C" {
#include "inc/i2c.h"
#include "inc/gpio.h"
#include "inc/ble.h"
#include "inc/imu.h"
#include "inc/stc3115.h"
#include "inc/watchdog.h"
#include "inc/userTimer.h"
#include "inc/scheduler.h"
#include "update/nvm_image.h"

#include "debug/debug.h"
}

/******************** defines *****************************/
#undef VERSION_TEXT
#undef VERSION_MAJ
#undef VERSION_MIN
#define VERSION_MAJ 0
#define VERSION_MIN 1
#define VERSION_TEXT "SmarKo Student Firmware v%d.%2d\nMCU: EFM32GG230F1024\n\n"

#define IMU_TIME 		25				// Zeit in ms, dass ist die Zeit die angbiet in welchen Abständen gemessen wird
#define IMU_ACCEL_TIME	IMU_TIME * 1	// gibt die Zeit in ms an in der die Accel Daten übertragen werden
#define IMU_GYRO_TIME 	IMU_TIME * 4	// gibt die Zeit in ms an in der die Gyro Daten übertragen werden
#define IMU_MAG_TIME 	IMU_TIME * 4	// gibt die Zeit in ms an in der die Mag Daten übertragen werden

#define LED_TOGGLE_TIME	250
#define LED_TIME		5000
#define STC3115_TIME 	30000

#define STC3115_BYTE	3
#define IMU_BYTE		4
#define IMAGE_0_BYTE	6
#define IMAGE_1_BYTE	7

/******************** struct data type ********************/

/******************** prototypes **************************/
static void SysTick_userHandler(void);

static void setAccelData (int16_t *accel);
static void changeAccelTime(uint16_t time);
static void changeGyroTime(uint16_t time);
static void changeMagTime(uint16_t time);
static void diasbleAllSensors(void);
static void startAllSensors(void);

static void task_ledOn(void);
static void task_ledOff(void);
static void task_ledToggle(void);
static void task_imu(void);
static void task_imuAccel(void);
static void task_imuGyro(void);
static void task_imuMag(void);
static void task_stc3115(void);

static void ble_rxHandler(uint8_t *rxBuf, uint16_t offset);

/******************** private variables********************/
static void *i2cHandle = NULL;
static void *imuHandle = NULL;
static void *stcHandle = NULL;

static bool connected = false;
static bool debug = false;
static bool update = false;
static bool restartAll = false;
static uint32_t numOfChunks = 0;

static uint8_t errorByte = 0;
static uint8_t statusByte = 0;
static int16_t stc3115Voltage = 0;
static int16_t stc3115Current = 0;
static int16_t stc3115Temperature = 0;
static int16_t stc3115Soc = 0;
static uint8_t updateBuffer[RX_BUFFERSIZE];

static uint32_t msTicks;
static uint32_t gyroTime;
static uint32_t accelTime;
static uint32_t magTime;

static bool get_ledOn = false;
static bool get_ledOff = false;
static bool get_ledToggle = false;
static bool get_imu = false;
static bool get_imuAccel = false;
static bool get_imuGyro = false;
static bool get_imuMag = false;
static bool get_stc3115 = false;

/******************** private function ********************/
void setAccelData (int16_t *accel)
{
	return;
}

void changeAccelTime(uint16_t time)
{
	scheduler_removeTask(task_imuAccel);
	get_imuAccel = false;
	scheduler_addTask(task_imuAccel, time);
#if (DEBUG && DEBUG_SWO)
	log_info("MPU9250 Gyro Time: %d\n", time);
#endif
}

void changeGyroTime(uint16_t time)
{
	scheduler_removeTask(task_imuGyro);
	get_imuGyro = false;
	scheduler_addTask(task_imuGyro, time);
#if (DEBUG && DEBUG_SWO)
	log_info("MPU9250 Accel Time: %d\n", time);
#endif
}

void changeMagTime(uint16_t time)
{
	scheduler_removeTask(task_imuMag);
	get_imuMag = false;
	scheduler_addTask(task_imuMag, time);
#if (DEBUG && DEBUG_SWO)
	log_info("MPU9250 Mag Time: %d\n", time);
#endif
}

void diasbleAllSensors(void)
{
	scheduler_removeTask(task_stc3115);
	scheduler_removeTask(task_imu);
	scheduler_removeTask(task_imuAccel);
	scheduler_removeTask(task_imuGyro);
	scheduler_removeTask(task_imuMag);
	scheduler_removeTask(task_ledOn);
	scheduler_removeTask(task_ledOff);
	stc3115_deInit(stcHandle);
	imu_deInit(imuHandle);

	get_ledOn = false;
	get_ledOff = false;
	get_imu = false;
	get_imuAccel = false;
	get_imuGyro = false;
	get_imuMag = false;
	get_stc3115 = false;
}

void startAllSensors(void)
{
	get_ledOn = false;
	get_ledOff = false;
	get_imu = false;
	get_imuAccel = false;
	get_imuGyro = false;
	get_imuMag = false;
	get_stc3115 = false;

	scheduler_addTask(task_ledOn, LED_TIME);

	if (stc3115_init(stcHandle) == STC3115_SUCCESS) {
		scheduler_addTask(task_stc3115, STC3115_TIME);
		statusByte |= (1 << STC3115_BYTE);
		stc3115_getValues(stcHandle, &stc3115Voltage, &stc3115Current, &stc3115Temperature, &stc3115Soc);
#if (DEBUG && DEBUG_SWO)
		log_info("STC3115 init success.\n");
		log_info("Battery SOC: %d%%\n", stc3115Soc);
		log_info("Battery Voltage:  %dmV\n", stc3115Voltage);
		log_info("Battery Current: %dmA\n", stc3115Current);
	} else {
		log_error("STC3115 init failed.\n");
#endif
	}

	watchdog_feed();

	if (imu_init(imuHandle, setAccelData) == IMU_SUCCESS) {
		scheduler_addTask(task_imu, IMU_TIME);
		scheduler_addTask(task_imuAccel, IMU_ACCEL_TIME);
		scheduler_addTask(task_imuGyro, IMU_GYRO_TIME);
		scheduler_addTask(task_imuMag, IMU_MAG_TIME);
		statusByte |= (1 << IMU_BYTE);
#if (DEBUG && DEBUG_SWO)
		log_info("IMU init success.\n");
	} else {
		log_error("IMU init failed.\n");
#endif
	}
}

/******************** tasks *******************************/
void task_ledOn(void)
{
	get_ledOn = true;
	scheduler_addTask(task_ledOff, 50);
}

void task_ledOff(void)
{
	get_ledOff = true;
	scheduler_removeTask(task_ledOff);
}

void task_ledToggle(void)
{
	if(get_ledToggle) {
		get_ledOn = true;
		get_ledToggle = false;
	} else {
		get_ledOff = true;
		get_ledToggle = true;
	}
}

void task_imu(void)
{
	get_imu = true;
}

void task_imuAccel(void)
{
	get_imuAccel = true;
}

void task_imuGyro(void)
{
	get_imuGyro = true;
}

void task_imuMag(void)
{
	get_imuMag = true;
}

void task_stc3115(void)
{
	get_stc3115 = true;
}

/******************** interrupt function ******************/
void SysTick_userHandler(void)
{
	msTicks++;
	scheduler_interruptHandler();
}

/******************** callback function *******************/
void ble_rxHandler(uint8_t *rxBuf, uint16_t offset)
{
	int16_t ret;
	uint16_t buf[3];
	uint16_t bufLen;
	uint16_t checksum_org = 0;

	switch(rxBuf[offset + 3])
	{
		case 'C':
			switch(rxBuf[offset + 5])
			{
				case SET_GYRO_TIME:
					changeGyroTime((rxBuf[offset + 6] << 8) | rxBuf[offset + 7]);
					break;
				case SET_GYRO_LIMIT:
					buf[0] = (rxBuf[offset + 6] << 8) | rxBuf[offset + 7];
					buf[1] = (rxBuf[offset + 8] << 8) | rxBuf[offset + 9];
					buf[2] = (rxBuf[offset + 10] << 8) | rxBuf[offset + 11];
					imu_setGyroLimit(imuHandle, buf);
					break;
				case SET_ACCEL_TIME:
					changeAccelTime((rxBuf[offset + 6] << 8) | rxBuf[offset + 7]);
					break;
				case SET_ACCEL_LIMIT:
					buf[0] = (rxBuf[offset + 6] << 8) | rxBuf[offset + 7];
					buf[1] = (rxBuf[offset + 8] << 8) | rxBuf[offset + 9];
					buf[2] = (rxBuf[offset + 10] << 8) | rxBuf[offset + 11];
					imu_setAccelLimit(imuHandle, buf);
					break;
				case SET_MAG_TIME:
					changeMagTime((rxBuf[offset + 6] << 8) | rxBuf[offset + 7]);
					break;
				case SET_MAG_LIMIT:
					buf[0] = (rxBuf[offset + 6] << 8) | rxBuf[offset + 7];
					buf[1] = (rxBuf[offset + 8] << 8) | rxBuf[offset + 9];
					buf[2] = (rxBuf[offset + 10] << 8) | rxBuf[offset + 11];
					imu_setMagLimit(imuHandle, buf);
					break;
				case GET_RESET:
					debug = false;
					connected = false;
					diasbleAllSensors();
					restartAll = true;
					//watchdog_reset();
					break;
				case GET_CONNECTION:
					if(rxBuf[offset + 7] == 0) {
						connected = false;
#if (DEBUG && DEBUG_SWO)
						log_info("Connection closed!\n");
#endif
					} else {
						connected = true;
#if (DEBUG && DEBUG_SWO)
						log_info("Connection open!\n");
#endif
					}
					break;
				case GET_DEBUG_MODE:
					if(rxBuf[offset + 7]  == 0) {
						debug = false;
					} else {
						debug = true;
					}
					break;
			}
			break;

		case 'U':
			switch(rxBuf[offset + 5])
			{
				case 'F':
					watchdog_feed();
					for(bufLen = 0; bufLen < rxBuf[offset + 4]; bufLen++) {
						updateBuffer[bufLen] = rxBuf[offset + 6 + bufLen];
					}

					//convert array of numbers to one number!
					for (bufLen = 0; bufLen < (rxBuf[offset + 4] -1); bufLen++) {
						checksum_org = 10 * checksum_org + (updateBuffer[bufLen]-48);
					}

					ret = image_Finish(checksum_org);
					ble_returnUpdateCmd(&rxBuf[offset + 5], ret);
#if (DEBUG && DEBUG_UPDATE)
					log_info("Done with OUTA!\n");
#endif
					update = false;
					restartAll = true;
					break;
				case 'U':
					watchdog_feed();
#if (DEBUG && DEBUG_UPDATE)
					log_msg("\n");
					log_info("receive c no.: %d, data length: %d\n", numOfChunks, rxBuf[4]);
#endif
					for(bufLen = 0; bufLen < rxBuf[offset + 4]-1; bufLen++) {
						updateBuffer[bufLen] = 0;
					}
					for(bufLen = 0; bufLen < rxBuf[offset + 4]-1; bufLen++) {
						updateBuffer[bufLen] = rxBuf[offset + 6 + bufLen];
#if (DEBUG && DEBUG_IMAGE)
					log_msg("%02x", updateBuffer[bufLen]);
						if(((bufLen % 16) == 0) && (bufLen > 0)) {
							log_msg("\n");
						}
#endif
					}
					if(numOfChunks < 480)
						ret = image_Write(updateBuffer, bufLen);
					else {
						numOfChunks++;
						ret = image_Write(updateBuffer, bufLen);
						numOfChunks--;
					}
					for(bufLen = 0; bufLen < (rxBuf[offset + 4] + 6); bufLen++) {
						rxBuf[bufLen] = 0;
					}
					if(ret < 0)
						ble_returnUpdateCmd(&rxBuf[offset + 5], ret);
					else
						ble_returnUpdateCmd(&rxBuf[offset + 5], numOfChunks++);
					//numOfChunks++;
					break;
				case 'S':
					watchdog_feed();
#if (DEBUG && DEBUG_UPDATE)
					log_info("OUTA started!\n");
#endif
					diasbleAllSensors();
					scheduler_addTask(task_ledToggle, LED_TOGGLE_TIME);
					//watchdog_stop();
					update = true;
					numOfChunks = 0;
					NVMHAL_Init();
					ret = image_Init();
					if(ret == 0)
						ret = image_Current();

					ble_returnUpdateCmd(&rxBuf[offset + 5], ret);

#if (DEBUG && DEBUG_UPDATE)
					log_info("Image init finished!\n");
#endif
					break;
			}
			break;
	}
	return;
}

/******************** global function *********************/
int main(void)
{
	bool panic = false;
	bool charging = false;

	float gyro[3];
	float accel[3];
	float mag[3];

	connected = false;
	debug = false;
	update = false;
	restartAll = false;

	errorByte = 0;
	statusByte = 0;

	stc3115Voltage = 0;
	stc3115Current = 0;
	stc3115Temperature = 0;
	stc3115Soc = 0;

	msTicks = 0;
	gyroTime = 0;
	accelTime = 0;
	magTime = 0;

	/* Chip errata */
	CHIP_Init();

	CMU ->CTRL = (CMU ->CTRL & ~_CMU_CTRL_HFXOMODE_MASK) | CMU_CTRL_HFXOMODE_XTAL;
	CMU ->CTRL = (CMU ->CTRL & ~_CMU_CTRL_HFXOBOOST_MASK) | CMU_CTRL_HFXOBOOST_50PCENT;
	SystemHFXOClockSet(48000000);

	CMU_OscillatorEnable(cmuOsc_HFXO, true, true);

	/* Enable peripheral clock */
	CMU_ClockEnable(cmuClock_HFPER, true);

	// call the Handler of interrupt function
	sysTick_init(SysTick_userHandler);

	gpio_init(&charging, &panic);
	gpio_setLED(LED_YELLOW);

	NVMHAL_Init();

	uint8_t *address;
	NVMHAL_Read(CURRENT_IMAGE_ADDR, &address, sizeof(address));

	if(address == IMAGE_0_BINARY_ADDR){
		errorByte |= (1 << IMAGE_0_BYTE);
		statusByte |= (1 << IMAGE_0_BYTE);
		gpio_setLED(LED_GREEN);
	}
	else if(address == IMAGE_1_BINARY_ADDR){
		errorByte |= (1 << IMAGE_1_BYTE);
		statusByte |= (1 << IMAGE_1_BYTE);
		gpio_setLED(LED_RED);
	}

	NVMHAL_DeInit();

	I2CConfig_t i2cConfig;
	i2cConfig.i2c = I2C_IDX0;
	i2cConfig.sclPort = I2C0_SCL_PORT;
	i2cConfig.sclPin = I2C0_SCL_PIN;
	i2cConfig.sdaPort = I2C0_SDA_PORT;
	i2cConfig.sdaPin = I2C0_SDA_PIN;
	i2cConfig.portLocation = 0;
	i2cConfig.i2cMaxFreq = I2C_FREQ_STANDARD_MAX;

	i2cHandle = i2c_createHandle(&i2cConfig);
	i2c_init(i2cHandle, NULL, true);

#if (DEBUG && DEBUG_SWO)
	swo_init();				// enable SWO-pin for debug data -> needed for energy profiling

	log_info(VERSION_TEXT, VERSION_MAJ, VERSION_MIN);
#endif

	ble_init(ble_rxHandler);

	gpio_setLED(LED_YELLOW);
	scheduler_init();

	watchdog_init();
	watchdog_start();

	STC3115Battery_t battery = BATTERY_DEFAULT;
	battery.CAPACITY = 240;
	battery.RINT = 0;
	stcHandle = stc3115_createHandle(i2cHandle, STC3115_ADDRESS, &battery);
	imuHandle = imu_createHandle(i2cHandle, IMU_ADDRESS, IMU_ADDRESS_MAGNET);
	startAllSensors();

#if (DEBUG && DEBUG_SWO)
	uint8_t txBuf = 0;
	uint8_t rxBuf = 0;
	uint8_t i2cAddress = 0;
	for(int i = 0; i < 0x7F; i++) {
		i2cAddress = i << 1;
		if(i2c_writeRead(i2cHandle, i2cAddress, &txBuf, 1, &rxBuf, 1) == I2C_SUCCESS) {
			log_info("I2C device found at 0x%02X address.\n", i2cAddress);
		}
	}
#endif

	gpio_setLED(LED_OFF);
	restartAll = false;

	/* Infinite loop */
	while (1) {
		watchdog_feed();

		if(restartAll) {
			restartAll = false;
			delay(1000);
			NVIC_SystemReset();
		}

		if(get_ledOn) {
			get_ledOn = false;
			gpio_setLED(LED_GREEN);
		}

		if(get_ledOff)  {
			get_ledOff = false;
			gpio_setLED(LED_OFF);
		}

		if(get_imu) {
			get_imu = false;
			imu_measure(imuHandle, &msTicks);
			if(get_imuAccel)  {
				get_imuAccel = false;
				if(imu_getAccelData(imuHandle, accel, &accelTime) == IMU_SUCCESS) {
					get_imuAccel = true;
					ble_sendIMUCmd(SET_ACCEL, &accelTime, accel, 3);
					//ble_sendCmd(SET_ACCEL, accel, 3);
#if (DEBUG && DEBUG_SWO)
				log_info("IMU Accel: %d - %d / %d / %d\n", (uint32_t)accelTime, accel[0], accel[1], accel[2]);
#endif
				}
			}

			if(get_imuGyro) {
				get_imuGyro = false;
				if(imu_getGyroData(imuHandle, gyro, &gyroTime) == IMU_SUCCESS){
					get_imuGyro = false;
					ble_sendIMUCmd(SET_GYRO, &gyroTime, gyro, 3);
					//ble_sendCmd(SET_GYRO, gyro, 3);
#if (DEBUG && DEBUG_SWO)
					log_info("IMU Gyro: %d - %d / %d / %d\n", (uint32_t)gyroTime, gyro[0], gyro[1], gyro[2]);
#endif
				}
			}

			if(get_imuMag) {
				get_imuMag = false;
				if(imu_getMagData(imuHandle, mag, &magTime) == IMU_SUCCESS) {
					get_imuMag = false;
					ble_sendIMUCmd(SET_MAG, &magTime, mag, 3);
					//ble_sendCmd(SET_MAG, mag, 3);
#if (DEBUG && DEBUG_SWO)
					log_info("IMU Mag: %d - %d / %d / %d\n", (uint32_t)magTime, mag[0], mag[1], mag[2]);
#endif
				}
			}
		}

		if(get_stc3115) {
			get_stc3115 = false;
			stc3115_measure(stcHandle);
			stc3115_getValues(stcHandle, &stc3115Voltage, &stc3115Current, &stc3115Temperature, &stc3115Soc);
			ble_sendCmd(SET_BATTERY_SOC, &stc3115Soc, 1);
			ble_sendCmd(SET_BATTERY_VOLTAGE, &stc3115Voltage, 1);
			ble_sendCmd(SET_BATTERY_CURRENT, &stc3115Current, 1);
			ble_sendCmd(SET_BATTERY_TEMP, &stc3115Temperature, 1);
#if (DEBUG && DEBUG_SWO)
			log_info("Battery SOC: %d%%\n", stc3115Soc);
			log_info("Battery Voltage:  %dmV\n", stc3115Voltage);
			log_info("Battery Current: %dmA\n", stc3115Current);
#endif
		}
	}
}
