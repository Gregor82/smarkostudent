/**
 * @file			watchdog.c
 * @date			11.08.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include "inc/watchdog.h"

/******************** defines *****************************/

/******************** prototypes **************************/

/******************** struct data type ********************/

/******************** private variables********************/
static bool initialized = false;
static bool reset = false;

/******************** private function ********************/

/******************** global function *********************/
void watchdog_init(void)
{
	initialized = false;
	/* Defining the watchdog initialization data */
	WDOG_Init_TypeDef init;

	init.enable     = false,               /* Start watchdog when init done */
	init.debugRun   = false,              /* WDOG not counting during debug halt */
	init.em2Run     = true,               /* WDOG counting when in EM2 */
	init.em3Run     = true,               /* WDOG counting when in EM3 */
	init.em4Block   = false,              /* EM4 can be entered */
	init.swoscBlock = false,              /* Do not block disabling LFRCO/LFXO in CMU */
	init.lock       = false,              /* Do not lock WDOG configuration (if locked, reset needed to unlock) */
	init.clkSel     = wdogClkSelULFRCO,   /* Select 1kHZ WDOG oscillator */
	init.perSel     = wdogPeriod_2k,      /* Set the watchdog period to 2049 clock periods (ie ~2 seconds)*/

	WDOG_Init(&init);

	reset = false;
	initialized = true;
}

void watchdog_start(void)
{
	if(!initialized)
		return;

	reset = false;
	WDOG_Enable(true);
}

void watchdog_stop(void)
{
	if(!initialized)
		return;

	reset = false;
	WDOG_Enable(false);
}

void watchdog_feed(void)
{
	if(!initialized)
		return;

	if(initialized && reset)
		return;

	WDOG_Feed();
}

void watchdog_reset(void)
{
	if(!initialized)
		return;

	reset = true;
	delay(5000);
}
