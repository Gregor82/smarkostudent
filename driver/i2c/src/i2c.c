/**
 * @file 			i2c.c
 *
 * @createdBy		grli
 * @dateCreated		13.08.2019
 * $Revision: 29 $
 * $LastChangedBy: grli $
 * $LastChangedDate: 2019-06-27 11:06:20 +0200 (Thu, 27 Jun 2019) $
 *
 *
**/


/******************** include files ***********************/
#include "inc/i2c.h"

#include "em_i2c.h"
#include "em_cmu.h"
#include "em_gpio.h"

#ifdef UNIT_TESTS

#else

#endif


/******************** defines *****************************/
#if defined(I2C0)
uint8_t i2c0_rxBuffer[I2C_BUFFER_SIZE];
uint8_t i2c0_rxBufferIndex;
#endif

#if defined(I2C1)
uint8_t i2c1_rxBuffer[I2C_BUFFER_SIZE];
uint8_t i2c1_rxBufferIndex;
#endif
/******************** prototypes **************************/
static i2cSlaveCallback_fptr_t _i2cSlaveCallback = NULL;

/******************** struct data type ********************/
struct I2CHandle_s {
	I2CConfig_t *i2cConfig;

	I2C_TypeDef *port;
	bool master;
	bool initialized;
	bool busy;
};

/******************** private variables********************/

/******************** private function ********************/

/******************** interrupt function ******************/
#if defined(I2C0)
void I2C0_IRQHandler(void)
{
	int status;
	status = I2C0->IF;

	if (status & I2C_IF_ADDR)
	{
		/* Address Match */
		I2C_IntClear(I2C0, I2C_IFC_ADDR);
		if (I2C0->RXDATA & 0x01)
		{
			/* Mater read, enable slave Tx */
			I2C0->IEN |= I2C_IEN_ACK;
			I2C0->TXDATA = 0xFF;
		}
		else
		{
			/* Mater write, enable slave Rx */
			I2C0->IEN |= I2C_IEN_RXDATAV;
		}
	}
	else if (status & I2C_IF_RXDATAV)
	{
		/* Data received */
		i2c0_rxBuffer[i2c0_rxBufferIndex] = I2C0->RXDATA;
		i2c0_rxBufferIndex++;
	}
	else if (status & I2C_IF_ACK)
	{
		/* ACK received, send next data */
		I2C_IntClear(I2C0, I2C_IFC_ACK);
		I2C0->TXDATA = 0xFF;
	}
	else
	{
		/* Stop received, Tx/Rx is ended */
		I2C_IntClear(I2C0, I2C_IFC_SSTOP);
		I2C0->IEN &= ~(I2C_IEN_ACK + I2C_IEN_RXDATAV);
		i2c0_rxBufferIndex = 0;
	}
}
#endif

#if defined( I2C1 )
void I2C1_IRQHandler(void)
{
	int status;
	status = I2C1->IF;

	if (status & I2C_IF_ADDR)
	{
		/* Address Match */
		I2C_IntClear(I2C1, I2C_IFC_ADDR);
		if (I2C1->RXDATA & 0x01)
		{
			/* Mater read, enable slave Tx */
			I2C1->IEN |= I2C_IEN_ACK;
			I2C1->TXDATA = 0xFF;
		}
		else
		{
			/* Mater write, enable slave Rx */
			I2C1->IEN |= I2C_IEN_RXDATAV;
		}
	}
	else if (status & I2C_IF_RXDATAV)
	{
		/* Data received */
		i2c1_rxBuffer[i2c1_rxBufferIndex] = I2C1->RXDATA;
		i2c1_rxBufferIndex++;
	}
	else if (status & I2C_IF_ACK)
	{
		/* ACK received, send next data */
		I2C_IntClear(I2C1, I2C_IFC_ACK);
		I2C1->TXDATA = 0xFF;
	}
	else
	{
		/* Stop received, Tx/Rx is ended */
		I2C_IntClear(I2C1, I2C_IFC_SSTOP);
		I2C1->IEN &= ~(I2C_IEN_ACK + I2C_IEN_RXDATAV);
		i2c1_rxBufferIndex = 0;
	}
}
#endif

/******************** global function *********************/
void *i2c_createHandle(I2CConfig_t *i2cConfig)
{
	struct I2CHandle_s *i2cHandle = calloc(1, sizeof(struct I2CHandle_s));

	i2cHandle->i2cConfig = i2cConfig;

	switch(i2cConfig->i2c)
	{
		case I2C_IDX0:
#if defined(I2C0)
			i2cHandle->port = I2C0;
#endif
			break;
		case I2C_IDX1:
#if defined(I2C1)
			i2cHandle->port = I2C1;
#endif
			break;
	}

	i2cHandle->master = false;
	i2cHandle->busy = false;
	i2cHandle->initialized = false;

	return i2cHandle;
}

I2C_RETURN i2c_destroyHandle(void **handle)
{
	i2c_deInit(*handle);

	free(*handle);
	*handle = NULL;

	return I2C_SUCCESS;
}

I2C_RETURN i2c_init(void *handle, i2cSlaveCallback_fptr_t callback, bool master)
{
	if(!handle) {
		return I2C_NO_HANDLER;
	}

	if(!master && (callback == NULL)) {
		return I2C_NO_INIT;
	}

	struct I2CHandle_s *i2cHandle =  (struct I2CHandle_s *)handle;
	i2cHandle->master = master;

	CMU_Clock_TypeDef i2cClock;
	I2C_Init_TypeDef i2cInit;

	/* Output value must be set to 1 to not drive lines low. Set
	SCL first, to ensure it is high before changing SDA. */
	GPIO_PinModeSet(i2cHandle->i2cConfig->sclPort, i2cHandle->i2cConfig->sclPin, gpioModeWiredAndPullUp, 1);
	GPIO_PinModeSet(i2cHandle->i2cConfig->sdaPort, i2cHandle->i2cConfig->sdaPin, gpioModeWiredAndPullUp, 1);

	//CMU_ClockEnable(cmuClock_HFPER, true);

	/* Select I2C peripheral clock */
	if (false)
	{
#if defined(I2C0)
	}
	else if (i2cHandle->port == I2C0)
	{
		i2cClock = cmuClock_I2C0;
#endif
#if defined(I2C1)
	}
	else if (i2cHandle->port == I2C1)
	{
		i2cClock = cmuClock_I2C1;
#endif
	}
	else
	{
		/* I2C clock is not defined */
		return I2C_USAGE_FAULT;
	}
	CMU_ClockEnable(i2cClock, true);

	if(i2cHandle->master) {
		/* In some situations, after a reset during an I2C transfer, the slave
		device may be left in an unknown state. Send 9 clock pulses to
		set slave in a defined state. */
		uint8_t i;
		for (i = 0; i < 9; i++)
		{
			GPIO_PinOutSet(i2cHandle->i2cConfig->sclPort, i2cHandle->i2cConfig->sclPin);
			GPIO_PinOutClear(i2cHandle->i2cConfig->sclPort, i2cHandle->i2cConfig->sclPin);
		}

		/* Enable pins and set location */
	#if defined (_I2C_ROUTEPEN_MASK)
		i2cHandle->i2cConfig->port->ROUTEPEN  = I2C_ROUTEPEN_SDAPEN | I2C_ROUTEPEN_SCLPEN;
		i2cHandle->i2cConfig->port->ROUTELOC0 = (i2cConfig->portLocationSda << _I2C_ROUTELOC0_SDALOC_SHIFT) |
									(i2cConfig->portLocationScl << _I2C_ROUTELOC0_SCLLOC_SHIFT);
	#else
		i2cHandle->port->ROUTE = I2C_ROUTE_SDAPEN | I2C_ROUTE_SCLPEN |
					  (i2cHandle->i2cConfig->portLocation << _I2C_ROUTE_LOCATION_SHIFT);
	#endif

		/* Set emlib init parameters */
		i2cInit.enable = true;
		i2cInit.master = true; /* master mode only */
		i2cInit.freq = i2cHandle->i2cConfig->i2cMaxFreq;
		i2cInit.refFreq = 0;
		i2cInit.clhr = i2cClockHLRStandard;

		I2C_Init(i2cHandle->port, &i2cInit);
		_i2cSlaveCallback = NULL;
	} else {
//		/* Enable pins at location 1 */
//		I2C1->ROUTE = I2C_ROUTE_SDAPEN | I2C_ROUTE_SCLPEN | I2C_ROUTE_LOCATION_LOC1;
//
//		/* Initializing the I2C */
//		i2cInit.enable = 1;
//		i2cInit.master = 0;
//		i2cInit.freq = I2C_FREQ_STANDARD_MAX;
//		i2cInit.clhr = i2cClockHLRStandard;
//		I2C_Init(I2C1, &i2cInit);
//
//		/* Setting the status flags and index */
//	//	i2c_rxInProgress = false;
//	//	i2c_startTx = false;
//		i2c_rxBufferIndex = 0;
//
//		/* Setting up to enable slave mode */
//		I2C1->SADDR = I2C_ADDRESS;
//		I2C1->CTRL |= I2C_CTRL_SLAVE | I2C_CTRL_AUTOACK | I2C_CTRL_AUTOSN;
//
//
//		I2C_IntClear(I2C1, I2C_IEN_ADDR | I2C_IEN_RXDATAV | I2C_IEN_SSTOP);
//		I2C_IntEnable(I2C1, I2C_IEN_ADDR | I2C_IEN_RXDATAV | I2C_IEN_SSTOP);
//		NVIC_ClearPendingIRQ(I2C1_IRQn);
//		NVIC_EnableIRQ(I2C1_IRQn);
		_i2cSlaveCallback = callback;
	}

	i2cHandle->busy = false;
	i2cHandle->initialized = true;
	return I2C_SUCCESS;
}

I2C_RETURN i2c_deInit(void *handle)
{
	if(!handle) {
		return I2C_NO_HANDLER;
	}

	struct I2CHandle_s *i2cHandle = (struct I2CHandle_s *)handle;
	i2cHandle->initialized = false;

	return I2C_SW_FAULT;
}

I2C_RETURN i2c_getInitialized(void *handle)
{
	if (!handle) {
		return I2C_NO_HANDLER;
	}

	if(!((struct I2CHandle_s*)handle)->initialized) {
		return I2C_NO_INIT;
	}

	return I2C_SUCCESS;
}

I2C_RETURN i2c_write(void *handle, uint8_t addr, uint8_t* txBuf, uint32_t txSize)
{
	if (!handle) {
		return I2C_NO_HANDLER;
	}

	struct I2CHandle_s *i2cHandle =  (struct I2CHandle_s *)handle;

	if(!i2cHandle->initialized) {
		return I2C_NO_INIT;
	}

	if(i2cHandle->busy) {
		return I2C_TRANSFER_IN_PROGRESS;
	}

	i2cHandle->busy = true;

	//I2C_RETURN ret = I2C_SUCCESS;
	I2C_TransferSeq_TypeDef i2cTransfer;

	i2cTransfer.addr  = addr;
	i2cTransfer.flags = I2C_FLAG_WRITE;
	i2cTransfer.buf[0].data   = txBuf;
	i2cTransfer.buf[0].len    = txSize;

	/* Sending data */
	I2C_TransferReturn_TypeDef i2cRet = I2C_TransferInit(i2cHandle->port, &i2cTransfer);
	do{
		i2cRet = I2C_Transfer(i2cHandle->port);
	}while(i2cRet == i2cTransferInProgress);

	i2cHandle->busy = false;
	return i2cRet;
}

I2C_RETURN i2c_writeRead(void *handle, uint8_t addr, uint8_t *txBuf, uint8_t txSize, uint8_t *rxBuf, uint8_t rxSize)
{
	if (!handle) {
		return I2C_NO_HANDLER;
	}

	if(rxSize == 0) {
		return I2C_USAGE_FAULT;
	}

	struct I2CHandle_s *i2cHandle =  (struct I2CHandle_s *)handle;

	if(!i2cHandle->initialized) {
		return I2C_NO_INIT;
	}

	if(i2cHandle->busy) {
		return I2C_TRANSFER_IN_PROGRESS;
	}

	i2cHandle->busy = true;

	//i2cRet = I2C_SUCCESS;
	I2C_TransferSeq_TypeDef i2cTransfer;

	i2cTransfer.addr  = addr;
	i2cTransfer.flags = I2C_FLAG_WRITE_READ;
	i2cTransfer.buf[0].data   = txBuf;
	i2cTransfer.buf[0].len    = txSize;

	/* Select location/length of data to be read */
	i2cTransfer.buf[1].data = rxBuf;
	i2cTransfer.buf[1].len  = rxSize;

	/* Sending data */
	I2C_TransferReturn_TypeDef i2cRet = I2C_TransferInit(i2cHandle->port, &i2cTransfer);
	do{
		i2cRet = I2C_Transfer(i2cHandle->port);
	}while(i2cRet == i2cTransferInProgress);

	i2cHandle->busy = false;
	return i2cRet;
}
