/**
 * @file 			i2c.h
 *
 * @createdBy		grli
 * @dateCreated		28.06.2019
 * $Revision: 29 $
 * $LastChangedBy: grli $
 * $LastChangedDate: 2019-06-27 11:06:20 +0200 (Thu, 27 Jun 2019) $
 *      
 * 
**/

#ifndef _I2C_H_
#define _I2C_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#ifndef I2C_BUFFER_SIZE
#define I2C_BUFFER_SIZE	32 			//!< I2C buffer size.
#endif

/**
 * @brief Return codes for i2c communication.
 */
typedef enum {
	I2C_TRANSFER_IN_PROGRESS = 1,   //!< Transfer in progress.
	I2C_SUCCESS = 0,     			//!< Transfer completed successfully.
	I2C_NACK = -1,        			//!< NACK received during transfer.
	I2C_BUS_ERR = -2,    			//!< Bus error during transfer (misplaced START/STOP).
	I2C_ARB_LOST = -3,   			//!< Arbitration lost during transfer.
	I2C_USAGE_FAULT = -4,			//!< Usage fault.
	I2C_SW_FAULT = -5,   			//!< SW fault.
	I2C_NO_HANDLER = -98,			//!<
	I2C_NO_INIT = -99    			//!< I2C not initialized.
}I2C_RETURN;

/**
 * @brief
 */
typedef enum{
	I2C_IDX0 = 0,					//!< I2C_IDX0
	I2C_IDX1,     					//!< I2C_IDX1
}I2C_IDX;

/**
 * @brief I2C configuration
 */
typedef struct
{
	I2C_IDX		i2c;          		/**< Peripheral port */
	uint8_t		sclPort;        	/**< SCL pin port number */
	uint8_t		sclPin;         	/**< SCL pin number */
	uint8_t		sdaPort;        	/**< SDA pin port number */
	uint8_t		sdaPin;         	/**< SDA pin number */
	uint32_t	i2cMaxFreq;     	/**< I2C max bus frequency to use */
#if defined (_I2C_ROUTELOC0_MASK)
	uint8_t		portLocationScl; 	/**< Port location of SCL signal */
	uint8_t		portLocationSda;	/**< Port location of SDA signal */
#else
	uint8_t		portLocation;   	/**< Port location */
#endif
	uint8_t		slaveAddress;		/**< I2C slave address in slave mode */
} I2CConfig_t;


/**
 *
 * @param rxBuf
 * @param rxSize
 * @param txBuf
 * @param txSize
 */
typedef void (*i2cSlaveCallback_fptr_t)(uint8_t *rxBuf, uint8_t rxSize, uint8_t *txBuf, uint8_t *txSize);

/**
 *
 * @param i2cConfig
 */
void *i2c_createHandle(I2CConfig_t *i2cConfig);

/**
 *
 * @param handle
 * @return
 */
I2C_RETURN i2c_destroyHandle(void **handle);

/**
 * @brief Initialize I2C
 *
 * The initialized i2c index is I2C0 on port PA0 and PA1 (route location zero).
 *
 * @param handle
 * @param callback
 * @param master
 * @return I2C_RETURN
 */
I2C_RETURN i2c_init(void *handle, i2cSlaveCallback_fptr_t callback, bool master);

/**
 *
 * @param handle
 * @return
 */
I2C_RETURN i2c_deInit(void *handle);

/**
 * @brief Check if I2C is initialized
 *
 * @param handle
 *
 * @return I2C_RETURN
 * @retval I2C_SUCCESS i2c initialized
 * @retval I2C_NO_INIT i2c not initialized
 */
I2C_RETURN i2c_getInitialized(void *handle);


/**
 * @brief Starts the I2C communication without reading.
 *
 * It is only possible to write data to a device.
 *
 * @param handle
 * @param addr  I2C bus address in 7-bit format
 * @param txBuf TX buffer
 * @param txSize TX buffer size
 * @return I2C_RETURN
 */
I2C_RETURN i2c_write(void *handle, uint8_t addr, uint8_t* txBuf, uint32_t txSize);

/**
 * @brief Starts the I2C communication with reading and writing.
 *
 * It is possible to write and read data at the same time.
 *
 * @param handle
 * @param addr I2C bus address in 7-bit format
 * @param txBuf TX buffer
 * @param txSize TX buffer size
 * @param rxBuf RX buffer
 * @param rxSize RX buffer size, set to 0 if no data will be received
 * @return I2C_RETURN
 */
I2C_RETURN i2c_writeRead(void *handle, uint8_t addr, uint8_t *txBuf, uint8_t txSize, uint8_t *rxBuf, uint8_t rxSize);

#endif /* _I2C_H_ */
