/******************** (C) COPYRIGHT 2011 STMicroelectronics ********************
* File Name          : stc3115_Battery.h
* Author             : AMS - IMS application
* Version            : V00
* Date               : 30 July 2014
* Description        : Application/Battery description
********************************************************************************
* THE PRESENT SOFTWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH SOFTWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.

* THIS SOURCE CODE IS PROTECTED BY A LICENSE.
* FOR MORE INFORMATION PLEASE CAREFULLY READ THE LICENSE AGREEMENT FILE LOCATED
* IN THE ROOT DIRECTORY OF THIS FIRMWARE PACKAGE.
*******************************************************************************/

/* Define to prevent recursive inclusion ------------------------------------ */
#ifndef __Battery_H
#define __Battery_H

#include <stdint.h>

/**
 * @brief Default battery definition.
 *
 * @param CAPACITY 				1500mAh
 * @param RINT 					200mOhm
 * @param VMODE 				mixed mode
 * @param ALM_EN 				disabled
 * @param ALM_SOC 				10%
 * @param ALM_VBAT 				3600mV
 * @param RSENSE 				10mOhm
 * @param APP_EOC_CURRENT 		75mA
 * @param APP_CUTOFF_VOLTAGE	3000mV
 */
#define BATTERY_DEFAULT		\
{							\
  1500,						\
  200,						\
  0,						\
  0,						\
  10,						\
  3600,						\
  10,						\
  75,						\
  3000						\
}

/**
 * @brief Battery definition structure
 */
typedef struct {
	uint32_t CAPACITY;					//!< battery nominal capacity in mAh
	uint16_t RINT;						//!< Internal battery impedance in mOhms,0 if unknown
	uint8_t VMODE;						//!< running mode constant, VM_MODE or MIXED_MODE
	uint8_t ALM_EN;						//!< Alarm enable constant, set at 1 to enable
	uint8_t ALM_SOC;					//!< SOC alarm in %
	uint32_t ALM_VBAT;					//!< Voltage alarm in mV
	uint16_t RSENSE;					//!< sense resistor in mOhms
	uint16_t APP_EOC_CURRENT;			//!< end charge current in mA
	uint32_t APP_CUTOFF_VOLTAGE;		//!< end charge current in mV
} STC3115Battery_t;

#endif
