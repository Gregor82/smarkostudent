/**
 * @file			stc3115.h
 * @date			20.03.2017
 *	
 * @details
 */

#ifndef STC3115_H_
#define STC3115_H_

#include <stdio.h>

#include "inc/i2c.h"
#include "inc/stc3115Battery.h"


#define STC3115_ADDRESS		0xE0	//!< STC3115 I2C address

/**
 * @brief Return codes for the STC3115.
 */
typedef enum {
    STC3115_SUCCESS = 0, 		//!< Transfer completed successfully.
    STC3115_FAIL = -1,   		//!< STC3115 error
    STC3115_I2C_ERR = -2,		//!< I2C error.
	STC3115_NO_HANDLER = -98,			//!<
    STC3115_NO_INIT = -99		//!< STC3115 not initialized.
}STC3115_RETURN;

/**
 *
 * @param i2cConfig
 */
void *stc3115_createHandle(void *i2cHandle, uint8_t slaveAddress, STC3115Battery_t *batteryDefinition);

/**
 *
 * @param handle
 * @return
 */
STC3115_RETURN stc3115_destroyHandle(void **handle);

/**
 * @brief Initialize the STC3115. The battery values are defined in the stc3115.h file.
 *
 * @code {.c}
STC3115_RETURN retValue;
STC3115Battery_t batDef = BATTERY_DEFALT;

retValue = stc3115_init(batDef);
   @endcode
 *
 * @param[in] batteryDefinition Battery definition
 *
 * @return STC3115_RETURN
 * @retval STC3115_SUCCESS 	STC3115 detected
 * @retval STC3115_FAIL 	STC3115 not detected and initialisation failed
 * @retval STC3115_I2C_ERR 	I2C communication error
 * @retval STC3115_NO_INT 	STC3115 not initialized
 */
STC3115_RETURN stc3115_init(void *handle);

/**
 * @brief Uninitialize the STC3115 temperature sensor.
 *
 * @return STC3115_RETURN
 * @retval STC3115_SUCCESS Uninitialization success
 */
STC3115_RETURN stc3115_deInit(void *handle);

/**
 * @brief Check if the initialization status of the STC3115.
 *
 * @return STC3115_RETURN
 * @retval STC3115_SUCCESS STC3115 initialized
 * @retval STC3115_NO_INT STC3115 not initialized
 */
STC3115_RETURN stc3115_getInitialized(void *handle);

/**
 * @brief Perform a measurement.
 *
 * @note Must be called on a regular basis!
 *
 * @return STC3115_RETURN
 * @retval STC3115_SUCCESS 	STC3115 detected
 * @retval STC3115_NO_INT 	STC3115 not initialized
 */
STC3115_RETURN stc3115_measure(void *handle);

/**
 * @brief Gets all data from the STC3115.
 *
 * @note Call #stc3115_measure before calling stc3115_getValues to get actual data.
 *
 * @param[out] voltage		Measured battery voltage
 * @param[out] current		Measured battery current
 * @param[out] temperature	Measured temperature on the STC3115
 * @param[out] soc			Calculated battery capacity in %
 *
 * @return STC3115_RETURN
 * @retval STC3115_SUCCESS 	STC3115 detected
 * @retval STC3115_NO_INT 	STC3115 not initialized
 */
STC3115_RETURN stc3115_getValues(void *handle, int16_t *voltage, int16_t *current, int16_t *temperature, int16_t *soc);

/** @cond DO_NOT_INCLUDE_WITH_DOXYGEN */
#ifdef UNIT_TESTS
int8_t stc3115_i2cWrite(uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt);
int8_t stc3115_i2cRead(uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt);
#endif
/** @endcond */

#endif /* STC3115_H_ */
