/**
 * @file			stc3115.c
 * @date			20.03.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include "inc/stc3115.h"
#include "driver/stc3115Driver.h"

/******************** defines *****************************/
#ifndef UNIT_TESTS
static int8_t stc3115_i2cWrite(uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt);
static int8_t stc3115_i2cRead(uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt);
#endif

/******************** prototypes **************************/

/******************** struct data type ********************/
struct STC3115Handle_s {
	void **i2c;
	uint8_t slaveAddress;

	STC3115DRIVER_ConfigData_TypeDef stc3115ConfigData;
	STC3115DRIVER_BatteryData_TypeDef stc3115BatteryData;

	bool initialized;

	int16_t stc3115_voltage;
	int16_t stc3115_current;
	int16_t stc3115_temperature;
	int16_t stc3115_soc;
};

/******************** private variables********************/
static void **localI2C = NULL;
static uint8_t localSlaveAddress;

static uint8_t writeData[I2C_BUFFER_SIZE];
static uint8_t readData[I2C_BUFFER_SIZE];

/******************** private function ********************/
int8_t stc3115_i2cWrite(uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt)
{
	uint8_t i;

	writeData[0] = reg_addr;
	for(i = 0; i < cnt; i++)
		writeData[i + 1] = reg_data[i];

	if(i2c_write(localI2C, localSlaveAddress, writeData, cnt + 1) != I2C_SUCCESS)
		return -1;

	return 0;
}

int8_t stc3115_i2cRead(uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt)
{
	uint8_t i;

	writeData[0] = reg_addr;
	if(i2c_writeRead(localI2C, localSlaveAddress, writeData, 1, readData, cnt) != I2C_SUCCESS)
		return -1;

	for(i = 0; i < cnt; i++)
		reg_data[i] = readData[i];

	return 0;
}

/******************** global function *********************/
void *stc3115_createHandle(void *i2cHandle, uint8_t slaveAddress, STC3115Battery_t *batteryDefinition)
{
	if(batteryDefinition == NULL)
		return NULL;

	struct STC3115Handle_s *stc3115Handle = calloc(1, sizeof(struct STC3115Handle_s));
	localI2C = i2cHandle;
	stc3115Handle->i2c = i2cHandle;
	localSlaveAddress = slaveAddress;
	stc3115Handle->slaveAddress = slaveAddress;

	stc3115Driver_setup(batteryDefinition, stc3115_i2cRead, stc3115_i2cRead);

	return stc3115Handle;
}

STC3115_RETURN stc3115_destroyHandle(void **handle)
{
	stc3115_init(*handle);

	free(*handle);
	*handle = NULL;

	return STC3115_SUCCESS;
}

STC3115_RETURN stc3115_init(void *handle)
{
	uint8_t i2cReadData;

	if(!handle) {
		return STC3115_NO_HANDLER;
	}

	struct STC3115Handle_s *stc3115Handle = (struct STC3115Handle_s *)handle;
	stc3115Handle->initialized = false;


	if(stc3115_i2cRead(STC3115DRIVER_REG_ID, &i2cReadData, 1) != 0)
		return STC3115_I2C_ERR;

	if(i2cReadData != STC3115DRIVER_ID)
		return STC3115_FAIL;

	stc3115Handle->stc3115_voltage = -1;
	stc3115Handle->stc3115_current = -1;
	stc3115Handle->stc3115_temperature = -1;
	stc3115Handle->stc3115_soc = -1;

	if(GasGauge_Initialization(&stc3115Handle->stc3115ConfigData, &stc3115Handle->stc3115BatteryData) != STC3115_SUCCESS)
		return STC3115_NO_INIT;

	stc3115Handle->initialized = true;

	return stc3115_measure(handle);
}

STC3115_RETURN stc3115_deInit(void *handle)
{
	if(!handle) {
		return STC3115_NO_HANDLER;
	}

	struct STC3115Handle_s *stc3115Handle = (struct STC3115Handle_s *)handle;
	stc3115Handle->initialized = false;

	return STC3115_SUCCESS;
}

STC3115_RETURN stc3115_getInitialized(void *handle)
{
	if (!handle) {
		return STC3115_NO_HANDLER;
	}

	if(!((struct STC3115Handle_s*)handle)->initialized) {
		return STC3115_NO_INIT;
	}

	return STC3115_SUCCESS;
}

STC3115_RETURN stc3115_measure(void *handle)
{
	if(!handle) {
		return I2C_NO_HANDLER;
	}

	struct STC3115Handle_s *stc3115Handle = (struct STC3115Handle_s *)handle;

	if(!stc3115Handle->initialized) {
		return STC3115_NO_INIT;
	}

	stc3115Handle->stc3115_voltage = -1;
	stc3115Handle->stc3115_current = -1;
	stc3115Handle->stc3115_temperature = -1;
	stc3115Handle->stc3115_soc = -1;

	GasGauge_Task(&stc3115Handle->stc3115ConfigData, &stc3115Handle->stc3115BatteryData);
	stc3115Handle->stc3115_voltage = stc3115Handle->stc3115BatteryData.Voltage;
	stc3115Handle->stc3115_current = stc3115Handle->stc3115BatteryData.Current;
	//stc3115Handle->stc3115_temperature = stc3115Handle->stc3115BatteryData.Temperature;
	//stc3115Handle->stc3115_soc = stc3115Handle->stc3115BatteryData.SOC;

	// Temperature has only one decimal plcae, but for consistency with body
	// temperature we add another place
	stc3115Handle->stc3115_temperature = stc3115Handle->stc3115BatteryData.Temperature * 10;
	stc3115Handle->stc3115_soc = (stc3115Handle->stc3115BatteryData.SOC + 5) / 10;

	return STC3115_SUCCESS;
}

STC3115_RETURN stc3115_getValues(void *handle, int16_t *voltage, int16_t *current, int16_t *temperature, int16_t *soc)
{
	*voltage = -1;
	*current = -1;
	*temperature = -1;
	*soc = -1;

	if (!handle) {
		return STC3115_NO_HANDLER;
	}

	struct STC3115Handle_s *stc3115Handle = (struct STC3115Handle_s *)handle;

	if(!stc3115Handle->initialized) {
		return STC3115_NO_INIT;
	}

	*voltage = stc3115Handle->stc3115_voltage;
	*current = stc3115Handle->stc3115_current;
	*temperature = stc3115Handle->stc3115_temperature;
	*soc = stc3115Handle->stc3115_soc;

	return STC3115_SUCCESS;
}
