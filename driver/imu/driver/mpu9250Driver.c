/**
 * @file 	mpu9250Driver.c
 * @author 	GrLi
 * @date 	30.01.2019
 *      
 * @brief
**/


/******************** include files ***********************/
#include "math.h"
#include "imu/driver/mpu9250Driver.h"

/******************** defines *****************************/
//Magnetometer Registers
#define MPU9250_AK8963_ADDRESS   0x0C
#define MPU9250_WHO_AM_I_AK8963  0x00 // should return 0x48
#define MPU9250_INFO             0x01
#define MPU9250_AK8963_ST1       0x02  // data ready status bit 0
#define MPU9250_AK8963_XOUT_L    0x03  // data
#define MPU9250_AK8963_XOUT_H    0x04
#define MPU9250_AK8963_YOUT_L    0x05
#define MPU9250_AK8963_YOUT_H    0x06
#define MPU9250_AK8963_ZOUT_L    0x07
#define MPU9250_AK8963_ZOUT_H    0x08
#define MPU9250_AK8963_ST2       0x09  // Data overflow bit 3 and data read error status bit 2
#define MPU9250_AK8963_CNTL      0x0A  // Power down (0000), single-measurement (0001), self-test (1000) and Fuse ROM (1111) modes on bits 3:0
#define MPU9250_AK8963_CNTL2     0x0B  // Reset
#define MPU9250_AK8963_ASTC      0x0C  // Self test control
#define MPU9250_AK8963_I2CDIS    0x0F  // I2C disable
#define MPU9250_AK8963_ASAX      0x10  // Fuse ROM x-axis sensitivity adjustment value
#define MPU9250_AK8963_ASAY      0x11  // Fuse ROM y-axis sensitivity adjustment value
#define MPU9250_AK8963_ASAZ      0x12  // Fuse ROM z-axis sensitivity adjustment value

#define MPU9250_SELF_TEST_X_GYRO 0x00
#define MPU9250_SELF_TEST_Y_GYRO 0x01
#define MPU9250_SELF_TEST_Z_GYRO 0x02

/*#define MPU9250_X_FINE_GAIN      0x03 // [7:0] fine gain
#define MPU9250_Y_FINE_GAIN      0x04
#define MPU9250_Z_FINE_GAIN      0x05
#define MPU9250_XA_OFFSET_H      0x06 // User-defined trim values for accelerometer
#define MPU9250_XA_OFFSET_L_TC   0x07
#define MPU9250_YA_OFFSET_H      0x08
#define MPU9250_YA_OFFSET_L_TC   0x09
#define MPU9250_ZA_OFFSET_H      0x0A
#define MPU9250_ZA_OFFSET_L_TC   0x0B */

#define MPU9250_SELF_TEST_X_ACCEL 0x0D
#define MPU9250_SELF_TEST_Y_ACCEL 0x0E
#define MPU9250_SELF_TEST_Z_ACCEL 0x0F

#define MPU9250_SELF_TEST_A      0x10

#define MPU9250_XG_OFFSET_H      0x13  // User-defined trim values for gyroscope
#define MPU9250_XG_OFFSET_L      0x14
#define MPU9250_YG_OFFSET_H      0x15
#define MPU9250_YG_OFFSET_L      0x16
#define MPU9250_ZG_OFFSET_H      0x17
#define MPU9250_ZG_OFFSET_L      0x18
#define MPU9250_SMPLRT_DIV       0x19
#define MPU9250_CONFIG   	 	 0x1A
#define MPU9250_GYRO_CONFIG      0x1B
#define MPU9250_ACCEL_CONFIG     0x1C
#define MPU9250_ACCEL_CONFIG2    0x1D
#define MPU9250_LP_ACCEL_ODR     0x1E
#define MPU9250_WOM_THR          0x1F

#define MPU9250_MOT_DUR          0x20  // Duration counter threshold for motion interrupt generation, 1 kHz rate, LSB = 1 ms
#define MPU9250_ZMOT_THR         0x21  // Zero-motion detection threshold bits [7:0]
#define MPU9250_ZRMOT_DUR        0x22  // Duration counter threshold for zero motion interrupt generation, 16 Hz rate, LSB = 64 ms

#define MPU9250_FIFO_EN          0x23
#define MPU9250_I2C_MST_CTRL     0x24
#define MPU9250_I2C_SLV0_ADDR    0x25
#define MPU9250_I2C_SLV0_REG     0x26
#define MPU9250_I2C_SLV0_CTRL    0x27
#define MPU9250_I2C_SLV1_ADDR    0x28
#define MPU9250_I2C_SLV1_REG     0x29
#define MPU9250_I2C_SLV1_CTRL    0x2A
#define MPU9250_I2C_SLV2_ADDR    0x2B
#define MPU9250_I2C_SLV2_REG     0x2C
#define MPU9250_I2C_SLV2_CTRL    0x2D
#define MPU9250_I2C_SLV3_ADDR    0x2E
#define MPU9250_I2C_SLV3_REG     0x2F
#define MPU9250_I2C_SLV3_CTRL    0x30
#define MPU9250_I2C_SLV4_ADDR    0x31
#define MPU9250_I2C_SLV4_REG     0x32
#define MPU9250_I2C_SLV4_DO      0x33
#define MPU9250_I2C_SLV4_CTRL    0x34
#define MPU9250_I2C_SLV4_DI      0x35
#define MPU9250_I2C_MST_STATUS   0x36
#define MPU9250_INT_PIN_CFG      0x37
#define MPU9250_INT_ENABLE       0x38
#define MPU9250_DMP_INT_STATUS   0x39  // Check DMP interrupt
#define MPU9250_INT_STATUS       0x3A
#define MPU9250_ACCEL_XOUT_H     0x3B
#define MPU9250_ACCEL_XOUT_L     0x3C
#define MPU9250_ACCEL_YOUT_H     0x3D
#define MPU9250_ACCEL_YOUT_L     0x3E
#define MPU9250_ACCEL_ZOUT_H     0x3F
#define MPU9250_ACCEL_ZOUT_L     0x40
#define MPU9250_TEMP_OUT_H       0x41
#define MPU9250_TEMP_OUT_L       0x42
#define MPU9250_GYRO_XOUT_H      0x43
#define MPU9250_GYRO_XOUT_L      0x44
#define MPU9250_GYRO_YOUT_H      0x45
#define MPU9250_GYRO_YOUT_L      0x46
#define MPU9250_GYRO_ZOUT_H      0x47
#define MPU9250_GYRO_ZOUT_L      0x48
#define MPU9250_EXT_SENS_DATA_00 0x49
#define MPU9250_EXT_SENS_DATA_01 0x4A
#define MPU9250_EXT_SENS_DATA_02 0x4B
#define MPU9250_EXT_SENS_DATA_03 0x4C
#define MPU9250_EXT_SENS_DATA_04 0x4D
#define MPU9250_EXT_SENS_DATA_05 0x4E
#define MPU9250_EXT_SENS_DATA_06 0x4F
#define MPU9250_EXT_SENS_DATA_07 0x50
#define MPU9250_EXT_SENS_DATA_08 0x51
#define MPU9250_EXT_SENS_DATA_09 0x52
#define MPU9250_EXT_SENS_DATA_10 0x53
#define MPU9250_EXT_SENS_DATA_11 0x54
#define MPU9250_EXT_SENS_DATA_12 0x55
#define MPU9250_EXT_SENS_DATA_13 0x56
#define MPU9250_EXT_SENS_DATA_14 0x57
#define MPU9250_EXT_SENS_DATA_15 0x58
#define MPU9250_EXT_SENS_DATA_16 0x59
#define MPU9250_EXT_SENS_DATA_17 0x5A
#define MPU9250_EXT_SENS_DATA_18 0x5B
#define MPU9250_EXT_SENS_DATA_19 0x5C
#define MPU9250_EXT_SENS_DATA_20 0x5D
#define MPU9250_EXT_SENS_DATA_21 0x5E
#define MPU9250_EXT_SENS_DATA_22 0x5F
#define MPU9250_EXT_SENS_DATA_23 0x60
#define MPU9250_MOT_DETECT_STATUS 0x61
#define MPU9250_I2C_SLV0_DO      0x63
#define MPU9250_I2C_SLV1_DO      0x64
#define MPU9250_I2C_SLV2_DO      0x65
#define MPU9250_I2C_SLV3_DO      0x66
#define MPU9250_I2C_MST_DELAY_CTRL 0x67
#define MPU9250_SIGNAL_PATH_RESET  0x68
#define MPU9250_MOT_DETECT_CTRL  0x69
#define MPU9250_USER_CTRL        0x6A  // Bit 7 enable DMP, bit 3 reset DMP
#define MPU9250_PWR_MGMT_1       0x6B // Device defaults to the SLEEP mode
#define MPU9250_PWR_MGMT_2       0x6C
#define MPU9250_DMP_BANK         0x6D  // Activates a specific bank in the DMP
#define MPU9250_DMP_RW_PNT       0x6E  // Set read/write pointer to a specific start address in specified DMP bank
#define MPU9250_DMP_REG          0x6F  // Register in DMP from which to read or to which to write
#define MPU9250_DMP_REG_1        0x70
#define MPU9250_DMP_REG_2        0x71
#define MPU9250_FIFO_COUNTH      0x72
#define MPU9250_FIFO_COUNTL      0x73
#define MPU9250_FIFO_R_W         0x74
#define MPU9250_WHO_AM_I_MPU9250 0x75 // Should return 0x71
#define MPU9250_XA_OFFSET_H      0x77
#define MPU9250_XA_OFFSET_L      0x78
#define MPU9250_YA_OFFSET_H      0x7A
#define MPU9250_YA_OFFSET_L      0x7B
#define MPU9250_ZA_OFFSET_H      0x7D
#define MPU9250_ZA_OFFSET_L      0x7E

/******************** prototypes **************************/
static float getMres(MSCALE_SEL Mscale);
static float getGres(GYRO_FS_SEL Gscale);
static float getAres(ACCEL_FS_SEL Ascale);

//static void mag_writeRegister(imuDriver_t *mpu9250, uint8_t regAdd, uint8_t data);
//static void mag_readRegister(imuDriver_t *mpu9250, uint8_t regAdd, uint8_t *data, uint8_t len);

/******************** struct data type ********************/

/******************** private variables********************/
static uint8_t Mmode = 0x06;        // Either 8 Hz 0x02) or 100 Hz (0x06) magnetometer data ODR

/******************** private function ********************/
float getMres(MSCALE_SEL Mscale)
{
  switch (Mscale)
  {
    // Possible magnetometer scales (and their register bit settings) are:
    // 14 bit resolution (0) and 16 bit resolution (1)
    case MFS_14BITS:
          return 10.0*4912.0/8190.0; // Proper scale to return milliGauss
          break;
    case MFS_16BITS:
    	  return 10.0*4912.0/32760.0; // Proper scale to return milliGauss
          break;
  }
  return 0;
}

float getGres(GYRO_FS_SEL Gscale)
{
  switch (Gscale)
  {
    // Possible gyro scales (and their register bit settings) are:
    // 250 DPS (00), 500 DPS (01), 1000 DPS (10), and 2000 DPS  (11).
    // Here's a bit of an algorith to calculate DPS/(ADC tick) based on that 2-bit value:
#if (MPU9250)
    case GYRO_FS_SEL_250dps:
    	  return 250.0/32768.0;
          break;
#endif
    case GYRO_FS_SEL_500dps:
    	  return 500.0/32768.0;
          break;
    case GYRO_FS_SEL_1000dps:
    	  return 1000.0/32768.0;
          break;
    case GYRO_FS_SEL_2000dps:
    	return 2000.0/32768.0;
        break;
  }
  return 0;
}

float getAres(ACCEL_FS_SEL Ascale)
{
  switch (Ascale)
  {
    // Possible accelerometer scales (and their register bit settings) are:
    // 2 Gs (00), 4 Gs (01), 8 Gs (10), and 16 Gs  (11).
        // Here's a bit of an algorith to calculate DPS/(ADC tick) based on that 2-bit value:
#if (MPU9250)
    case ACCEL_FS_SEL_2g:
    	  return 2.0/32768.0;
          break;
#endif
    case ACCEL_FS_SEL_4g:
    	  return 4.0/32768.0;
          break;
    case ACCEL_FS_SEL_8g:
    	  return 8.0/32768.0;
          break;
    case ACCEL_FS_SEL_16g:
    	  return 16.0/32768.0;
          break;
  }
  return 0;
}

/*
void mag_writeRegister(imuDriver_t *mpu9250, uint8_t regAdd, uint8_t data)
{
	uint8_t i2cData = MPU9250_AK8963_ADDRESS;
	mpu9250->write(MPU9250_I2C_SLV0_ADDR, &i2cData, 1);
	mpu9250->write(MPU9250_I2C_SLV0_REG, &regAdd, 1);
	mpu9250->write(MPU9250_I2C_SLV0_DO, &data, 1);
	i2cData = 0x80 | 0x01;
	mpu9250->write(MPU9250_I2C_SLV0_CTRL, &i2cData, 1);
}

void mag_readRegister(imuDriver_t *mpu9250, uint8_t regAdd, uint8_t *data, uint8_t len)
{
	uint8_t i2cData = MPU9250_AK8963_ADDRESS | 0x80;
	mpu9250->write(MPU9250_I2C_SLV0_ADDR, &i2cData, 1);
	mpu9250->write(MPU9250_I2C_SLV0_REG, &regAdd, 1);
	i2cData = 0x80 | len;
	mpu9250->write(MPU9250_I2C_SLV0_CTRL, &i2cData, 1);
	mpu9250->read(MPU9250_EXT_SENS_DATA_00, data, len);
}*/

/******************** interrupt function ******************/

/******************** global function *********************/
int8_t mpu9250Driver_getID(void *handle, imuDriver_t *mpu9250, uint8_t *id)
{
	return mpu9250->read(handle, MPU9250_WHO_AM_I_MPU9250, id, 1, false);
}

void mpu9250Driver_reset(void *handle, imuDriver_t *mpu9250)
{
	// reset device
	uint8_t data = 0x80;
	mpu9250->write(handle, MPU9250_PWR_MGMT_1, &data, 1, false);
	delay(100); 		// Wait for all registers to reset
}

// Function which accumulates gyro and accelerometer data after device initialization. It calculates the average
// of the at-rest readings and then loads the resulting offsets into accelerometer and gyro bias registers.
void mpu9250Driver_calibrate(void *handle, imuDriver_t *mpu9250)
{
	uint8_t i2cData = 0;
	uint8_t data[12]; // data array to hold accelerometer and gyro x, y, z, data
	uint16_t ii;
	uint16_t packet_count;
	uint16_t fifo_count;
	int32_t gyro_bias[3]  = {0, 0, 0};
	int32_t accel_bias[3] = {0, 0, 0};

	mpu9250->accelBias[0] = 0;
	mpu9250->accelBias[1] = 0;
	mpu9250->accelBias[2] = 0;
	mpu9250->gyroBias[0] = 0;
	mpu9250->gyroBias[1] = 0;
	mpu9250->gyroBias[2] = 0;

	mpu9250Driver_reset(handle, mpu9250);
	delay(100);

	// get stable time source; Auto select clock source to be PLL gyroscope reference if ready
	// else use the internal oscillator, bits 2:0 = 001
	i2cData = 0x01;
	mpu9250->write(handle, MPU9250_PWR_MGMT_1, &i2cData, 1, false);
	i2cData = 0x00;
	mpu9250->write(handle, MPU9250_PWR_MGMT_2, &i2cData, 1, false);
	delay(200);

	// Configure device for bias calculation 001
	i2cData = 0x00;
	mpu9250->write(handle, MPU9250_INT_ENABLE, &i2cData, 1, false);		// Disable all interrupts
	mpu9250->write(handle, MPU9250_FIFO_EN, &i2cData, 1, false);			// Disable FIFO
	mpu9250->write(handle, MPU9250_PWR_MGMT_1, &i2cData, 1, false);		// Turn on internal clock source
	mpu9250->write(handle, MPU9250_I2C_MST_CTRL, &i2cData, 1, false);		// Disable I2C master
	mpu9250->write(handle, MPU9250_USER_CTRL, &i2cData, 1, false);			// Disable FIFO and I2C master modes
	i2cData = 0x0C;
	mpu9250->write(handle, MPU9250_USER_CTRL, &i2cData, 1, false);			// Reset FIFO and DMP
	delay(15);

	// Configure MPU6050 gyro and accelerometer for bias calculation
	i2cData = 0x01;
	mpu9250->write(handle, MPU9250_CONFIG, &i2cData, 1, false);
	i2cData = 0x00;
	mpu9250->write(handle, MPU9250_SMPLRT_DIV, &i2cData, 1, false);		// Set sample rate to 1 kHz
	mpu9250->write(handle, MPU9250_GYRO_CONFIG, &i2cData, 1, false);		// Set gyro full-scale to 250 degrees per second, maximum sensitivity
	mpu9250->write(handle, MPU9250_ACCEL_CONFIG, &i2cData, 1, false);	// Set accelerometer full-scale to 2 g, maximum sensitivity

	uint16_t  gyrosensitivity  = 131;   			// = 131 LSB/degrees/sec
	uint16_t  accelsensitivity = 16384;  			// = 16384 LSB/g

	// Configure FIFO to capture accelerometer and gyro data for bias calculation
	i2cData = 0x40;
	mpu9250->write(handle, MPU9250_USER_CTRL, &i2cData, 1, false);  		// Enable FIFO
	i2cData = 0x78;
	mpu9250->write(handle, MPU9250_FIFO_EN, &i2cData, 1, false);    		// Enable gyro and accelerometer sensors for FIFO  (max size 512 bytes in MPU-9150)
	delay(40); // accumulate 40 samples in 40 milliseconds = 480 bytes

	// At end of sample accumulation, turn off FIFO sensor read
	i2cData = 0x00;
	mpu9250->write(handle, MPU9250_FIFO_EN, &i2cData, 1, false);        	// Disable gyro and accelerometer sensors for FIFO
	mpu9250->read(handle, MPU9250_FIFO_COUNTH, data, 2, false); 		// read FIFO sample count
	fifo_count = ((uint16_t)data[0] << 8) | data[1];
	packet_count = fifo_count/12;					// How many sets of full gyro and accelerometer data for averaging

	for (ii = 0; ii < packet_count; ii++) {
		int16_t accel_temp[3] = {0, 0, 0};
		int16_t gyro_temp[3] = {0, 0, 0};
		mpu9250->read(handle, MPU9250_FIFO_R_W, data, 12, false);
		accel_temp[0] = (int16_t) (((int16_t)data[0] << 8) | data[1]  ) ;  // Form signed 16-bit integer for each sample in FIFO
		accel_temp[1] = (int16_t) (((int16_t)data[2] << 8) | data[3]  ) ;
		accel_temp[2] = (int16_t) (((int16_t)data[4] << 8) | data[5]  ) ;
		gyro_temp[0]  = (int16_t) (((int16_t)data[6] << 8) | data[7]  ) ;
		gyro_temp[1]  = (int16_t) (((int16_t)data[8] << 8) | data[9]  ) ;
		gyro_temp[2]  = (int16_t) (((int16_t)data[10] << 8) | data[11]) ;

		accel_bias[0] += (int32_t) accel_temp[0]; // Sum individual signed 16-bit biases to get accumulated signed 32-bit biases
		accel_bias[1] += (int32_t) accel_temp[1];
		accel_bias[2] += (int32_t) accel_temp[2];
		gyro_bias[0]  += (int32_t) gyro_temp[0];
		gyro_bias[1]  += (int32_t) gyro_temp[1];
		gyro_bias[2]  += (int32_t) gyro_temp[2];
	}
	accel_bias[0] /= (int32_t) packet_count; // Normalize sums to get average count biases
	accel_bias[1] /= (int32_t) packet_count;
	accel_bias[2] /= (int32_t) packet_count;
	gyro_bias[0]  /= (int32_t) packet_count;
	gyro_bias[1]  /= (int32_t) packet_count;
	gyro_bias[2]  /= (int32_t) packet_count;

	if(accel_bias[2] > 0L)
		accel_bias[2] -= (int32_t) accelsensitivity;  // Remove gravity from the z-axis accelerometer bias calculation
	else
		accel_bias[2] += (int32_t) accelsensitivity;

	// Construct the gyro biases for push to the hardware gyro bias registers, which are reset to zero upon device startup
	data[0] = (-gyro_bias[0]/4  >> 8) & 0xFF; // Divide by 4 to get 32.9 LSB per deg/s to conform to expected bias input format
	data[1] = (-gyro_bias[0]/4)       & 0xFF; // Biases are additive, so change sign on calculated average gyro biases
	data[2] = (-gyro_bias[1]/4  >> 8) & 0xFF;
	data[3] = (-gyro_bias[1]/4)       & 0xFF;
	data[4] = (-gyro_bias[2]/4  >> 8) & 0xFF;
	data[5] = (-gyro_bias[2]/4)       & 0xFF;

	// Push gyro biases to hardware registers
	i2cData = data[0];
	mpu9250->write(handle, MPU9250_XG_OFFSET_H, &i2cData, 1, false);
	i2cData = data[1];
	mpu9250->write(handle, MPU9250_XG_OFFSET_L, &i2cData, 1, false);
	i2cData = data[2];
	mpu9250->write(handle, MPU9250_YG_OFFSET_H, &i2cData, 1, false);
	i2cData = data[3];
	mpu9250->write(handle, MPU9250_YG_OFFSET_L, &i2cData, 1, false);
	i2cData = data[4];
	mpu9250->write(handle, MPU9250_ZG_OFFSET_H, &i2cData, 1, false);
	i2cData = data[5];
	mpu9250->write(handle, MPU9250_ZG_OFFSET_L, &i2cData, 1, false);

	// Output scaled gyro biases for display in the main program
	mpu9250->gyroBias[0] = (float) gyro_bias[0]/(float) gyrosensitivity;
	mpu9250->gyroBias[1] = (float) gyro_bias[1]/(float) gyrosensitivity;
	mpu9250->gyroBias[2] = (float) gyro_bias[2]/(float) gyrosensitivity;

	// Construct the accelerometer biases for push to the hardware accelerometer bias registers. These registers contain
	// factory trim values which must be added to the calculated accelerometer biases; on boot up these registers will hold
	// non-zero values. In addition, bit 0 of the lower byte must be preserved since it is used for temperature
	// compensation calculations. Accelerometer bias registers expect bias input as 2048 LSB per g, so that
	// the accelerometer biases calculated above must be divided by 8.

	int32_t accel_bias_reg[3] = {0, 0, 0}; 			// A place to hold the factory accelerometer trim biases
	mpu9250->read(handle, MPU9250_XA_OFFSET_H, data, 2, false);		 	// Read factory accelerometer trim values
	accel_bias_reg[0] = (int32_t) (((int16_t)data[0] << 8) | data[1]);
	mpu9250->read(handle, MPU9250_YA_OFFSET_H, data, 2, false);
	accel_bias_reg[1] = (int32_t) (((int16_t)data[0] << 8) | data[1]);
	mpu9250->read(handle, MPU9250_ZA_OFFSET_H, data, 2, false);
	accel_bias_reg[2] = (int32_t) (((int16_t)data[0] << 8) | data[1]);

	uint32_t mask = 1uL; // Define mask for temperature compensation bit 0 of lower byte of accelerometer bias registers
	uint8_t mask_bit[3] = {0, 0, 0}; // Define array to hold mask bit for each accelerometer bias axis

	for(ii = 0; ii < 3; ii++) {
		if((accel_bias_reg[ii] & mask))
			mask_bit[ii] = 0x01; // If temperature compensation bit is set, record that fact in mask_bit
	}

	// Construct total accelerometer bias, including calculated average accelerometer bias from above
	accel_bias_reg[0] -= (accel_bias[0]/8); // Subtract calculated averaged accelerometer bias scaled to 2048 LSB/g (16 g full scale)
	accel_bias_reg[1] -= (accel_bias[1]/8);
	accel_bias_reg[2] -= (accel_bias[2]/8);

	data[0] = (accel_bias_reg[0] >> 8) & 0xFF;
	data[1] = (accel_bias_reg[0])      & 0xFF;
	data[1] = data[1] | mask_bit[0]; // preserve temperature compensation bit when writing back to accelerometer bias registers
	data[2] = (accel_bias_reg[1] >> 8) & 0xFF;
	data[3] = (accel_bias_reg[1])      & 0xFF;
	data[3] = data[3] | mask_bit[1]; // preserve temperature compensation bit when writing back to accelerometer bias registers
	data[4] = (accel_bias_reg[2] >> 8) & 0xFF;
	data[5] = (accel_bias_reg[2])      & 0xFF;
	data[5] = data[5] | mask_bit[2]; // preserve temperature compensation bit when writing back to accelerometer bias registers

	// Apparently this is not working for the acceleration biases in the MPU-9250
	// Are we handling the temperature correction bit properly?
	// Push accelerometer biases to hardware registers
	//  writeByte(MPU9250_ADDRESS, XA_OFFSET_H, data[0]);
	//  writeByte(MPU9250_ADDRESS, XA_OFFSET_L, data[1]);
	//  writeByte(MPU9250_ADDRESS, YA_OFFSET_H, data[2]);
	//  writeByte(MPU9250_ADDRESS, YA_OFFSET_L, data[3]);
	//  writeByte(MPU9250_ADDRESS, ZA_OFFSET_H, data[4]);
	//  writeByte(MPU9250_ADDRESS, ZA_OFFSET_L, data[5]);

	// Output scaled accelerometer biases for display in the main program
	mpu9250->accelBias[0] = (float)accel_bias[0]/(float)accelsensitivity;
	mpu9250->accelBias[1] = (float)accel_bias[1]/(float)accelsensitivity;
	mpu9250->accelBias[2] = (float)accel_bias[2]/(float)accelsensitivity;
/*
	uint8_t _Mmode = Mmode;

	int32_t mag_scale[3] = {0, 0, 0};
	int16_t mag_max[3] = {-32767, -32767, -32767};
	int16_t mag_min[3] = {32767, 32767, 32767};
	int16_t mag_temp[3] = {0, 0, 0};
	uint8_t rawData[7] = {0, 0, 0, 0, 0, 0, 0};

	delay(4000);

	// shoot for ~fifteen seconds of mag data
	if(_Mmode == 0x02) {
		packet_count = 128;  // at 8 Hz ODR, new mag data is available every 125 ms
	}

	if(_Mmode == 0x06) {
		packet_count = 1500;  // at 100 Hz ODR, new mag data is available every 10 ms
	}

	for(ii = 0; ii < packet_count; ii++) {
		if(_Mmode == 0x02) {
			delay(125);  // at 8 Hz ODR, new mag data is available every 125 ms
		}
		if(_Mmode == 0x06) {
			delay(10);   // at 100 Hz ODR, new mag data is available every 10 ms
		}

		mpu9250->read(handle, MPU9250_AK8963_XOUT_L, rawData, 7, true);// Read the x-, y-, and z-axis calibration values
		mag_temp[0] = ((int16_t)rawData[1] << 8) | rawData[0] ;     // Turn the MSB and LSB into a signed 16-bit value
		mag_temp[1] = ((int16_t)rawData[3] << 8) | rawData[2] ;     // Data stored as little Endian
		mag_temp[2] = ((int16_t)rawData[5] << 8) | rawData[4] ;

		for (int jj = 0; jj < 3; jj++) {
			if(mag_temp[jj] > mag_max[jj]) mag_max[jj] = mag_temp[jj];
			if(mag_temp[jj] < mag_min[jj]) mag_min[jj] = mag_temp[jj];
		}
	}

	// Get hard iron correction
	mpu9250->magbias[0]  = (mag_max[0] + mag_min[0])/2;  // get average x mag bias in counts
	mpu9250->magbias[1]  = (mag_max[1] + mag_min[1])/2;  // get average y mag bias in counts
	mpu9250->magbias[2]  = (mag_max[2] + mag_min[2])/2;  // get average z mag bias in counts

	mpu9250->read(handle, MPU9250_AK8963_ASAX, rawData, 3, true);	// Read the x-, y-, and z-axis calibration values
	mpu9250->magCalibration[0] =  (float)(rawData[0] - 128)/256.0f + 1.0f;        // Return x-axis sensitivity adjustment values, etc.
	mpu9250->magCalibration[1] =  (float)(rawData[1] - 128)/256.0f + 1.0f;
	mpu9250->magCalibration[2] =  (float)(rawData[2] - 128)/256.0f + 1.0f;

//	float _mRes = getMres(MFS_16BITS);
//	dest1[0] = (float) mpu9250->magbias[0] * _mRes * mpu9250->magCalibration[0];  // save mag biases in G for main program
//	dest1[1] = (float) mpu9250->magbias[1] * _mRes * mpu9250->magCalibration[1];
//	dest1[2] = (float) mpu9250->magbias[2] * _mRes * mpu9250->magCalibration[2];

	// Get soft iron correction estimate
	mag_scale[0]  = (mag_max[0] - mag_min[0])/2;  // get average x axis max chord length in counts
	mag_scale[1]  = (mag_max[1] - mag_min[1])/2;  // get average y axis max chord length in counts
	mag_scale[2]  = (mag_max[2] - mag_min[2])/2;  // get average z axis max chord length in counts

	float avg_rad = mag_scale[0] + mag_scale[1] + mag_scale[2];
	avg_rad /= 3.0;

//	dest2[0] = avg_rad/((float)mag_scale[0]);
//	dest2[1] = avg_rad/((float)mag_scale[1]);
//	dest2[2] = avg_rad/((float)mag_scale[2]);
*/
}

void mpu9250Driver_init(void *handle, imuDriver_t *mpu9250, ACCEL_FS_SEL Ascale, GYRO_FS_SEL Gscale, MSCALE_SEL Mscale)
{
	// wake up device
	uint8_t data = 0x00;
	mpu9250->write(handle, MPU9250_PWR_MGMT_1, &data, 1, false);
	delay(100); // Wait for all registers to reset

	// get stable time source
	data = 0x01;
	mpu9250->write(handle, MPU9250_PWR_MGMT_1, &data, 1, false);
	delay(200);

	// Configure Gyro and Thermometer
	// Disable FSYNC and set thermometer and gyro bandwidth to 41 and 42 Hz, respectively;
	// minimum delay time for this setting is 5.9 ms, which means sensor fusion update rates cannot
	// be higher than 1 / 0.0059 = 170 Hz
	// DLPF_CFG = bits 2:0 = 011; this limits the sample rate to 1000 Hz for both
	// With the MPU9250, it is possible to get gyro sample rates of 32 kHz (!), 8 kHz, or 1 kHz
	data = 0x03;
	mpu9250->write(handle, MPU9250_CONFIG, &data, 1, false);

	// Set sample rate = gyroscope output rate/(1 + SMPLRT_DIV)
	//data = 0x04;
	data = 0x19;
	mpu9250->write(handle, MPU9250_SMPLRT_DIV, &data, 1, false);	 // Use a 200 Hz rate; a rate consistent with the filter update rate
									 	 	 // determined inset in CONFIG above

	// Set gyroscope full scale range
	// Range selects FS_SEL and AFS_SEL are 0 - 3, so 2-bit values are left-shifted into positions 4:3
	uint8_t c;
	mpu9250->read(handle, MPU9250_GYRO_CONFIG, &c, 1, false); 		// get current GYRO_CONFIG register value
	// c = c & ~0xE0; // Clear self-test bits [7:5]
	c = c & ~0x02; // Clear Fchoice bits [1:0]
	c = c & ~0x18; // Clear AFS bits [4:3]
	c = c | Gscale << 3; // Set full scale range for the gyro
	// c =| 0x00; // Set Fchoice for the gyro to 11 by writing its inverse to bits 1:0 of GYRO_CONFIG
	mpu9250->write(handle, MPU9250_GYRO_CONFIG, &c, 1, false);		// Write new GYRO_CONFIG value to register

	// Set accelerometer full-scale range configuration
	mpu9250->read(handle, MPU9250_ACCEL_CONFIG, &c, 1, false);		// get current ACCEL_CONFIG register value
	// c = c & ~0xE0; // Clear self-test bits [7:5]
	c = c & ~0x18;  // Clear AFS bits [4:3]
	c = c | Ascale << 3; // Set full scale range for the accelerometer
	mpu9250->write(handle, MPU9250_ACCEL_CONFIG, &c, 1, false);	// Write new ACCEL_CONFIG register value

	// Set accelerometer sample rate configuration
	// It is possible to get a 4 kHz sample rate from the accelerometer by choosing 1 for
	// accel_fchoice_b bit [3]; in this case the bandwidth is 1.13 kHz
	mpu9250->read(handle, MPU9250_ACCEL_CONFIG2, &c, 1, false);	// get current ACCEL_CONFIG2 register value
	c = c & ~0x0F; // Clear accel_fchoice_b (bit 3) and A_DLPFG (bits [2:0])
	c = c | 0x03;  // Set accelerometer rate to 1 kHz and bandwidth to 41 Hz
	mpu9250->write(handle, MPU9250_ACCEL_CONFIG2, &c, 1, false);	// Write new ACCEL_CONFIG2 register value

	// The accelerometer, gyro, and thermometer are set to 1 kHz sample rates,
	// but all these rates are further reduced by a factor of 5 to 200 Hz because of the SMPLRT_DIV setting

//	data = 0x40;
//	mpu9250->write(handle, MPU9250_USER_CTRL, &data, 1, false);			// Enable I2C Master mode
//	data = 0x0D;
//	mpu9250->write(handle, MPU9250_I2C_MST_CTRL, &data, 1, false);			// I2C configuration STOP after each transaction, master I2C bus at 400 KHz
//	data = 0x81;
//	mpu9250->write(handle, MPU9250_I2C_MST_DELAY_CTRL, &data, 1, false);	// Use blocking data retreival and enable delay for mag sample rate mismatch
//	data = 0x01;
//	mpu9250->write(handle, MPU9250_I2C_SLV4_CTRL, &data, 1, false);		// Delay mag data retrieval to once every other accel/gyro data sample
	delay(100);

	// Configure Interrupts and Bypass Enable
	// Set interrupt pin active high, push-pull, hold interrupt pin level HIGH until interrupt cleared,
	// clear on read of INT_STATUS, and enable I2C_BYPASS_EN so additional chips
	// can join the I2C bus and all can be controlled by the Arduino as master
//	data = 0x10;
	data = 0x12;
	mpu9250->write(handle, MPU9250_INT_PIN_CFG, &data, 1, false);			// INT is 50 microsecond pulse and any read to clear
	data = 0x01;
	mpu9250->write(handle, MPU9250_INT_ENABLE, &data, 1, false);			// Enable data ready (bit 0) interrupt
	delay(100);

	mpu9250->aRes = getAres(Ascale);
	mpu9250->gRes = getGres(Gscale);
	delay(100);

	uint8_t rawData[3] = {0, 0, 0};
	mpu9250->read(handle, MPU9250_WHO_AM_I_AK8963, rawData, 1, true);

	data = 0x01;
	mpu9250->write(handle, MPU9250_AK8963_CNTL2, &data, 1, true); 		// Reset AK8963
	delay(100);

	data = 0x00;
	mpu9250->write(handle, MPU9250_AK8963_CNTL, &data, 1, true);        // Power down magnetometer
	delay(100);

	data = 0x0F;
	mpu9250->write(handle, MPU9250_AK8963_CNTL, &data, 1, true);		// Enter fuze mode
	delay(100);

	mpu9250->read(handle, MPU9250_AK8963_ASAX, rawData, 3, true);			 // Read the x-, y-, and z-axis calibration values
	mpu9250->magCalibration[0] =  (float)(rawData[0] - 128)/256.0f + 1.0f;    // Return x-axis sensitivity adjustment values, etc.
	mpu9250->magCalibration[1] =  (float)(rawData[1] - 128)/256.0f + 1.0f;
	mpu9250->magCalibration[2] =  (float)(rawData[2] - 128)/256.0f + 1.0f;

	data = 0x00;
	mpu9250->write(handle, MPU9250_AK8963_CNTL, &data, 1, true); 		// Power down magnetometer
	delay(100);

	// Configure the magnetometer for continuous read and highest resolution
	// set Mscale bit 4 to 1 (0) to enable 16 (14) bit resolution in CNTL register,
	// and enable continuous mode data acquisition Mmode (bits [3:0]), 0010 for 8 Hz and 0110 for 100 Hz sample rates
	data = (Mscale << 4 | Mmode);
	mpu9250->write(handle, MPU9250_AK8963_CNTL, &data, 1, true);		// Set magnetometer data resolution and sample ODR
	delay(100);

	mpu9250->mRes = getMres(Mscale);
}

// Accelerometer and gyroscope self test; check calibration wrt factory settings
void mpu9250Driver_selfTest(void *handle, imuDriver_t *mpu9250, float *destination) // Should return percent deviation from factory trim values, +/- 14 or less deviation is a pass
{
	uint8_t i2cData = 0x00;
	uint8_t rawData[6] = {0, 0, 0, 0, 0, 0};
	uint8_t selfTest[6];
	int32_t gAvg[3] = {0}, aAvg[3] = {0}, aSTAvg[3] = {0}, gSTAvg[3] = {0};
	float factoryTrim[6];
	uint8_t FS = 0;

	i2cData = 0x00;
	mpu9250->write(handle, MPU9250_SMPLRT_DIV, &i2cData, 1, false);    // Set gyro sample rate to 1 kHz
	i2cData = 0x02;
	mpu9250->write(handle, MPU9250_CONFIG, &i2cData, 1, false);        // Set gyro sample rate to 1 kHz and DLPF to 92 Hz
	i2cData = FS<<3;
	mpu9250->write(handle, MPU9250_GYRO_CONFIG, &i2cData, 1, false);  // Set full scale range for the gyro to 250 dps
	i2cData = 0x02;
	mpu9250->write(handle, MPU9250_ACCEL_CONFIG2, &i2cData, 1, false); // Set accelerometer rate to 1 kHz and bandwidth to 92 Hz
	i2cData = FS<<3;
	mpu9250->write(handle, MPU9250_ACCEL_CONFIG, &i2cData, 1, false); // Set full scale range for the accelerometer to 2 g

	for( int ii = 0; ii < 200; ii++) {  // get average current values of gyro and acclerometer

		mpu9250->read(handle, MPU9250_ACCEL_XOUT_H, rawData, 6, false);				// Read the six raw data registers into data array
		aAvg[0] += (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]) ;  // Turn the MSB and LSB into a signed 16-bit value
		aAvg[1] += (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]) ;
		aAvg[2] += (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]) ;

		mpu9250->read(handle, MPU9250_GYRO_XOUT_H, rawData, 6, false);			// Read the six raw data registers sequentially into data array
		gAvg[0] += (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]) ;  // Turn the MSB and LSB into a signed 16-bit value
		gAvg[1] += (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]) ;
		gAvg[2] += (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]) ;
	}

	for (int ii =0; ii < 3; ii++) {  // Get average of 200 values and store as average current readings
		aAvg[ii] /= 200;
		gAvg[ii] /= 200;
	}

	// Configure the accelerometer for self-test
	i2cData = 0xE0;
	mpu9250->write(handle, MPU9250_ACCEL_CONFIG, &i2cData, 1, false);  // Enable self test on all three axes and set accelerometer range to +/- 2 g
	mpu9250->write(handle, MPU9250_GYRO_CONFIG, &i2cData, 1, false);  // Enable self test on all three axes and set gyro range to +/- 250 degrees/s
	delay(25);  // Delay a while to let the device stabilize

	for( int ii = 0; ii < 200; ii++) {  // get average self-test values of gyro and acclerometer

		mpu9250->read(handle, MPU9250_ACCEL_XOUT_H, rawData, 6, false);		// Read the six raw data registers into data array
		aSTAvg[0] += (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]) ;  // Turn the MSB and LSB into a signed 16-bit value
		aSTAvg[1] += (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]) ;
		aSTAvg[2] += (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]) ;

		mpu9250->read(handle, MPU9250_GYRO_XOUT_H, rawData, 6, false);		// Read the six raw data registers sequentially into data array
		gSTAvg[0] += (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]) ;  // Turn the MSB and LSB into a signed 16-bit value
		gSTAvg[1] += (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]) ;
		gSTAvg[2] += (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]) ;
	}

	for (int ii =0; ii < 3; ii++) {  // Get average of 200 values and store as average self-test readings
		aSTAvg[ii] /= 200;
		gSTAvg[ii] /= 200;
	}

	// Configure the gyro and accelerometer for normal operation
	i2cData = 0x00;
	mpu9250->write(handle, MPU9250_ACCEL_CONFIG, &i2cData, 1, false);
	mpu9250->write(handle, MPU9250_GYRO_CONFIG, &i2cData, 1, false);
	delay(25);  // Delay a while to let the device stabilize

	// Retrieve accelerometer and gyro factory Self-Test Code from USR_Reg
	mpu9250->read(handle, MPU9250_SELF_TEST_X_ACCEL, &selfTest[0], 1, false);	// X-axis accel self-test results
	mpu9250->read(handle, MPU9250_SELF_TEST_Y_ACCEL, &selfTest[1], 1, false);	// Y-axis accel self-test results
	mpu9250->read(handle, MPU9250_SELF_TEST_Z_ACCEL, &selfTest[2], 1, false);	// Z-axis accel self-test results
	mpu9250->read(handle, MPU9250_SELF_TEST_X_GYRO, &selfTest[3], 1, false);	// X-axis gyro self-test results
	mpu9250->read(handle, MPU9250_SELF_TEST_Y_GYRO, &selfTest[4], 1, false);	// Y-axis gyro self-test results
	mpu9250->read(handle, MPU9250_SELF_TEST_Z_GYRO, &selfTest[5], 1, false);	// Z-axis gyro self-test results

	// Retrieve factory self-test value from self-test code reads
	factoryTrim[0] = (float)(2620/1<<FS)*(pow( 1.01 , ((float)selfTest[0] - 1.0) )); // FT[Xa] factory trim calculation
	factoryTrim[1] = (float)(2620/1<<FS)*(pow( 1.01 , ((float)selfTest[1] - 1.0) )); // FT[Ya] factory trim calculation
	factoryTrim[2] = (float)(2620/1<<FS)*(pow( 1.01 , ((float)selfTest[2] - 1.0) )); // FT[Za] factory trim calculation
	factoryTrim[3] = (float)(2620/1<<FS)*(pow( 1.01 , ((float)selfTest[3] - 1.0) )); // FT[Xg] factory trim calculation
	factoryTrim[4] = (float)(2620/1<<FS)*(pow( 1.01 , ((float)selfTest[4] - 1.0) )); // FT[Yg] factory trim calculation
	factoryTrim[5] = (float)(2620/1<<FS)*(pow( 1.01 , ((float)selfTest[5] - 1.0) )); // FT[Zg] factory trim calculation

	// Report results as a ratio of (STR - FT)/FT; the change from Factory Trim of the Self-Test Response
	// To get percent, must multiply by 100
	for (int i = 0; i < 3; i++) {
		destination[i]   = 100.0*((float)(aSTAvg[i] - aAvg[i]))/factoryTrim[i] - 100.;   // Report percent differences
		destination[i+3] = 100.0*((float)(gSTAvg[i] - gAvg[i]))/factoryTrim[i+3] - 100.; // Report percent differences
	}
}

void mpu9250Driver_getTempData(void *handle, imuDriver_t *mpu9250, float *temperature)
{
	uint8_t rawData[2] = {0, 0};

	mpu9250->read(handle, MPU9250_TEMP_OUT_H, rawData, 2, false);
	*temperature = ((((int16_t)rawData[0]) << 8) | rawData[1]) * 333.87 + 21.0;

	return;
}

bool mpu9250Driver_interruptHandler(void *handle, imuDriver_t *mpu9250, float *gyro, float *accel, float *mag)
{
	uint8_t rawData[14] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int16_t accelCount[3] = {0, 0, 0};
	int16_t gyroCount[3] = {0, 0, 0};
	int16_t magCount[3] = {0, 0, 0};
	bool ret = false;

	// If intPin goes high, all data registers have new data
	mpu9250->read(handle, MPU9250_INT_STATUS, rawData, 1, false);
	if (rawData[0] & 0x01) {  // On interrupt, check if data ready interrupt

		mpu9250->read(handle, MPU9250_ACCEL_XOUT_H, rawData, 14, false);		// Read the x/y/z adc values
		accelCount[0] = (((int16_t)rawData[0]) << 8) | rawData[1];
		accelCount[1] = (((int16_t)rawData[2]) << 8) | rawData[3];
		accelCount[2] = (((int16_t)rawData[4]) << 8) | rawData[5];

		// Now we'll calculate the accleration value into actual g's
		accel[0] = (float)accelCount[0] * mpu9250->aRes; // - accelBias[0];  // get actual g value, this depends on scale being set
		accel[1] = (float)accelCount[1] * mpu9250->aRes; // - accelBias[1];
		accel[2] = (float)accelCount[2] * mpu9250->aRes; // - accelBias[2];

		gyroCount[0] = (((int16_t)rawData[8]) << 8) | rawData[9];
		gyroCount[1] = (((int16_t)rawData[10]) << 8) | rawData[11];
		gyroCount[2] = (((int16_t)rawData[12]) << 8) | rawData[13];

		// Calculate the gyro value into actual degrees per second
		gyro[0] = (float)gyroCount[0] * mpu9250->gRes;  // get actual gyro value, this depends on scale being set
		gyro[1] = (float)gyroCount[1] * mpu9250->gRes;
		gyro[2] = (float)gyroCount[2] * mpu9250->gRes;
		ret = true;
	}

	mpu9250->read(handle, MPU9250_AK8963_ST1, rawData, 8, true);
	if (rawData[0] & 0x01) {
		//mpu9250->read(handle, MPU9250_AK8963_XOUT_L, rawData, 7, true);
		if(!(rawData[7] & 0x08)) {
			magCount[0] = ((int16_t)rawData[2] << 8) | rawData[1] ;  // Turn the MSB and LSB into a signed 16-bit value
			magCount[1] = ((int16_t)rawData[4] << 8) | rawData[3] ;  // Data stored as little Endian
			magCount[2] = ((int16_t)rawData[6] << 8) | rawData[5] ;
		}
//		readMagData(magCount);  // Read the x/y/z adc values
		mpu9250->magbias[0] = +470.;  // User environmental x-axis correction in milliGauss, should be automatically calculated
		mpu9250->magbias[1] = +120.;  // User environmental x-axis correction in milliGauss
		mpu9250->magbias[2] = +125.;  // User environmental x-axis correction in milliGauss

//		// Calculate the magnetometer values in milliGauss
//		// Include factory calibration per data sheet and user environmental corrections
		mag[0] = (float)magCount[0] * mpu9250->mRes * mpu9250->magCalibration[0] - mpu9250->magbias[0];  // get actual magnetometer value, this depends on scale being set
		mag[1] = (float)magCount[1] * mpu9250->mRes * mpu9250->magCalibration[1] - mpu9250->magbias[1];
		mag[2] = (float)magCount[2] * mpu9250->mRes * mpu9250->magCalibration[2] - mpu9250->magbias[2];
		ret = true;
	}
	return ret;
}
