/*
 * icm20649Driver.c
 *
 *  Created on: 14.03.2019
 *      Author: grli
 */


/**
 * @file 	mpu9250Driver.c
 * @author 	GrLiif(!initialized)
		return IMU_FAIL;
 * @date 	30.01.2019
 *
 * @brief
**/


/******************** include files ***********************/
#include "math.h"
#include "imu/driver/icm20649Driver.h"

/******************** defines *****************************/
#define ICM20649_BANK_0                  (0 << 7)     /**< Register bank 0 */
#define ICM20649_BANK_1                  (1 << 7)     /**< Register bank 1 */
#define ICM20649_BANK_2                  (2 << 7)     /**< Register bank 2 */
#define ICM20649_BANK_3                  (3 << 7)     /**< Register bank 3 */

/***********************/
/* Bank 0 register map */
/***********************/
#define ICM20649_REG_WHO_AM_I            (ICM20649_BANK_0 | 0x00)    /**< Device ID register                                     */

#define ICM20649_REG_USER_CTRL           (ICM20649_BANK_0 | 0x03)    /**< User control register                                  */
#define ICM20649_BIT_DMP_EN              0x80                        /**< DMP enable bit                                         */
#define ICM20649_BIT_FIFO_EN             0x40                        /**< FIFO enable bit                                        */
#define ICM20649_BIT_I2C_MST_EN          0x20                        /**< I2C master I/F enable bit                              */
#define ICM20649_BIT_I2C_IF_DIS          0x10                        /**< Disable I2C, enable SPI bit                            */
#define ICM20649_BIT_DMP_RST             0x08                        /**< DMP module reset bit                                   */
#define ICM20649_BIT_DIAMOND_DMP_RST     0x04                        /**< SRAM module reset bit                                  */

#define ICM20649_REG_LP_CONFIG           (ICM20649_BANK_0 | 0x05)    /**< Low Power mode config register                         */
#define ICM20649_BIT_I2C_MST_CYCLE       0x40                        /**< I2C master cycle mode enable                           */
#define ICM20649_BIT_ACCEL_CYCLE         0x20                        /**< Accelerometer cycle mode enable                        */
#define ICM20649_BIT_GYRO_CYCLE          0x10                        /**< Gyroscope cycle mode enable                            */

#define ICM20649_REG_PWR_MGMT_1          (ICM20649_BANK_0 | 0x06)    /**< Power Management 1 register                            */
#define ICM20649_BIT_H_RESET             0x80                        /**< Device reset bit                                       */
#define ICM20649_BIT_SLEEP               0x40                        /**< Sleep mode enable bit                                  */
#define ICM20649_BIT_LP_EN               0x20                        /**< Low Power feature enable bit                           */
#define ICM20649_BIT_TEMP_DIS            0x08                        /**< Temperature sensor disable bit                         */
#define ICM20649_BIT_CLK_PLL             0x01                        /**< Auto clock source selection setting                    */

#define ICM20649_REG_PWR_MGMT_2          (ICM20649_BANK_0 | 0x07)    /**< Power Management 2 register                            */
#define ICM20649_BIT_PWR_ACCEL_STBY      0x38                        /**< Disable accelerometer                                  */
#define ICM20649_BIT_PWR_GYRO_STBY       0x07                        /**< Disable gyroscope                                      */
#define ICM20649_BIT_PWR_ALL_OFF         0x7F                        /**< Disable both accel and gyro                            */

#define ICM20649_REG_INT_PIN_CFG         (ICM20649_BANK_0 | 0x0F)    /**< Interrupt Pin Configuration register                   */
#define ICM20649_BIT_INT_ACTL            0x80                        /**< Active low setting bit                                 */
#define ICM20649_BIT_INT_OPEN            0x40                        /**< Open collector onfiguration bit                        */
#define ICM20649_BIT_INT_LATCH_EN        0x20                        /**< Latch enable bit                                       */
#define ICM20649_BIT_INT_ANYRD       	 0x10                        /**< Any Read enable bit                                       */

#define ICM20649_REG_INT_ENABLE          (ICM20649_BANK_0 | 0x10)    /**< Interrupt Enable register                              */
#define ICM20649_BIT_WOM_INT_EN          0x08                        /**< Wake-up On Motion enable bit                           */

#define ICM20649_REG_INT_ENABLE_1        (ICM20649_BANK_0 | 0x11)    /**< Interrupt Enable 1 register                            */
#define ICM20649_BIT_RAW_DATA_0_RDY_EN   0x01                        /**< Raw data ready interrupt enable bit                    */

#define ICM20649_REG_INT_ENABLE_2        (ICM20649_BANK_0 | 0x12)    /**< Interrupt Enable 2 register                            */
#define ICM20649_BIT_FIFO_OVERFLOW_EN_0  0x01                        /**< FIFO overflow interrupt enable bit                     */

#define ICM20649_REG_INT_ENABLE_3        (ICM20649_BANK_0 | 0x13)    /**< Interrupt Enable 2 register                            */

#define ICM20649_REG_INT_STATUS          (ICM20649_BANK_0 | 0x19)    /**< Interrupt Status register                              */
#define ICM20649_BIT_WOM_INT             0x08                        /**< Wake-up on motion interrupt occured bit                */
#define ICM20649_BIT_PLL_RDY             0x04                        /**< PLL ready interrupt occured bit                        */

#define ICM20649_REG_INT_STATUS_1        (ICM20649_BANK_0 | 0x1A)    /**< Interrupt Status 1 register                            */
#define ICM20649_BIT_RAW_DATA_0_RDY_INT  0x01                        /**< Raw data ready interrupt occured bit                   */

#define ICM20649_REG_INT_STATUS_2        (ICM20649_BANK_0 | 0x1B)    /**< Interrupt Status 2 register                            */

#define ICM20649_REG_ACCEL_XOUT_H_SH     (ICM20649_BANK_0 | 0x2D)    /**< Accelerometer X-axis data high byte                    */
#define ICM20649_REG_ACCEL_XOUT_L_SH     (ICM20649_BANK_0 | 0x2E)    /**< Accelerometer X-axis data low byte                     */
#define ICM20649_REG_ACCEL_YOUT_H_SH     (ICM20649_BANK_0 | 0x2F)    /**< Accelerometer Y-axis data high byte                    */
#define ICM20649_REG_ACCEL_YOUT_L_SH     (ICM20649_BANK_0 | 0x30)    /**< Accelerometer Y-axis data low byte                     */
#define ICM20649_REG_ACCEL_ZOUT_H_SH     (ICM20649_BANK_0 | 0x31)    /**< Accelerometer Z-axis data high byte                    */
#define ICM20649_REG_ACCEL_ZOUT_L_SH     (ICM20649_BANK_0 | 0x32)    /**< Accelerometer Z-axis data low byte                     */

#define ICM20649_REG_GYRO_XOUT_H_SH      (ICM20649_BANK_0 | 0x33)    /**< Gyroscope X-axis data high byte                        */
#define ICM20649_REG_GYRO_XOUT_L_SH      (ICM20649_BANK_0 | 0x34)    /**< Gyroscope X-axis data low byte                         */
#define ICM20649_REG_GYRO_YOUT_H_SH      (ICM20649_BANK_0 | 0x35)    /**< Gyroscope Y-axis data high byte                        */
#define ICM20649_REG_GYRO_YOUT_L_SH      (ICM20649_BANK_0 | 0x36)    /**< Gyroscope Y-axis data low byte                         */
#define ICM20649_REG_GYRO_ZOUT_H_SH      (ICM20649_BANK_0 | 0x37)    /**< Gyroscope Z-axis data high byte                        */
#define ICM20649_REG_GYRO_ZOUT_L_SH      (ICM20649_BANK_0 | 0x38)    /**< Gyroscope Z-axis data low byte                         */

#define ICM20649_REG_TEMPERATURE_H       (ICM20649_BANK_0 | 0x39)    /**< Temperature data high byte                             */
#define ICM20649_REG_TEMPERATURE_L       (ICM20649_BANK_0 | 0x3A)    /**< Temperature data low byte                              */
#define ICM20649_REG_TEMP_CONFIG         (ICM20649_BANK_0 | 0x53)    /**< Temperature Configuration register                     */

#define ICM20649_REG_FIFO_EN_1           (ICM20649_BANK_0 | 0x66)    /**< FIFO Enable 1 register                                 */

#define ICM20649_REG_FIFO_EN_2           (ICM20649_BANK_0 | 0x67)    /**< FIFO Enable 2 register                                 */
#define ICM20649_BIT_ACCEL_FIFO_EN       0x10                        /**< Enable writing acceleration data to FIFO bit           */
#define ICM20649_BITS_GYRO_FIFO_EN       0x0E                        /**< Enable writing gyroscope data to FIFO bit              */

#define ICM20649_REG_FIFO_RST            (ICM20649_BANK_0 | 0x68)    /**< FIFO Reset register                                    */
#define ICM20649_REG_FIFO_MODE           (ICM20649_BANK_0 | 0x69)    /**< FIFO Mode register                                     */

#define ICM20649_REG_FIFO_COUNT_H        (ICM20649_BANK_0 | 0x70)    /**< FIFO data count high byte                              */
#define ICM20649_REG_FIFO_COUNT_L        (ICM20649_BANK_0 | 0x71)    /**< FIFO data count low byte                               */
#define ICM20649_REG_FIFO_R_W            (ICM20649_BANK_0 | 0x72)    /**< FIFO Read/Write register                               */

#define ICM20649_REG_DATA_RDY_STATUS     (ICM20649_BANK_0 | 0x74)    /**< Data Ready Status register                             */
#define ICM20649_BIT_RAW_DATA_0_RDY      0x01                        /**< Raw Data Ready bit                                     */

#define ICM20649_REG_FIFO_CFG            (ICM20649_BANK_0 | 0x76)    /**< FIFO Configuration register                            */
#define ICM20649_BIT_MULTI_FIFO_CFG      0x01                        /**< Interrupt status for each sensor is required           */
#define ICM20649_BIT_SINGLE_FIFO_CFG     0x00                        /**< Interrupt status for only a single sensor is required  */

/***********************/
/* Bank 1 register map */
/***********************/
#define ICM20649_REG_XA_OFFSET_H         (ICM20649_BANK_1 | 0x14)    /**< Acceleration sensor X-axis offset cancellation high byte  */
#define ICM20649_REG_XA_OFFSET_L         (ICM20649_BANK_1 | 0x15)    /**< Acceleration sensor X-axis offset cancellation low byte   */
#define ICM20649_REG_YA_OFFSET_H         (ICM20649_BANK_1 | 0x17)    /**< Acceleration sensor Y-axis offset cancellation high byte  */
#define ICM20649_REG_YA_OFFSET_L         (ICM20649_BANK_1 | 0x18)    /**< Acceleration sensor Y-axis offset cancellation low byte   */
#define ICM20649_REG_ZA_OFFSET_H         (ICM20649_BANK_1 | 0x1A)    /**< Acceleration sensor Z-axis offset cancellation high byte  */
#define ICM20649_REG_ZA_OFFSET_L         (ICM20649_BANK_1 | 0x1B)    /**< Acceleration sensor Z-axis offset cancellation low byte   */

#define ICM20649_REG_TIMEBASE_CORR_PLL   (ICM20649_BANK_1 | 0x28)    /**< PLL Timebase Correction register                          */

/***********************/
/* Bank 2 register map */
/***********************/
#define ICM20649_REG_GYRO_SMPLRT_DIV     (ICM20649_BANK_2 | 0x00)    /**< Gyroscope Sample Rate Divider regiser      */

#define ICM20649_REG_GYRO_CONFIG_1       (ICM20649_BANK_2 | 0x01)    /**< Gyroscope Configuration 1 register         */
#define ICM20649_BIT_GYRO_FCHOICE        0x01                        /**< Gyro Digital Low-Pass Filter enable bit    */
#define ICM20649_SHIFT_GYRO_FS_SEL       1                           /**< Gyro Full Scale Select bit shift           */
#define ICM20649_SHIFT_GYRO_DLPCFG       3                           /**< Gyro DLPF Config bit shift                 */
#define ICM20649_MASK_GYRO_FULLSCALE     0x06                        /**< Gyro Full Scale Select bitmask             */
#define ICM20649_MASK_GYRO_BW            0x39                        /**< Gyro Bandwidth Select bitmask              */
#define ICM20649_GYRO_FULLSCALE_500DPS   (0x00 << ICM20649_SHIFT_GYRO_FS_SEL)    /**< Gyro Full Scale = 250 deg/sec  */
#define ICM20649_GYRO_FULLSCALE_1000DPS  (0x01 << ICM20649_SHIFT_GYRO_FS_SEL)    /**< Gyro Full Scale = 500 deg/sec  */
#define ICM20649_GYRO_FULLSCALE_2000DPS  (0x02 << ICM20649_SHIFT_GYRO_FS_SEL)    /**< Gyro Full Scale = 1000 deg/sec */
#define ICM20649_GYRO_FULLSCALE_4000DPS  (0x03 << ICM20649_SHIFT_GYRO_FS_SEL)    /**< Gyro Full Scale = 2000 deg/sec */
#define ICM20649_GYRO_BW_12100HZ         (0x00 << ICM20649_SHIFT_GYRO_DLPCFG)                                     /**< Gyro Bandwidth = 12100 Hz */
#define ICM20649_GYRO_BW_360HZ           ( (0x07 << ICM20649_SHIFT_GYRO_DLPCFG) | ICM20649_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 360 Hz   */
#define ICM20649_GYRO_BW_200HZ           ( (0x00 << ICM20649_SHIFT_GYRO_DLPCFG) | ICM20649_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 200 Hz   */
#define ICM20649_GYRO_BW_150HZ           ( (0x01 << ICM20649_SHIFT_GYRO_DLPCFG) | ICM20649_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 150 Hz   */
#define ICM20649_GYRO_BW_120HZ           ( (0x02 << ICM20649_SHIFT_GYRO_DLPCFG) | ICM20649_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 120 Hz   */
#define ICM20649_GYRO_BW_51HZ            ( (0x03 << ICM20649_SHIFT_GYRO_DLPCFG) | ICM20649_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 51 Hz    */
#define ICM20649_GYRO_BW_24HZ            ( (0x04 << ICM20649_SHIFT_GYRO_DLPCFG) | ICM20649_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 24 Hz    */
#define ICM20649_GYRO_BW_12HZ            ( (0x05 << ICM20649_SHIFT_GYRO_DLPCFG) | ICM20649_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 12 Hz    */
#define ICM20649_GYRO_BW_6HZ             ( (0x06 << ICM20649_SHIFT_GYRO_DLPCFG) | ICM20649_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 6 Hz     */

#define ICM20649_REG_GYRO_CONFIG_2       (ICM20649_BANK_2 | 0x02)    /**< Gyroscope Configuration 2 register                     */
#define ICM20649_BIT_GYRO_CTEN           0x38                        /**< Gyroscope Self-Test Enable bits                        */

#define ICM20649_REG_XG_OFFS_USRH        (ICM20649_BANK_2 | 0x03)    /**< Gyroscope sensor X-axis offset cancellation high byte  */
#define ICM20649_REG_XG_OFFS_USRL        (ICM20649_BANK_2 | 0x04)    /**< Gyroscope sensor X-axis offset cancellation low byte   */
#define ICM20649_REG_YG_OFFS_USRH        (ICM20649_BANK_2 | 0x05)    /**< Gyroscope sensor Y-axis offset cancellation high byte  */
#define ICM20649_REG_YG_OFFS_USRL        (ICM20649_BANK_2 | 0x06)    /**< Gyroscope sensor Y-axis offset cancellation low byte   */
#define ICM20649_REG_ZG_OFFS_USRH        (ICM20649_BANK_2 | 0x07)    /**< Gyroscope sensor Z-axis offset cancellation high byte  */
#define ICM20649_REG_ZG_OFFS_USRL        (ICM20649_BANK_2 | 0x08)    /**< Gyroscope sensor Z-axis offset cancellation low byte   */

#define ICM20649_REG_ODR_ALIGN_EN        (ICM20649_BANK_2 | 0x09)    /**< Output Data Rate start time alignment                  */

#define ICM20649_REG_ACCEL_SMPLRT_DIV_1  (ICM20649_BANK_2 | 0x10)    /**< Acceleration Sensor Sample Rate Divider 1 register     */
#define ICM20649_REG_ACCEL_SMPLRT_DIV_2  (ICM20649_BANK_2 | 0x11)    /**< Acceleration Sensor Sample Rate Divider 2 register     */

#define ICM20649_REG_ACCEL_INTEL_CTRL    (ICM20649_BANK_2 | 0x12)    /**< Accelerometer Hardware Intelligence Control register   */
#define ICM20649_BIT_ACCEL_INTEL_EN      0x02                        /**< Wake-up On Motion enable bit                           */
#define ICM20649_BIT_ACCEL_INTEL_MODE    0x01                        /**< WOM algorithm selection bit                            */

#define ICM20649_REG_ACCEL_WOM_THR       (ICM20649_BANK_2 | 0x13)    /**< Wake-up On Motion Threshold register                   */

#define ICM20649_REG_ACCEL_CONFIG        (ICM20649_BANK_2 | 0x14)    /**< Accelerometer Configuration register                   */
#define ICM20649_BIT_ACCEL_FCHOICE       0x01                        /**< Accel Digital Low-Pass Filter enable bit               */
#define ICM20649_SHIFT_ACCEL_FS          1                           /**< Accel Full Scale Select bit shift                      */
#define ICM20649_SHIFT_ACCEL_DLPCFG      3                           /**< Accel DLPF Config bit shift                            */
#define ICM20649_MASK_ACCEL_FULLSCALE    0x06                        /**< Accel Full Scale Select bitmask                        */
#define ICM20649_MASK_ACCEL_BW           0x39                        /**< Accel Bandwidth Select bitmask                         */
#define ICM20649_ACCEL_FULLSCALE_4G      (0x00 << ICM20649_SHIFT_ACCEL_FS)    /**< Accel Full Scale = 2 g  */
#define ICM20649_ACCEL_FULLSCALE_8G      (0x01 << ICM20649_SHIFT_ACCEL_FS)    /**< Accel Full Scale = 4 g  */
#define ICM20649_ACCEL_FULLSCALE_16G     (0x02 << ICM20649_SHIFT_ACCEL_FS)    /**< Accel Full Scale = 8 g  */
#define ICM20649_ACCEL_FULLSCALE_30G     (0x03 << ICM20649_SHIFT_ACCEL_FS)    /**< Accel Full Scale = 16 g */
#define ICM20649_ACCEL_BW_1210HZ         (0x00 << ICM20649_SHIFT_ACCEL_DLPCFG)                                    /**< Accel Bandwidth = 1210 Hz  */
#define ICM20649_ACCEL_BW_470HZ          ( (0x07 << ICM20649_SHIFT_ACCEL_DLPCFG) | ICM20649_BIT_ACCEL_FCHOICE)    /**< Accel Bandwidth = 470 Hz   */
#define ICM20649_ACCEL_BW_246HZ          ( (0x00 << ICM20649_SHIFT_ACCEL_DLPCFG) | ICM20649_BIT_ACCEL_FCHOICE)    /**< Accel Bandwidth = 246 Hz   */
#define ICM20649_ACCEL_BW_111HZ          ( (0x02 << ICM20649_SHIFT_ACCEL_DLPCFG) | ICM20649_BIT_ACCEL_FCHOICE)    /**< Accel Bandwidth = 111 Hz   */
#define ICM20649_ACCEL_BW_50HZ           ( (0x03 << ICM20649_SHIFT_ACCEL_DLPCFG) | ICM20649_BIT_ACCEL_FCHOICE)    /**< Accel Bandwidth = 50 Hz    */
#define ICM20649_ACCEL_BW_24HZ           ( (0x04 << ICM20649_SHIFT_ACCEL_DLPCFG) | ICM20649_BIT_ACCEL_FCHOICE)    /**< Accel Bandwidth = 24 Hz    */
#define ICM20649_ACCEL_BW_12HZ           ( (0x05 << ICM20649_SHIFT_ACCEL_DLPCFG) | ICM20649_BIT_ACCEL_FCHOICE)    /**< Accel Bandwidth = 12 Hz    */
#define ICM20649_ACCEL_BW_6HZ            ( (0x06 << ICM20649_SHIFT_ACCEL_DLPCFG) | ICM20649_BIT_ACCEL_FCHOICE)    /**< Accel Bandwidth = 6 Hz     */

#define ICM20649_REG_ACCEL_CONFIG_2      (ICM20649_BANK_2 | 0x15)    /**< Accelerometer Configuration 2 register              */
#define ICM20649_BIT_ACCEL_CTEN          0x1C                        /**< Accelerometer Self-Test Enable bits                 */

/***********************/
/* Bank 3 register map */
/***********************/
#define ICM20649_REG_I2C_MST_ODR_CONFIG  (ICM20649_BANK_3 | 0x00)    /**< I2C Master Output Data Rate Configuration register  */

#define ICM20649_REG_I2C_MST_CTRL        (ICM20649_BANK_3 | 0x01)    /**< I2C Master Control register                         */
#define ICM20649_BIT_I2C_MST_P_NSR       0x10                        /**< Stop between reads enabling bit                     */

#define ICM20649_REG_I2C_MST_DELAY_CTRL  (ICM20649_BANK_3 | 0x02)    /**< I2C Master Delay Control register                   */
#define ICM20649_BIT_SLV0_DLY_EN         0x01                        /**< I2C Slave0 Delay Enable bit                         */
#define ICM20649_BIT_SLV1_DLY_EN         0x02                        /**< I2C Slave1 Delay Enable bit                         */
#define ICM20649_BIT_SLV2_DLY_EN         0x04                        /**< I2C Slave2 Delay Enable bit                         */
#define ICM20649_BIT_SLV3_DLY_EN         0x08                        /**< I2C Slave3 Delay Enable bit                         */

#define ICM20649_REG_I2C_SLV0_ADDR       (ICM20649_BANK_3 | 0x03)    /**< I2C Slave0 Physical Address register                */
#define ICM20649_REG_I2C_SLV0_REG        (ICM20649_BANK_3 | 0x04)    /**< I2C Slave0 Register Address register                */
#define ICM20649_REG_I2C_SLV0_CTRL       (ICM20649_BANK_3 | 0x05)    /**< I2C Slave0 Control register                         */
#define ICM20649_REG_I2C_SLV0_DO         (ICM20649_BANK_3 | 0x06)    /**< I2C Slave0 Data Out register                        */

#define ICM20649_REG_I2C_SLV1_ADDR       (ICM20649_BANK_3 | 0x07)    /**< I2C Slave1 Physical Address register                */
#define ICM20649_REG_I2C_SLV1_REG        (ICM20649_BANK_3 | 0x08)    /**< I2C Slave1 Register Address register                */
#define ICM20649_REG_I2C_SLV1_CTRL       (ICM20649_BANK_3 | 0x09)    /**< I2C Slave1 Control register                         */
#define ICM20649_REG_I2C_SLV1_DO         (ICM20649_BANK_3 | 0x0A)    /**< I2C Slave1 Data Out register                        */

#define ICM20649_REG_I2C_SLV2_ADDR       (ICM20649_BANK_3 | 0x0B)    /**< I2C Slave2 Physical Address register                */
#define ICM20649_REG_I2C_SLV2_REG        (ICM20649_BANK_3 | 0x0C)    /**< I2C Slave2 Register Address register                */
#define ICM20649_REG_I2C_SLV2_CTRL       (ICM20649_BANK_3 | 0x0D)    /**< I2C Slave2 Control register                         */
#define ICM20649_REG_I2C_SLV2_DO         (ICM20649_BANK_3 | 0x0E)    /**< I2C Slave2 Data Out register                        */

#define ICM20649_REG_I2C_SLV3_ADDR       (ICM20649_BANK_3 | 0x0F)    /**< I2C Slave3 Physical Address register                */
#define ICM20649_REG_I2C_SLV3_REG        (ICM20649_BANK_3 | 0x10)    /**< I2C Slave3 Register Address register                */
#define ICM20649_REG_I2C_SLV3_CTRL       (ICM20649_BANK_3 | 0x11)    /**< I2C Slave3 Control register                         */
#define ICM20649_REG_I2C_SLV3_DO         (ICM20649_BANK_3 | 0x12)    /**< I2C Slave3 Data Out register                        */

#define ICM20649_REG_I2C_SLV4_ADDR       (ICM20649_BANK_3 | 0x13)    /**< I2C Slave4 Physical Address register                */
#define ICM20649_REG_I2C_SLV4_REG        (ICM20649_BANK_3 | 0x14)    /**< I2C Slave4 Register Address register                */
#define ICM20649_REG_I2C_SLV4_CTRL       (ICM20649_BANK_3 | 0x15)    /**< I2C Slave4 Control register                         */
#define ICM20649_REG_I2C_SLV4_DO         (ICM20649_BANK_3 | 0x16)    /**< I2C Slave4 Data Out register                        */
#define ICM20649_REG_I2C_SLV4_DI         (ICM20649_BANK_3 | 0x17)    /**< I2C Slave4 Data In register                         */

#define ICM20649_BIT_I2C_SLV_EN          0x80                        /**< I2C Slave Enable bit                                */
#define ICM20649_BIT_I2C_BYTE_SW         0x40                        /**< I2C Slave Byte Swap enable bit                      */
#define ICM20649_BIT_I2C_REG_DIS         0x20                        /**< I2C Slave Do Not Write Register Value bit           */
#define ICM20649_BIT_I2C_GRP             0x10                        /**< I2C Slave Group bit                                 */
#define ICM20649_BIT_I2C_READ            0x80                        /**< I2C Slave R/W bit                                   */

/* Register common for all banks */
#define ICM20649_REG_BANK_SEL            0x7F                        /**< Bank Select register                                */

/******************** prototypes **************************/
static int8_t icm20649_write(void *handle, imuDriver_t *icm20649, uint16_t reg_addr, uint8_t *reg_data, uint8_t cnt);
static int8_t icm20649_read(void *handle, imuDriver_t *icm20649, uint16_t reg_addr, uint8_t *reg_data, uint8_t cnt);
static void icm20649_sensorEnable(void *handle, imuDriver_t *icm20649, bool accel, bool gyro, bool temp);
static void icm20649_sampleRateSet(void *handle, imuDriver_t *icm20649, float sampleRate);
static void icm20649_interruptEnable(void *handle, imuDriver_t *icm20649, bool dataReadyEnable, bool womEnable);
static void icm20649_lowPowerModeEnter(void *handle, imuDriver_t *icm20649, bool enAccel, bool enGyro, bool enTemp);
static float icm20649_getGres(void *handle, imuDriver_t *icm20649);
static float icm20649_getAres(void *handle, imuDriver_t *icm20649);

/******************** struct data type ********************/

/******************** private variables********************/

/******************** private function ********************/
int8_t icm20649_write(void *handle, imuDriver_t *icm20649, uint16_t reg_addr, uint8_t *reg_data, uint8_t cnt)
{
	uint8_t bank = ((reg_addr >> 7) << 4);
	if(icm20649->write(handle, ICM20649_REG_BANK_SEL, &bank, 1, false) != 0){
		return -1;
	}

	if(icm20649->write(handle, (reg_addr & 0x7F), reg_data, cnt, false) != 0) {
		return -1;
	}

	return 0;
}

int8_t icm20649_read(void *handle, imuDriver_t *icm20649, uint16_t reg_addr, uint8_t *reg_data, uint8_t cnt)
{
	uint8_t bank = ((reg_addr >> 7) << 4);
	if(icm20649->write(handle, ICM20649_REG_BANK_SEL, &bank, 1, false) != 0){
		return -1;
	}

	if(icm20649->read(handle, (reg_addr & 0x7F), reg_data, cnt, false) != 0) {
		return -1;
	}

	return 0;
}

void icm20649_sensorEnable(void *handle, imuDriver_t *icm20649, bool accel, bool gyro, bool temp)
{
	uint8_t pwrManagement1;
	uint8_t pwrManagement2;

	icm20649_read(handle, icm20649, ICM20649_REG_PWR_MGMT_1, &pwrManagement1, 1);
	pwrManagement2 = 0;

	/* To enable the accelerometer clear the DISABLE_ACCEL bits in PWR_MGMT_2 */
	if ( accel ) {
		pwrManagement2 &= ~(ICM20649_BIT_PWR_ACCEL_STBY);
	} else {
		pwrManagement2 |= ICM20649_BIT_PWR_ACCEL_STBY;
	}

	/* To enable gyro clear the DISABLE_GYRO bits in PWR_MGMT_2 */
	if ( gyro ) {
		pwrManagement2 &= ~(ICM20649_BIT_PWR_GYRO_STBY);
	} else {
		pwrManagement2 |= ICM20649_BIT_PWR_GYRO_STBY;
	}

	/* To enable the temperature sensor clear the TEMP_DIS bit in PWR_MGMT_1 */
	if ( temp ) {
		pwrManagement1 &= ~(ICM20649_BIT_TEMP_DIS);
	} else {
		pwrManagement1 |= ICM20649_BIT_TEMP_DIS;
	}

	/* Write back the modified values */
	icm20649_write(handle, icm20649, ICM20649_REG_PWR_MGMT_1, &pwrManagement1, 1);
	icm20649_write(handle, icm20649, ICM20649_REG_PWR_MGMT_2, &pwrManagement2, 1);

	return;
}

void icm20649_sampleRateSet(void *handle, imuDriver_t *icm20649, float sampleRate)
{
	uint8_t gyroDiv;
	uint16_t accelDiv;
	float gyroSampleRate;
	float accelSampleRate;

	/* Calculate the sample rate divider */
	gyroSampleRate = (1125.0 / sampleRate) - 1.0;
	accelSampleRate = (1125.0 / sampleRate) - 1.0;

	/* Check if it fits in the divider register */
	if ( gyroSampleRate > 255.0 ) {
		gyroSampleRate = 255.0;
	}

	if ( gyroSampleRate < 0 ) {
		gyroSampleRate = 0.0;
	}

	if ( accelSampleRate > 4095.0 ) {
		accelSampleRate = 4095.0;
	}

	if ( accelSampleRate < 0 ) {
		accelSampleRate = 0.0;
	}

	/* Write the value to the register */
	gyroDiv = (uint8_t) gyroSampleRate;
	icm20649_write(handle, icm20649, ICM20649_REG_GYRO_SMPLRT_DIV, &gyroDiv, 1);

	accelDiv = (uint16_t) accelSampleRate;
	icm20649_write(handle, icm20649, ICM20649_REG_ACCEL_SMPLRT_DIV_1, (uint8_t *)(accelDiv >> 8), 1);
	icm20649_write(handle, icm20649, ICM20649_REG_ACCEL_SMPLRT_DIV_2, (uint8_t *)(accelDiv & 0xFF), 1);

	/* Calculate the actual sample rate from the divider value */
	gyroSampleRate = 1125.0 / (gyroDiv + 1);
	accelSampleRate = 1125.0 / (accelDiv + 1);
}

void icm20649_interruptEnable(void *handle, imuDriver_t *icm20649, bool dataReadyEnable, bool womEnable)
{
	uint8_t intEnable;

	/* All interrupts disabled by default */
	intEnable = 0;

	/* Enable one or both of the interrupt sources if required */
	if ( womEnable ) {
		intEnable = ICM20649_BIT_WOM_INT_EN;
	}
	/* Write value to register */
	icm20649_write(handle, icm20649, ICM20649_REG_INT_ENABLE, &intEnable, 1);

	/* All interrupts disabled by default */
	intEnable = 0;
	if ( dataReadyEnable ) {
		intEnable = ICM20649_BIT_RAW_DATA_0_RDY_EN;
	}

	/* Write value to register */
	icm20649_write(handle, icm20649, ICM20649_REG_INT_ENABLE_1, &intEnable, 1);

	return;
}

void icm20649_sleepModeEnable(void *handle, imuDriver_t *icm20649, bool enable)
{
	uint8_t reg;

	icm20649_read(handle, icm20649, ICM20649_REG_PWR_MGMT_1, &reg, 1);

	if ( enable ) {
		/* Sleep: set the SLEEP bit */
		reg |= ICM20649_BIT_SLEEP;
	} else {
		/* Wake up: clear the SLEEP bit */
		reg &= ~(ICM20649_BIT_SLEEP);
	}

	icm20649_write(handle, icm20649, ICM20649_REG_PWR_MGMT_1, &reg, 1);

	return;
}

void icm20649_lowPowerModeEnter(void *handle, imuDriver_t *icm20649, bool enAccel, bool enGyro, bool enTemp)
{
	uint8_t data;

	icm20649_read(handle, icm20649, ICM20649_REG_PWR_MGMT_1, &data, 1);

	if ( enAccel || enGyro || enTemp ) {
		/* Make sure that the chip is not in sleep */
		icm20649_sleepModeEnable(handle, icm20649, false);

		/* And in continuous mode */
		data = 0x00;
		icm20649_write(handle, icm20649, ICM20649_REG_LP_CONFIG, &data, 1);

		/* Enable the accelerometer and the gyroscope*/

		icm20649_sensorEnable(handle, icm20649, enAccel, enGyro, enTemp);
		delay(50);

		/* Enable cycle mode */
		data = ICM20649_BIT_ACCEL_CYCLE | ICM20649_BIT_GYRO_CYCLE;
		icm20649_write(handle, icm20649, ICM20649_REG_LP_CONFIG, &data, 1);

		/* Set the LP_EN bit to enable low power mode */
		data |= ICM20649_BIT_LP_EN;
	} else {
		/* Enable continuous mode */
		data = 0x00;
		icm20649_write(handle, icm20649, ICM20649_REG_LP_CONFIG, &data, 1);

		/* Clear the LP_EN bit to disable low power mode */
		data &= ~ICM20649_BIT_LP_EN;
	}

	/* Write the updated value to the PWR_MGNT_1 register */
	icm20649_write(handle, icm20649, ICM20649_REG_PWR_MGMT_1, &data, 1);

	return;
}

float icm20649_getAres(void *handle, imuDriver_t *icm20649)
{
	uint8_t reg;

	/* Read the actual acceleration full scale setting */
	icm20649_read(handle, icm20649, ICM20649_REG_ACCEL_CONFIG, &reg, 1);
	reg &= ICM20649_MASK_ACCEL_FULLSCALE;

	/* Calculate the resolution */
	switch ( reg ) {
		case ICM20649_ACCEL_FULLSCALE_4G:
			return 4.0 / 32768.0;

		case ICM20649_ACCEL_FULLSCALE_8G:
			return 8.0 / 32768.0;

		case ICM20649_ACCEL_FULLSCALE_16G:
			return 16.0 / 32768.0;

		case ICM20649_ACCEL_FULLSCALE_30G:
			return 30.0 / 32768.0;
	}
	return 0;
}

float icm20649_getGres(void *handle, imuDriver_t *icm20649)
{
	uint8_t reg;

	/* Read the actual gyroscope full scale setting */
	icm20649_read(handle, icm20649, ICM20649_REG_GYRO_CONFIG_1, &reg, 1);
	reg &= ICM20649_MASK_GYRO_FULLSCALE;

	/* Calculate the resolution */
	switch ( reg ) {
		case ICM20649_GYRO_FULLSCALE_500DPS:
			return 500.0 / 32768.0;

		case ICM20649_GYRO_FULLSCALE_1000DPS:
			return 1000.0 / 32768.0;

		case ICM20649_GYRO_FULLSCALE_2000DPS:
			return 2000.0 / 32768.0;

		case ICM20649_GYRO_FULLSCALE_4000DPS:
			return 4000.0 / 32768.0;
	}
	return 0;
}

/******************** interrupt function ******************/

/******************** global function *********************/
int8_t icm20649Driver_getID(void *handle, imuDriver_t *icm20649, uint8_t *id)
{
	return icm20649_read(handle, icm20649, ICM20649_REG_WHO_AM_I, id, 1);
}

void icm20649Driver_reset(void *handle, imuDriver_t *icm20649)
{
	uint8_t i2cData = ICM20649_BIT_H_RESET;
	icm20649_write(handle, icm20649, ICM20649_REG_PWR_MGMT_1, &i2cData, 1);
	delay(100); 		// Wait for all registers to reset
}

// Function which accumulates gyro and accelerometer data after device initialization. It calculates the average
// of the at-rest readings and then loads the resulting offsets into accelerometer and gyro bias registers.
void icm20649Driver_calibrate(void *handle, imuDriver_t *icm20649)
{
	uint8_t id = 0;
	uint8_t i2cData = 0;

	uint8_t data[12];
	uint16_t i, packetCount, fifoCount;
	int32_t gyroBias[3] = { 0, 0, 0 };
	int32_t accelBias[3] = { 0, 0, 0 };
	int32_t accelTemp[3];
	int32_t gyroTemp[3];
	int32_t accelBiasFactory[3];
	int32_t gyroBiasStored[3];
	float gyroRes, accelRes;

	icm20649Driver_reset(handle, icm20649);
	delay(100);

	icm20649Driver_getID(handle, icm20649, &id);
	if(id != ICM20649_DEVICE_ID)
		return;

	/* Auto selects the best available clock source Â– PLL if ready, else use the Internal oscillator */
	i2cData = ICM20649_BIT_CLK_PLL;
	icm20649_write(handle, icm20649, ICM20649_REG_PWR_MGMT_1, &i2cData, 1);

	/* PLL startup time - maybe it is too long but better be on the safe side, no spec in the datasheet */
	delay(30);

	/* INT pin: active low, open drain, IT status read clears. It seems that latched mode does not work, the INT pin cannot be cleared if set */
	i2cData = ICM20649_BIT_INT_ACTL | ICM20649_BIT_INT_OPEN | ICM20649_BIT_INT_ANYRD;
	icm20649_write(handle, icm20649, ICM20649_REG_INT_PIN_CFG, &i2cData, 1);

	/* Enable the accelerometer and the gyro */
	icm20649_sensorEnable(handle, icm20649, true, true, false);

	/* Set 1kHz sample rate */
	icm20649_sampleRateSet(handle, icm20649, 1100.0);

	/* 246Hz BW for the accelerometer and 200Hz for the gyroscope */
	/* Read the GYRO_CONFIG_1 register */
	icm20649_read(handle, icm20649, ICM20649_REG_ACCEL_CONFIG, &i2cData, 1);
	i2cData &= ~(ICM20649_MASK_ACCEL_BW);
	/* Write the new bandwidth value to the gyro config register */
	i2cData |= (ICM20649_ACCEL_BW_246HZ & ICM20649_MASK_ACCEL_BW);
	icm20649_write(handle, icm20649, ICM20649_REG_ACCEL_CONFIG, &i2cData, 1);
	/* Read the GYRO_CONFIG_1 register */
	icm20649_read(handle, icm20649, ICM20649_REG_GYRO_CONFIG_1, &i2cData, 1);
	i2cData &= ~(ICM20649_MASK_GYRO_BW);
	/* Write the new bandwidth value to the gyro config register */
	i2cData |= (ICM20649_GYRO_BW_12HZ & ICM20649_MASK_GYRO_BW);
	icm20649_write(handle, icm20649, ICM20649_REG_GYRO_CONFIG_1, &i2cData, 1);

	/* Set the most sensitive range: 2G full scale and 250dps full scale */
	uint8_t accelFs = ICM20649_ACCEL_FULLSCALE_4G & ICM20649_MASK_ACCEL_FULLSCALE;
	icm20649_read(handle, icm20649, ICM20649_REG_ACCEL_CONFIG, &i2cData, 1);
	i2cData &= ~(ICM20649_MASK_ACCEL_FULLSCALE);
	i2cData |= accelFs;
	icm20649_write(handle, icm20649, ICM20649_REG_ACCEL_CONFIG, &i2cData, 1);

	uint8_t gyroFs = GYRO_FS_SEL_500dps & ICM20649_MASK_GYRO_FULLSCALE;
	icm20649_read(handle, icm20649, ICM20649_REG_GYRO_CONFIG_1, &i2cData, 1);
	i2cData &= ~(ICM20649_MASK_GYRO_FULLSCALE);
	i2cData |= gyroFs;
	icm20649_write(handle, icm20649, ICM20649_REG_GYRO_CONFIG_1, &i2cData, 1);

	/* Retrieve the resolution per bit */
	accelRes = icm20649_getAres(handle, icm20649);
	gyroRes = icm20649_getGres(handle, icm20649);

	/* The accel sensor needs max 30ms, the gyro max 35ms to fully start */
	/* Experiments show that the gyro needs more time to get reliable results */
	delay(50);

	/* Disable the FIFO */
	i2cData = ICM20649_BIT_FIFO_EN;
	icm20649_write(handle, icm20649, ICM20649_REG_USER_CTRL, &i2cData, 1);
	i2cData = 0x0F;
	icm20649_write(handle, icm20649, ICM20649_REG_FIFO_MODE, &i2cData, 1);

	/* Enable accelerometer and gyro to store the data in FIFO */
	i2cData = ICM20649_BIT_ACCEL_FIFO_EN | ICM20649_BITS_GYRO_FIFO_EN;
	icm20649_write(handle, icm20649, ICM20649_REG_FIFO_EN_2, &i2cData, 1);

	/* Reset the FIFO */
	i2cData = 0x0F;
	icm20649_write(handle, icm20649, ICM20649_REG_FIFO_RST, &i2cData, 1);
	i2cData = 0x00;
	icm20649_write(handle, icm20649, ICM20649_REG_FIFO_RST, &i2cData, 1);

	/* Enable the FIFO */
	i2cData = ICM20649_BIT_FIFO_EN;
	icm20649_write(handle, icm20649, ICM20649_REG_USER_CTRL, &i2cData, 1);

	/* The max FIFO size is 4096 bytes, one set of measurements takes 12 bytes */
	/* (3 axes, 2 sensors, 2 bytes each value ) 340 samples use 4080 bytes of FIFO */
	/* Loop until at least 4080 samples gathered */
	fifoCount = 0;
	while ( fifoCount < 4080 ) {
		delay(5);
		/* Read FIFO sample count */
		icm20649_read(handle, icm20649, ICM20649_REG_FIFO_COUNT_H, data, 2);
		/* Convert to a 16 bit value */
		fifoCount = ( (uint16_t) (data[0] << 8) | data[1]);
	}

	/* Disable accelerometer and gyro to store the data in FIFO */
	i2cData = 0x00;
	icm20649_write(handle, icm20649, ICM20649_REG_FIFO_EN_2, &i2cData, 1);

	/* Read FIFO sample count */
	icm20649_read(handle, icm20649, ICM20649_REG_FIFO_COUNT_H, data, 2);

	/* Convert to a 16 bit value */
	fifoCount = ( (uint16_t) (data[0] << 8) | data[1]);
	/* Calculate the number of data sets (3 axis of accel an gyro, two bytes each = 12 bytes) */
	packetCount = fifoCount / 12;

	/* Retrieve the data from the FIFO */
	for ( i = 0; i < packetCount; i++ ) {
		icm20649_read(handle, icm20649, ICM20649_REG_FIFO_R_W, data, 12);
		/* Convert to 16 bit signed accel and gyro x,y and z values */
		accelTemp[0] = ( (int16_t) (data[0] << 8) | data[1]);
		accelTemp[1] = ( (int16_t) (data[2] << 8) | data[3]);
		accelTemp[2] = ( (int16_t) (data[4] << 8) | data[5]);
		gyroTemp[0] = ( (int16_t) (data[6] << 8) | data[7]);
		gyroTemp[1] = ( (int16_t) (data[8] << 8) | data[9]);
		gyroTemp[2] = ( (int16_t) (data[10] << 8) | data[11]);

		/* Sum the values */
		accelBias[0] += accelTemp[0];
		accelBias[1] += accelTemp[1];
		accelBias[2] += accelTemp[2];
		gyroBias[0] += gyroTemp[0];
		gyroBias[1] += gyroTemp[1];
		gyroBias[2] += gyroTemp[2];
	}

	/* Divide by packet count to get the average */
	accelBias[0] /= packetCount;
	accelBias[1] /= packetCount;
	accelBias[2] /= packetCount;
	gyroBias[0] /= packetCount;
	gyroBias[1] /= packetCount;
	gyroBias[2] /= packetCount;

	/* Acceleormeter: add or remove (depending on the orientation of the chip) 1G (gravity) from the Z axis value */
	if ( accelBias[2] > 0L ) {
		accelBias[2] -= (int32_t) (1.0 / accelRes);
	} else {
		accelBias[2] += (int32_t) (1.0 / accelRes);
	}

	/* Read stored gyro trim values. After reset these values are all 0 */
	icm20649_read(handle, icm20649, ICM20649_REG_XG_OFFS_USRH, data, 2);
	gyroBiasStored[0] = ( (int16_t) (data[0] << 8) | data[1]);
	icm20649_read(handle, icm20649, ICM20649_REG_YG_OFFS_USRH, data, 2);
	gyroBiasStored[1] = ( (int16_t) (data[0] << 8) | data[1]);
	icm20649_read(handle, icm20649, ICM20649_REG_ZG_OFFS_USRH, data, 2);
	gyroBiasStored[2] = ( (int16_t) (data[0] << 8) | data[1]);

	/* The gyro bias should be stored in 1000dps full scaled format. We measured in 250dps to get */
	/* the best sensitivity, so need to divide by 4 */
	/* Substract from the stored calibration value */
	gyroBiasStored[0] -= gyroBias[0] / 4;
	gyroBiasStored[1] -= gyroBias[1] / 4;
	gyroBiasStored[2] -= gyroBias[2] / 4;

	/* Split the values into two bytes */
	data[0] = (gyroBiasStored[0] >> 8) & 0xFF;
	data[1] = (gyroBiasStored[0]) & 0xFF;
	data[2] = (gyroBiasStored[1] >> 8) & 0xFF;
	data[3] = (gyroBiasStored[1]) & 0xFF;
	data[4] = (gyroBiasStored[2] >> 8) & 0xFF;
	data[5] = (gyroBiasStored[2]) & 0xFF;

	/* Write the  gyro bias values to the chip */
	i2cData = data[0];
	icm20649_write(handle, icm20649, ICM20649_REG_XG_OFFS_USRH, &i2cData, 1);
	i2cData = data[1];
	icm20649_write(handle, icm20649, ICM20649_REG_XG_OFFS_USRL, &i2cData, 1);
	i2cData = data[2];
	icm20649_write(handle, icm20649, ICM20649_REG_YG_OFFS_USRH, &i2cData, 1);
	i2cData = data[3];
	icm20649_write(handle, icm20649, ICM20649_REG_YG_OFFS_USRL, &i2cData, 1);
	i2cData = data[4];
	icm20649_write(handle, icm20649, ICM20649_REG_ZG_OFFS_USRH, &i2cData, 1);
	i2cData = data[5];
	icm20649_write(handle, icm20649, ICM20649_REG_ZG_OFFS_USRL, &i2cData, 1);

	/* Calculate the accelerometer bias values to store in the hardware accelerometer bias registers. These registers contain */
	/* factory trim values which must be added to the calculated accelerometer biases; on boot up these registers will hold */
	/* non-zero values. In addition, bit 0 of the lower byte must be preserved since it is used for temperature */
	/* compensation calculations(? the datasheet is not clear). Accelerometer bias registers expect bias input */
	/* as 2048 LSB per g, so that the accelerometer biases calculated above must be divided by 8. */

	/* Read factory accelerometer trim values */

	icm20649_read(handle, icm20649, ICM20649_REG_XA_OFFSET_H, data, 2);
	accelBiasFactory[0] = ( (int16_t) (data[0] << 8) | data[1]);
	icm20649_read(handle, icm20649, ICM20649_REG_YA_OFFSET_H, data, 2);
	accelBiasFactory[1] = ( (int16_t) (data[0] << 8) | data[1]);
	icm20649_read(handle, icm20649, ICM20649_REG_ZA_OFFSET_H, data, 2);
	accelBiasFactory[2] = ( (int16_t) (data[0] << 8) | data[1]);

	/* Construct total accelerometer bias, including calculated average accelerometer bias from above */
	/* Scale the 2g full scale (most sensitive range) results to 16g full scale - divide by 8 */
	/* Clear the last bit (temperature compensation? - the datasheet is not clear) */
	/* Substract from the factory calibration value */

	accelBiasFactory[0] -= ( (accelBias[0] / 8) & ~1);
	accelBiasFactory[1] -= ( (accelBias[1] / 8) & ~1);
	accelBiasFactory[2] -= ( (accelBias[2] / 8) & ~1);

	/* Split the values into two bytes */
	data[0] = (accelBiasFactory[0] >> 8) & 0xFF;
	data[1] = (accelBiasFactory[0]) & 0xFF;
	data[2] = (accelBiasFactory[1] >> 8) & 0xFF;
	data[3] = (accelBiasFactory[1]) & 0xFF;
	data[4] = (accelBiasFactory[2] >> 8) & 0xFF;
	data[5] = (accelBiasFactory[2]) & 0xFF;

	/* Store them in the accelerometer offset registers */
	i2cData = data[0];
	icm20649_write(handle, icm20649, ICM20649_REG_XA_OFFSET_H, &i2cData, 1);
	i2cData = data[1];
	icm20649_write(handle, icm20649, ICM20649_REG_XA_OFFSET_L, &i2cData, 1);
	i2cData = data[2];
	icm20649_write(handle, icm20649, ICM20649_REG_YA_OFFSET_H, &i2cData, 1);
	i2cData = data[3];
	icm20649_write(handle, icm20649, ICM20649_REG_YA_OFFSET_L, &i2cData, 1);
	i2cData = data[4];
	icm20649_write(handle, icm20649, ICM20649_REG_ZA_OFFSET_H, &i2cData, 1);
	i2cData = data[5];
	icm20649_write(handle, icm20649, ICM20649_REG_ZA_OFFSET_L, &i2cData, 1);

	/* Turn off FIFO */
	i2cData = 0x00;
	icm20649_write(handle, icm20649, ICM20649_REG_USER_CTRL, &i2cData, 1);

	/* Disable all sensors */
	icm20649_sensorEnable(handle, icm20649, false, false, false);

	icm20649->aRes = accelRes;
	icm20649->gRes = gyroRes;
	icm20649->mRes = 0;
	icm20649->accelBias[0] = 0;
	icm20649->accelBias[1] = 0;
	icm20649->accelBias[2] = 0;
	icm20649->gyroBias[0] = 0;
	icm20649->gyroBias[1] = 0;
	icm20649->gyroBias[2] = 0;
}

void icm20649Driver_init(void *handle, imuDriver_t *icm20649, ACCEL_FS_SEL Ascale, GYRO_FS_SEL Gscale, MSCALE_SEL Mscale)
{
	uint8_t i2cData;
	uint8_t rawData[4];
	//uint32_t itStatus;

	/* Clear the interrupts */
	icm20649_read(handle, icm20649, ICM20649_REG_INT_STATUS, rawData, 4);

	/* Enable accel sensor */
	icm20649_sensorEnable(handle, icm20649, true, true, false);

	/* Set sample rate */
	icm20649_sampleRateSet(handle, icm20649, 225.0);

	/* Filter bandwidth: 12kHz, otherwise the results may be off */
	/* Read the GYRO_CONFIG_1 register */
	icm20649_read(handle, icm20649, ICM20649_REG_ACCEL_CONFIG, &i2cData, 1);
	i2cData &= ~(ICM20649_MASK_ACCEL_BW);
	/* Write the new bandwidth value to the gyro config register */
	i2cData |= (ICM20649_ACCEL_BW_1210HZ & ICM20649_MASK_ACCEL_BW);
	icm20649_write(handle, icm20649, ICM20649_REG_ACCEL_CONFIG, &i2cData, 1);
	/* Read the GYRO_CONFIG_1 register */
	icm20649_read(handle, icm20649, ICM20649_REG_GYRO_CONFIG_1, &i2cData, 1);
	i2cData &= ~(ICM20649_MASK_GYRO_BW);
	/* Write the new bandwidth value to the gyro config register */
	i2cData |= (ICM20649_GYRO_BW_51HZ & ICM20649_MASK_GYRO_BW);
	icm20649_write(handle, icm20649, ICM20649_REG_GYRO_CONFIG_1, &i2cData, 1);

	/* Set the most sensitive range: 2G full scale and 250dps full scale */
	uint8_t accelFs = ICM20649_ACCEL_FULLSCALE_4G;
	switch (Ascale) {
		case ACCEL_FS_SEL_4g:
			accelFs = ICM20649_ACCEL_FULLSCALE_4G;
			break;
		case ACCEL_FS_SEL_8g:
			accelFs = ICM20649_ACCEL_FULLSCALE_8G;
			break;
		case ACCEL_FS_SEL_16g:
			accelFs = ICM20649_ACCEL_FULLSCALE_16G;
			break;
#if (ICM20649)
		case ACCEL_FS_SEL_30g:
			accelFs = ICM20649_ACCEL_FULLSCALE_30G;
			break;
#endif
	}
	accelFs &= ICM20649_MASK_ACCEL_FULLSCALE;
	icm20649_read(handle, icm20649, ICM20649_REG_ACCEL_CONFIG, &i2cData, 1);
	i2cData &= ~(ICM20649_MASK_ACCEL_FULLSCALE);
	i2cData |= accelFs;
	icm20649_write(handle, icm20649, ICM20649_REG_ACCEL_CONFIG, &i2cData, 1);

	uint8_t gyroFs = ICM20649_GYRO_FULLSCALE_500DPS;
	switch (Gscale) {
		case GYRO_FS_SEL_500dps:
			gyroFs = ICM20649_GYRO_FULLSCALE_500DPS;
			break;
		case GYRO_FS_SEL_1000dps:
			gyroFs = ICM20649_GYRO_FULLSCALE_1000DPS;
			break;
		case GYRO_FS_SEL_2000dps:
			gyroFs = ICM20649_GYRO_FULLSCALE_2000DPS;
			break;
#if (ICM20649)
		case GYRO_FS_SEL_4000dps:
			gyroFs = ICM20649_GYRO_FULLSCALE_4000DPS;
			break;
#endif
	}

	gyroFs &= ICM20649_MASK_GYRO_FULLSCALE;
	icm20649_read(handle, icm20649, ICM20649_REG_GYRO_CONFIG_1, &i2cData, 1);
	i2cData &= ~(ICM20649_MASK_GYRO_FULLSCALE);
	i2cData |= gyroFs;
	icm20649_write(handle, icm20649, ICM20649_REG_GYRO_CONFIG_1, &i2cData, 1);

	delay(50);

	/* Enable the raw data ready interrupt */
	icm20649_interruptEnable(handle, icm20649, true, false);

	/* Enter low power mode */
	//icm20649_lowPowerModeEnter(icm20649, true, true, false);

	/* Clear the interrupts */
	icm20649_read(handle, icm20649, ICM20649_REG_INT_STATUS, rawData, 4);
}

// Accelerometer and gyroscope self test; check calibration wrt factory settings
void icm20649Driver_selfTest(void *handle, imuDriver_t *icm20649, float *destination) // Should return percent deviation from factory trim values, +/- 14 or less deviation is a pass
{
	destination[0] = 0;
	destination[1] = 0;
	destination[2] = 0;
	destination[3] = 0;
	destination[4] = 0;
	destination[5] = 0;
}

void icm20649Driver_getTempData(void *handle, imuDriver_t *icm20649, float *temperature)
{
	uint8_t rawData[2] = {0, 0};

	icm20649_read(handle, icm20649, ICM20649_REG_TEMPERATURE_H, rawData, 2);
	*temperature = ((((int16_t)rawData[0]) << 8) | rawData[1]) * 333.87 + 21.0;

	return;
}

bool icm20649Driver_interruptHandler(void *handle, imuDriver_t *icm20649, float *gyro, float *accel, float *mag)
{
	uint8_t rawData[14] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int16_t accelCount[3] = {0, 0, 0};
	int16_t gyroCount[3] = {0, 0, 0};
	bool ret = false;

	// If intPin goes high, all data registers have new data
	icm20649_read(handle, icm20649, ICM20649_REG_INT_ENABLE_1, rawData, 1);
	if (rawData[0] & ICM20649_BIT_RAW_DATA_0_RDY_EN) {  // On interrupt, check if data ready interrupt

		icm20649_read(handle, icm20649, ICM20649_REG_ACCEL_XOUT_H_SH, rawData, 14);
		accelCount[0] = (((int16_t)rawData[0]) << 8) | rawData[1];
		accelCount[1] = (((int16_t)rawData[2]) << 8) | rawData[3];
		accelCount[2] = (((int16_t)rawData[4]) << 8) | rawData[5];

		// Now we'll calculate the accleration value into actual g's
		accel[0] = (float)accelCount[0] * icm20649->aRes; // - accelBias[0];  // get actual g value, this depends on scale being set
		accel[1] = (float)accelCount[1] * icm20649->aRes; // - accelBias[1];
		accel[2] = (float)accelCount[2] * icm20649->aRes; // - accelBias[2];

		//mpu9250->read(GYRO_XOUT_H, rawData, 6);		// Read the x/y/z adc values
		gyroCount[0] = (((int16_t)rawData[8]) << 8) | rawData[9];
		gyroCount[1] = (((int16_t)rawData[10]) << 8) | rawData[11];
		gyroCount[2] = (((int16_t)rawData[12]) << 8) | rawData[13];

		// Calculate the gyro value into actual degrees per second
		gyro[0] = (float)gyroCount[0] * icm20649->gRes;  // get actual gyro value, this depends on scale being set
		gyro[1] = (float)gyroCount[1] * icm20649->gRes;
		gyro[2] = (float)gyroCount[2] * icm20649->gRes;

		mag[0] = 0;
		mag[1] = 0;
		mag[2] = 0;
		ret = true;
	}
	icm20649_read(handle, icm20649, ICM20649_REG_INT_STATUS, rawData, 4);
	return ret;
}
