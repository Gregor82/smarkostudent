/**
 * @file 	mpu9250Driver.h
 * @author 	GrLi
 * @date 	30.01.2019
 *      
 * @brief
 *
 * https://github.com/kriswiner/MPU9250/blob/master/STM32F401/main.cpp#L32
 * https://github.com/kriswiner/MPU9250/blob/master/STM32F401/MPU9250.h
 * https://github.com/kriswiner/MPU9250/blob/master/Dual_MPU9250/MPU9250.cpp
 * https://github.com/kriswiner/MPU9250/blob/master/MPU9250BasicAHRS.ino
 *
 *
 * https://github.com/kriswiner/MPU9250/issues/9
 * https://github.com/kriswiner/MPU9250/issues/68
 * https://github.com/kriswiner/MPU9250/issues/82
 * https://github.com/bolderflight/MPU9250/blob/master/src/MPU9250.cpp
 * https://github.com/bolderflight/MPU9250/blob/master/src/MPU9250.h
 * https://emlid.com/navio-mpu9250-tutorial-cpp/
 *
**/

#ifndef _MPU9250DRIVER_H_
#define _MPU9250DRIVER_H_

#include "float.h"
#include "stdint.h"
#include "stdbool.h"

#include "../inc/imu_config.h"

#define I2C_ADDRESS_MPU9250	0xD1

#define MPU9250_DEVICE_ID	0x71

int8_t mpu9250Driver_getID(void *handle, imuDriver_t *mpu9250, uint8_t *id);
void mpu9250Driver_reset(void *handle, imuDriver_t *mpu9250);
void mpu9250Driver_calibrate(void *handle, imuDriver_t *mpu9250);
void mpu9250Driver_init(void *handle, imuDriver_t *mpu9250, ACCEL_FS_SEL Ascale, GYRO_FS_SEL Gscale, MSCALE_SEL Mscale);
void mpu9250Driver_selfTest(void *handle, imuDriver_t *mpu9250, float *destination);

void mpu9250Driver_getTempData(void *handle, imuDriver_t *mpu9250, float *temperature);
bool mpu9250Driver_interruptHandler(void *handle, imuDriver_t *mpu9250, float *gyro, float *accel, float *mag);

#endif /* _MPU9250DRIVER_H_ */
