/**
 * @file			mpu9250.c
 * @date			20.03.2017
 *	
 * @details
 */

/******************** include files ***********************/
#include <imu/inc/imu.h>
#include <imu/inc/imuBuffer.h>
#include "inc/ble.h"

/******************** defines *****************************/
#if (MPU9250)
#include "imu/driver/mpu9250Driver.h"

#define imuDriver_init				mpu9250Driver_init
#define imuDriver_reset				mpu9250Driver_reset
#define imuDriver_getID				mpu9250Driver_getID
#define imuDriver_selfTest			mpu9250Driver_selfTest
#define imuDriver_calibrate			mpu9250Driver_calibrate
#define imuDriver_getTempData		mpu9250Driver_getTempData
#define imuDriver_interruptHandler	mpu9250Driver_interruptHandler

#elif (ICM20649)
#include "imu/driver/icm20649Driver.h"

#define imuDriver_init				icm20649Driver_init
#define imuDriver_reset				icm20649Driver_reset
#define imuDriver_getID				icm20649Driver_getID
#define imuDriver_selfTest			icm20649Driver_selfTest
#define imuDriver_calibrate			icm20649Driver_calibrate
#define imuDriver_getTempData		icm20649Driver_getTempData
#define imuDriver_interruptHandler	icm20649Driver_interruptHandler

#else
#error "Choose a IMU"
#endif

/******************** prototypes **************************/
static int8_t imu_i2cWrite(void *handle, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt, bool magnetData);
static int8_t imu_i2cRead(void *handle, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt, bool magnetData);
static void imuInterruptOneCallback(uint8_t pin);
#if (ICM20649)
static void imuInterruptTwoCallback(uint8_t pin);
#endif

/******************** struct data type ********************/
static imuDriver_t imu = {
	.read = imu_i2cRead,
	.write = imu_i2cWrite
};

struct imuHandle_s{
	void **i2c;
	uint8_t slaveAddress;
	uint8_t slaveMagAddress;
	bool magnetEnable;

	imuDriver_t *imu;

	IMUBuffer_t localGyro;
	//imuMeasurement_t localGyro[16];
	float gyroLimit[3];

	IMUBuffer_t localAccel;
	//imuMeasurement_t  localAccel[16];
	float accelLimit[3];

	IMUBuffer_t localMag;
	//imuMeasurement_t  localMag[16];
	float magLimit[3];

	bool initialized;
	bool gyroInterrupt;
	bool accelInterrupt;
	bool magInterrupt;

	callback_setAccelData_t localSetAccelData;
};

/******************** private variables********************/
static bool localInitialized;
static bool interruptOneData;
static bool interruptTwoData;

static uint8_t writeData[I2C_BUFFER_SIZE];
static uint8_t readData[I2C_BUFFER_SIZE];

/******************** private function ********************/
int8_t imu_i2cWrite(void *handle, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt, bool magnetData)
{
	uint8_t i;
	uint8_t slaveAddress;

	struct imuHandle_s *imuHandle = (struct imuHandle_s *)handle;
	if(magnetData) {
		slaveAddress = imuHandle->slaveMagAddress;
	} else {
		slaveAddress = imuHandle->slaveAddress;
	}

	writeData[0] = reg_addr;
	for(i = 0; i < cnt; i++)
		writeData[i + 1] = reg_data[i];

	I2C_RETURN ret = i2c_write(imuHandle->i2c, slaveAddress, writeData, cnt + 1);
	if(ret != I2C_SUCCESS)
		return -1;

	return 0;
}

int8_t imu_i2cRead(void *handle, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt, bool magnetData)
{
	uint8_t i;
	uint8_t slaveAddress;

	struct imuHandle_s *imuHandle = (struct imuHandle_s *)handle;
	if(magnetData) {
		slaveAddress = imuHandle->slaveMagAddress;
	} else {
		slaveAddress = imuHandle->slaveAddress;
	}

	writeData[0] = reg_addr;
	I2C_RETURN ret = i2c_writeRead(imuHandle->i2c, slaveAddress, writeData, 1, readData, cnt);
	if(ret != I2C_SUCCESS)
		return -1;

	for(i = 0; i < cnt; i++)
		reg_data[i] = readData[i];

	return 0;
}

void imuInterruptOneCallback(uint8_t pin)
{
	if(localInitialized) {
		return;
	}

	interruptOneData = true;
	GPIO_IntClear(1 << IMU_INT_1_PIN);

	return;
}

#if (NUMBER_OF_INT == 2)
void imuInterruptTwoCallback(uint8_t pin)
{
	if(!handle) {
		return IMU_NO_HANDLER;
	}

	struct imuHandle_s *imuHandle = (struct imuHandle_s *)handle;

	if(!imuHandle->initialized) {
		return IMU_NO_INIT;
	}


	imuHandle->interruptTwoData = true;
	GPIO_IntClear(1 << IMU_INT_2_PIN);

	return;
}
#endif

/******************** global function *********************/
void *imu_createHandle(void *i2cHandle, uint8_t slaveAddress, uint8_t slaveMagAddress)
{
	struct imuHandle_s *imuHandle = calloc(1, sizeof(struct imuHandle_s));
	imuHandle->i2c = i2cHandle;
	imuHandle->localSetAccelData = NULL;
	imuHandle->slaveAddress = slaveAddress;
	imuHandle->slaveMagAddress = slaveMagAddress;

	if(imuHandle->slaveMagAddress != 0) {
		imuHandle->magnetEnable = true;
	} else {
		imuHandle->magnetEnable = false;
	}

	imuHandle->imu = &imu;

	imuHandle->initialized = false;
	imuHandle->gyroInterrupt = false;
	imuHandle->accelInterrupt = false;
	imuHandle->magInterrupt = false;

	localInitialized = false;
	interruptOneData = false;
	interruptTwoData = false;

	return imuHandle;
}

IMU_RETURN imu_destroyHandle(void **handle)
{
	imu_deInit(*handle);

	free(*handle);
	*handle = NULL;

	return IMU_SUCCESS;
}

IMU_RETURN imu_init(void *handle, callback_setAccelData_t setAccelData)
{
	uint8_t id = 0;
	float selfTest[6];

	if(!handle) {
		return IMU_NO_HANDLER;
	}

	struct imuHandle_s *imuHandle = (struct imuHandle_s *)handle;

	imuHandle->initialized = false;
	imuHandle->localSetAccelData = NULL;

	if(imuDriver_getID(handle, imuHandle->imu, &id) != IMU_SUCCESS)
		return IMU_I2C_ERR;

	if(id != IMU_DEVICE_ID)
		return IMU_FAIL;

	imuDriver_selfTest(handle, imuHandle->imu, selfTest);
	for(int i = 0; i < 6; i++) {
		if((selfTest[0] < -14) || (selfTest[0] > 14))
			return IMU_FAIL;
	}

	imuHandle->gyroInterrupt = false;
	imuHandle->accelInterrupt = false;
	imuHandle->magInterrupt = false;
	imuHandle->gyroLimit[0] = 0;
	imuHandle->gyroLimit[1] = 0;
	imuHandle->gyroLimit[2] = 0;
	imuHandle->accelLimit[0] = 0;
	imuHandle->accelLimit[1] = 0;
	imuHandle->accelLimit[2] = 0;
	imuHandle->magLimit[0] = 0;
	imuHandle->magLimit[1] = 0;
	imuHandle->magLimit[2] = 0;

	if(setAccelData == NULL)
		return IMU_NO_INIT;
	imuHandle->localSetAccelData = setAccelData;

	/* Configure PB11 as input and enable interrupt - mpu9250 interrupt. */
	/* Interrupt is active low */
	GPIO_PinModeSet(IMU_INT_1_PORT, IMU_INT_1_PIN, gpioModeInputPull, 1);
	GPIO_IntConfig(IMU_INT_1_PORT, IMU_INT_1_PIN, false, true, true);
	GPIOINT_CallbackRegister(IMU_INT_1_PIN, imuInterruptOneCallback);

#if (NUMBER_OF_INT == 2)
	GPIO_PinModeSet(IMU_INT_2_PORT, IMU_INT_2_PIN, gpioModeInputPull, 1);
	GPIO_IntConfig(IMU_INT_2_PORT, IMU_INT_2_PIN, false, true, true);
	GPIOINT_CallbackRegister(IMU_INT_2_PIN, imuInterruptTwoCallback);
#endif

	imuDriver_calibrate(handle, imuHandle->imu);
	imuDriver_init(handle, imuHandle->imu, ACCEL_FS_SEL_8g, GYRO_FS_SEL_500dps, MFS_16BITS);

	imuHandle->initialized = true;
	localInitialized = true;
	imuInterruptOneCallback(IMU_INT_1_PIN);
#if (NUMBER_OF_INT == 2)
	imuInterruptTwoCallback(IMU_INT_2_PIN);
#endif

	imuBuffer_init(&imuHandle->localGyro, 2);
	imuBuffer_init(&imuHandle->localAccel, 2);
	imuBuffer_init(&imuHandle->localMag, 2);

	return IMU_SUCCESS;
}

IMU_RETURN imu_deInit(void *handle)
{
//	imuDriver_reset(imuHandle->imu);		// -> f�hrt zu einer Endlosschleife und damit zu einem Watchdog reset.
	if(!handle) {
		return IMU_NO_HANDLER;
	}

	struct imuHandle_s *imuHandle = (struct imuHandle_s *)handle;
	imuHandle->initialized = false;

	return IMU_SUCCESS;
}

IMU_RETURN imu_measure(void *handle, uint32_t *timestamp)
{
	if(!handle) {
		return IMU_NO_HANDLER;
	}

	struct imuHandle_s *imuHandle = (struct imuHandle_s *)handle;

	if(!imuHandle->initialized) {
		return IMU_NO_INIT;
	}

	if(GPIO_PinInGet(IMU_INT_1_PORT, IMU_INT_1_PIN) == 0) {
		interruptOneData = true;
	} else {
		interruptOneData = false;
	}

	if(interruptOneData) {
		int16_t interrupt = 0;
		imuMeasurement_t tempGyro;
		imuMeasurement_t tempAccel;
		imuMeasurement_t tempMag;

		//imuDriver_interruptHandler(handle, imuHandle->imu, imuHandle->localGyro, imuHandle->localAccel, imuHandle->localMag);

		imuBuffer_deleteData(&imuHandle->localGyro);
		imuBuffer_deleteData(&imuHandle->localAccel);
		imuBuffer_deleteData(&imuHandle->localMag);

		tempGyro.timestamp = *timestamp;
		tempAccel.timestamp = *timestamp;
		tempMag.timestamp = *timestamp;
		//imuDriver_interruptHandler(&imu, tempGyro.value, tempAccel.value, tempMag.value);
		if(imuDriver_interruptHandler(handle, imuHandle->imu, tempGyro.value, tempAccel.value, tempMag.value)) {
			imuBuffer_write(&imuHandle->localGyro, &tempGyro);
			imuBuffer_write(&imuHandle->localAccel, &tempAccel);
			imuBuffer_write(&imuHandle->localMag, &tempMag);
		}

		if(imuHandle->localSetAccelData != NULL)
		{
			int16_t acc[3];
			acc[0] = tempAccel.value[0] * 256;
			acc[1] = tempAccel.value[1] * 256;
			acc[2] = tempAccel.value[2] * 256;
			imuHandle->localSetAccelData(acc);
		}

		if(imuHandle->gyroInterrupt) {
			uint8_t i;
			for(i = 0; i < 3; i++) {
				if((tempGyro.value[i] >= imuHandle->gyroLimit[i]) && (imuHandle->gyroLimit[i] > 0)){
					interrupt |= (1 << i);
				}
			}
		}

		if(imuHandle->accelInterrupt) {
			uint8_t i;
			for(i = 0; i < 3; i++) {
				if((tempAccel.value[i] >= imuHandle->accelLimit[i]) && (imuHandle->accelLimit[i] > 0)){
					interrupt |= (1 << (i + 3));
				}
			}
		}

		if(imuHandle->magnetEnable && imuHandle->magInterrupt) {
			uint8_t i;
			for(i = 0; i < 3; i++) {
				if((tempMag.value[i] >= imuHandle->magLimit[i]) && (imuHandle->magLimit[i] > 0)){
					interrupt |= (1 << (i + 6));
				}
			}
		}

		if(interrupt > 0) {
			float gyro[3];
			float accel[3];
			float mag[3];
			uint32_t gyroTime;
			uint32_t accelTime;
			uint32_t magTime;

			imu_getGyroData(handle, gyro, &gyroTime);
			imu_getAccelData(handle, accel, &accelTime);
			imu_getMagData(handle, mag, &magTime);

			ble_sendIMUCmd(SET_ACCEL, &accelTime, accel, 3);
			ble_sendIMUCmd(SET_GYRO, &gyroTime, gyro, 3);
			ble_sendIMUCmd(SET_MAG, &magTime, mag, 3);
//			ble_sendCmd(SET_ACCEL, accel, 3);
//			ble_sendCmd(SET_GYRO, gyro, 3);
//			ble_sendCmd(SET_MAG, mag, 3);

			ble_sendCmd(SET_MPU9250_INT, &interrupt, 1);
			interrupt = 0;
		}
		interruptOneData = false;
	}
	if(interruptTwoData) {
		interruptTwoData = false;
	}


	if(GPIO_PinInGet(IMU_INT_1_PORT, IMU_INT_1_PIN) == 1) {
		interruptOneData = true;
	}
	return IMU_SUCCESS;
}

IMU_RETURN imu_getData(void *handle, float *gyro, uint32_t *gyroTime, float *accel, uint32_t *accelTime, float *mag, uint32_t *magTime)
{
	gyro[0] = 0;
	gyro[1] = 0;
	gyro[2] = 0;
	accel[0] = 0;
	accel[1] = 0;
	accel[2] = 0;
	mag[0] = 0;
	mag[1] = 0;
	mag[2] = 0;

	if(imu_getAccelData(handle, accel, accelTime) != IMU_SUCCESS)
		return IMU_NO_INIT;

	if(imu_getGyroData(handle, gyro, gyroTime) != IMU_SUCCESS)
		return IMU_NO_INIT;

	if(imu_getMagData(handle, mag, magTime) != IMU_SUCCESS)
		return IMU_NO_INIT;

	return IMU_SUCCESS;
}

IMU_RETURN imu_getAccelData(void *handle, float *accel, uint32_t *timestamp)
{
	accel[0] = 0;
	accel[1] = 0;
	accel[2] = 0;

	if(!handle) {
		return IMU_NO_HANDLER;
	}

	struct imuHandle_s *imuHandle = (struct imuHandle_s *)handle;

	if(!imuHandle->initialized) {
		return IMU_NO_INIT;
	}

	if(imuBuffer_isEmpty(&imuHandle->localAccel)) {
		return IMU_NO_MORE_DATA;
	}
	imuMeasurement_t temp = imuBuffer_read(&imuHandle->localAccel);
	*timestamp = temp.timestamp;
	accel[0] = temp.value[0];
	accel[1] = temp.value[1];
	accel[2] = temp.value[2];

	return IMU_SUCCESS;
}

IMU_RETURN imu_getGyroData(void *handle,float *gyro, uint32_t *timestamp)
{
	gyro[0] = 0;
	gyro[1] = 0;
	gyro[2] = 0;

	if(!handle) {
		return IMU_NO_HANDLER;
	}

	struct imuHandle_s *imuHandle = (struct imuHandle_s *)handle;

	if(!imuHandle->initialized) {
		return IMU_NO_INIT;
	}

	if(imuBuffer_isEmpty(&imuHandle->localGyro)) {
		return IMU_NO_MORE_DATA;
	}

	imuMeasurement_t temp = imuBuffer_read(&imuHandle->localGyro);
	*timestamp = temp.timestamp;
	gyro[0] = temp.value[0];
	gyro[1] = temp.value[1];
	gyro[2] = temp.value[2];


	return IMU_SUCCESS;
}

IMU_RETURN imu_getMagData(void *handle, float *mag, uint32_t *timestamp)
{
	mag[0] = 0;
	mag[1] = 0;
	mag[2] = 0;

	if(!handle) {
		return IMU_NO_HANDLER;
	}

	struct imuHandle_s *imuHandle = (struct imuHandle_s *)handle;

	if(!imuHandle->initialized) {
		return IMU_NO_INIT;
	}

	if(imuBuffer_isEmpty(&imuHandle->localMag)) {
		return IMU_NO_MORE_DATA;
	}

	imuMeasurement_t temp = imuBuffer_read(&imuHandle->localMag);
	*timestamp = temp.timestamp;
	mag[0] = temp.value[0];
	mag[1] = temp.value[1];
	mag[2] = temp.value[2];

	return IMU_SUCCESS;
}

IMU_RETURN imu_getTempData(void *handle, int16_t *temperature)
{
	*temperature = -27315;

	if(!handle) {
		return IMU_NO_HANDLER;
	}

	struct imuHandle_s *imuHandle = (struct imuHandle_s *)handle;

	if(!imuHandle->initialized) {
		return IMU_NO_INIT;
	}

	float temp;
	imuDriver_getTempData(handle, imuHandle->imu, &temp);
	*temperature = temp + 0.5;

	return IMU_SUCCESS;
}

IMU_RETURN imu_setGyroLimit(void *handle, uint16_t *data)
{
	uint8_t i;

	if(!handle) {
		return IMU_NO_HANDLER;
	}

	struct imuHandle_s *imuHandle = (struct imuHandle_s *)handle;

	if(!imuHandle->initialized) {
		return IMU_NO_INIT;
	}

	imuHandle->gyroInterrupt = false;
	for(i = 0; i < 3; i++) {
		imuHandle->gyroLimit[i] = data[i] / 1000;
		if(imuHandle->gyroLimit[i] > 0) {
			imuHandle->gyroInterrupt = true;
		}
	}

	return IMU_SUCCESS;
}

IMU_RETURN imu_setAccelLimit(void *handle, uint16_t *data)
{
	uint8_t i;

	if(!handle) {
		return IMU_NO_HANDLER;
	}

	struct imuHandle_s *imuHandle = (struct imuHandle_s *)handle;

	if(!imuHandle->initialized) {
		return IMU_NO_INIT;
	}

	imuHandle->accelInterrupt = false;
	for(i = 0; i < 3; i++) {
		imuHandle->accelLimit[i] = data[i] / 1000;
		if(imuHandle->accelLimit[i] > 0) {
			imuHandle->accelInterrupt = true;
		}
	}

	return IMU_SUCCESS;
}

IMU_RETURN imu_setMagLimit(void *handle, uint16_t *data)
{
	uint8_t i;

	if(!handle) {
		return IMU_NO_HANDLER;
	}

	struct imuHandle_s *imuHandle = (struct imuHandle_s *)handle;

	if(!imuHandle->initialized) {
		return IMU_NO_INIT;
	}

	imuHandle->magInterrupt = false;
	for(i = 0; i < 3; i++) {
		imuHandle->magLimit[i] = data[i] / 1000;
		if(imuHandle->magLimit[i] > 0) {
			imuHandle->magInterrupt = true;
		}
	}

	return IMU_SUCCESS;
}
