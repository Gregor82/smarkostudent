/*
 * imuBuffer.c
 *
 *  Created on: 10.11.2019
 *      Author: Cattivo
 */
/******************** include files ***********************/
#include "imu/inc/imuBuffer.h"

/******************** defines *****************************/

/******************** prototypes **************************/

/******************** struct data type ********************/

/******************** private variables********************/

/******************** private function ********************/

/******************** global function *********************/
void imuBuffer_init(IMUBuffer_t *buffer, uint8_t bufferSize)
{
	buffer->size		= bufferSize;
	buffer->buffer		= (imuMeasurement_t *)calloc(sizeof(imuMeasurement_t), bufferSize);

	imuBuffer_deleteData(buffer);

	return;
}

void imuBuffer_write(IMUBuffer_t *buffer, imuMeasurement_t *data)
{
	if((buffer->overflow) && (buffer->write == buffer->read))
		return;

	buffer->buffer[buffer->write++] = *data;
	if(buffer->write >= buffer->size)
	{
		buffer->write = 0;
		buffer->overflow = true;
	}

	return;
}

imuMeasurement_t imuBuffer_read(IMUBuffer_t *buffer)
{
	if((!buffer->overflow) && (buffer->read == buffer->write)) {
		imuMeasurement_t dataFail;
		dataFail.timestamp = 0;
		dataFail.value[0] = 0;
		dataFail.value[1] = 0;
		dataFail.value[2] = 0;
		return dataFail;
	}

	imuMeasurement_t data = buffer->buffer[buffer->read++];
	if(buffer->read >= buffer->size)
	{
		buffer->read = 0;
		buffer->overflow = false;
	}

	return data;
}

void imuBuffer_deleteData(IMUBuffer_t *buffer)
{
	buffer->write = 0;
	buffer->read = 0;
	buffer->overflow = false;

	return;
}

bool imuBuffer_isFull(IMUBuffer_t *buffer)
{
	if((buffer->overflow) && (buffer->read == buffer->write))
		return true;

	return false;
}

bool imuBuffer_isEmpty(IMUBuffer_t *buffer)
{
	if((buffer->overflow) || (buffer->read < buffer->write))
		return false;

	return true;
}
