/*
 * imu_config.h
 *
 *  Created on: 14.03.2019
 *      Author: grli
 */

#ifndef DRIVER_IMU_INC_IMU_CONFIG_H_
#define DRIVER_IMU_INC_IMU_CONFIG_H_

#define MPU9250		1			// gyro alt -> dicke Platine
#define ICM20649	0			// gyro new -> d�nne Platine

#if (MPU9250)

#define IMU_ADDRESS			0xD1 //I2C_ADDRESS_MPU9250	//!< IMU I2C address
#define IMU_ADDRESS_MAGNET	(0x0C )
#define IMU_DEVICE_ID	MPU9250_DEVICE_ID

#define NUMBER_OF_INT	1

#define IMU_INT_1_PIN   (11)
#define IMU_INT_1_PORT  (gpioPortB)


typedef enum {
	GYRO_FS_SEL_250dps      = 0,
	GYRO_FS_SEL_500dps      = 1,
	GYRO_FS_SEL_1000dps     = 2,
	GYRO_FS_SEL_2000dps     = 3
}GYRO_FS_SEL;

typedef enum {
	ACCEL_FS_SEL_2g         = 0,
	ACCEL_FS_SEL_4g         = 1,
	ACCEL_FS_SEL_8g         = 2,
	ACCEL_FS_SEL_16g        = 3
}ACCEL_FS_SEL;

typedef enum {
	MFS_14BITS = 0, // 0.6 mG per LSB
	MFS_16BITS      // 0.15 mG per LSB
}MSCALE_SEL;

#elif (ICM20649)

#define IMU_ADDRESS			0xD1 //I2C_ADDRESS_ICM20649	//!< IMU I2C address
#define IMU_ADDRESS_MAGNET	0x00
#define IMU_DEVICE_ID	ICM20649_DEVICE_ID

#define NUMBER_OF_INT	2

#define IMU_INT_1_PIN   (11)
#define IMU_INT_1_PORT  (gpioPortB)

#define IMU_INT_2_PIN   (5)
#define IMU_INT_2_PORT  (gpioPortD)

typedef enum {
	GYRO_FS_SEL_500dps      = 0,
	GYRO_FS_SEL_1000dps     = 1,
	GYRO_FS_SEL_2000dps     = 2,
	GYRO_FS_SEL_4000dps     = 3
}GYRO_FS_SEL;

typedef enum {
	ACCEL_FS_SEL_4g         = 0,
	ACCEL_FS_SEL_8g         = 1,
	ACCEL_FS_SEL_16g        = 2,
	ACCEL_FS_SEL_30g        = 3
}ACCEL_FS_SEL;

typedef enum {
	MFS_14BITS = 0, // 0.6 mG per LSB
	MFS_16BITS      // 0.15 mG per LSB
}MSCALE_SEL;


#else
#error "Choose a IMU"
#endif



typedef int8_t (*imuDriver_i2c_fptr_t)(void *handle, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt, bool magnetData);

typedef struct imuDriver{
	float aRes; 				// scale resolutions per LSB for the sensors
	float gRes; 				// scale resolutions per LSB for the sensors
	float mRes; 				// scale resolutions per LSB for the sensors
	float gyroBias[3]; 			// Bias correction for gyro
	float accelBias[3]; 		// Bias correction for accelerometer
	float magCalibration[3];
	float magbias[3];
	imuDriver_i2c_fptr_t read;
	imuDriver_i2c_fptr_t write;
}imuDriver_t;

extern void delay(uint32_t dlyTicks);

#endif /* DRIVER_IMU_INC_IMU_CONFIG_H_ */
