/**
 * @file			mpu9250.h
 * @date			20.03.2017
 *	
 * @details
 */

#ifndef IMU_H_
#define IMU_H_

#include <stdbool.h>

#include "inc/i2c.h"
#include "inc/imu_config.h"

#include "gpiointerrupt.h"

typedef void (*callback_setAccelData_t)(int16_t *accel);

/**
 * @brief Return codes for the MP9250.
 */
typedef enum {
	IMU_NO_MORE_DATA = 1,
	IMU_SUCCESS = 0,			//!< Transfer completed successfully.
	IMU_FAIL = -1,          	//!< IMU error.
	IMU_I2C_ERR = -2,       	//!< I2C error.
	IMU_NO_HANDLER = -98,			//!<
	IMU_NO_INIT = -99       	//!< IMU not initialized.
}IMU_RETURN;


/**
 * @attention If no magnetometer is included in the imu, set the slaveMagAddress to 0.
 *
 * @param i2cHandle
 * @param slaveAddress
 * @param slaveMagAddress i2c slave address of the magnetometer.
 */
void *imu_createHandle(void *i2cHandle, uint8_t slaveAddress, uint8_t slaveMagAddress);

/**
 *
 * @param handle
 * @return
 */
IMU_RETURN imu_destroyHandle(void **handle);

/**
 * @brief Initialize the IMU.
 *
 * Initialize the the gyro and the acceleration. The magnetic measurement
 * is not initialized.
 *
 * @return IMU_RETURN
 * @retval IMU_SUCCESS			Transfer completed successfully
 * @retval IMU_NO_INIT			IMU not initialized
 */
IMU_RETURN imu_init(void *handle, callback_setAccelData_t setAccelData);

IMU_RETURN imu_deInit(void *handle);

IMU_RETURN imu_measure(void *handle, uint32_t *timestamp);

IMU_RETURN imu_getData(void *handle, float *gyro, uint32_t *gyroTime, float *accel, uint32_t *accelTime, float *mag, uint32_t *magTime);
IMU_RETURN imu_getAccelData(void *handle, float *accel, uint32_t *timestamp);
IMU_RETURN imu_getGyroData(void *handle, float *gyro, uint32_t *timestamp);
IMU_RETURN imu_getMagData(void *handle, float *mag, uint32_t *timestamp);
IMU_RETURN imu_getTempData(void *handle, int16_t *temperature);

IMU_RETURN imu_setGyroLimit(void *handle, uint16_t *data);
IMU_RETURN imu_setAccelLimit(void *handle, uint16_t *data);
IMU_RETURN imu_setMagLimit(void *handle, uint16_t *data);


#endif /* IMU_H_ */
