/*
 * imuBuffer.h
 *
 *  Created on: 10.11.2019
 *      Author: Cattivo
 */

#ifndef DRIVER_IMU_INC_IMUBUFFER_H_
#define DRIVER_IMU_INC_IMUBUFFER_H_



#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>


typedef struct{
	uint32_t timestamp;
	float value[3];
}imuMeasurement_t;

/**
 * struct for buffer
 */
typedef struct
{
	/*@{*/
	uint8_t write;		/**< write */
	uint8_t read;		/**< read */
	uint8_t size;		/**< size */
	bool overflow;		/**< overflow */
	imuMeasurement_t *buffer;	/**< buffer */
	/*@}*/
}IMUBuffer_t;

/**
 *
 * @param buffer
 * @param bufferSize
 */
void imuBuffer_init(IMUBuffer_t *buffer, uint8_t bufferSize);

/**
 *
 * @param buffer
 * @param data
 */
void imuBuffer_write(IMUBuffer_t *buffer, imuMeasurement_t *data);

/**
 *
 * @param buffer
 * @return
 */
imuMeasurement_t imuBuffer_read(IMUBuffer_t *buffer);

/**
 *
 * @param buffer
 */
void imuBuffer_deleteData(IMUBuffer_t *buffer);

/**
 *
 * @param buffer
 * @retval true
 * @retval false
 */
bool imuBuffer_isFull(IMUBuffer_t *buffer);

/**
 *
 * @param buffer
 * @retval true
 * @retval false
 */
bool imuBuffer_isEmpty(IMUBuffer_t *buffer);


#endif /* DRIVER_IMU_INC_IMUBUFFER_H_ */
