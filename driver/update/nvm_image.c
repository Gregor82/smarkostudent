#include <string.h>

#include "nvm_image.h"


#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))


ImageNo current_image_no = INVALID_IMAGE;
ImageNo other_image_no = INVALID_IMAGE;
uint8_t *current_write_addr = INVALID_ADDR;

uint8_t page_cache[FLASH_PAGE_SIZE];    // cache is needed because we can only erase a whole page at once, not single bytes separately
uint32_t cache_level = 0;

uint32_t written_image_bytes = 0;


uint8_t *get_current_image_addr() {
  uint8_t *address;

  NVMHAL_Read(CURRENT_IMAGE_ADDR, &address, sizeof(address));

  return address;
}

ImageNo get_current_image_no() {
  uint8_t *current_image_addr = get_current_image_addr();

  switch((uint32_t)current_image_addr) {
  case (uint32_t)IMAGE_0_BINARY_ADDR:
    return IMAGE_0;
  case (uint32_t)IMAGE_1_BINARY_ADDR:
    return IMAGE_1;
  }

  return INVALID_IMAGE;
}

ImageNo get_other_image_no(ImageNo current_image) {
  switch(current_image) {
  case IMAGE_0:
    return IMAGE_1;
  case IMAGE_1:
    return IMAGE_0;
  case INVALID_IMAGE: // Failed getting the other image. This might be because the current image has not been initialized yet. Take the second one.
    return IMAGE_1;
  }

  return INVALID_IMAGE;
}

ImageNo image_Current() {
  return current_image_no;
}

uint8_t *get_image_length_addr(ImageNo image_no) {
  switch(image_no) {
  case IMAGE_0:
    return IMAGE_0_LENGTH_ADDR;
  case IMAGE_1:
    return IMAGE_1_LENGTH_ADDR;
  case INVALID_IMAGE:
    return INVALID_ADDR;
  }

  return INVALID_ADDR;
}

uint8_t *get_image_checksum_addr(ImageNo image_no) {
  switch(image_no) {
  case IMAGE_0:
    return IMAGE_0_CHECKSUM_ADDR;
  case IMAGE_1:
    return IMAGE_1_CHECKSUM_ADDR;
  case INVALID_IMAGE:
    return INVALID_ADDR;
  }

  return INVALID_ADDR;
}

uint8_t *get_image_binary_addr(ImageNo image_no) {
  switch(image_no) {
  case IMAGE_0:
    return IMAGE_0_BINARY_ADDR;
  case IMAGE_1:
    return IMAGE_1_BINARY_ADDR;
  case INVALID_IMAGE:
    return INVALID_ADDR;
  }

  return INVALID_ADDR;
}

int8_t image_Init() {
  current_image_no = get_current_image_no();
  other_image_no = get_other_image_no(current_image_no);
  current_write_addr = get_image_binary_addr(other_image_no);
  written_image_bytes = 0;
  cache_level = 0;

  int i = 0;
  int8_t result = 0;
  int pages = IMAGE_BINARY_LEN/FLASH_PAGE_SIZE;
#if (DEBUG && DEBUG_UPDATE)
  int startTime = time_InMs();
#endif
  for (i=0; i<pages; i++){
      result = NVMHAL_PageErase(current_write_addr+i*FLASH_PAGE_SIZE);
      if (result<0)
    	  return -1;
  }
#if (DEBUG && DEBUG_UPDATE)
  log_info("flash earse time: %d\n", time_InMs()-startTime);
#endif

  return 0;
}

int8_t write_cache_to_page() {
  if(cache_level <= 0)
    return 0;

  //int8_t result = NVMHAL_PageErase(current_write_addr);
  int8_t result = 0;

  if(result < 0) {
    return result;
  }
#if (DEBUG && DEBUG_UPDATE)
//  log_info("the current_write_addr address is:%d \n",current_write_addr);
#endif

  result = NVMHAL_Write(current_write_addr, page_cache, cache_level);

#if (DEBUG && DEBUG_UPDATE)
//  log_info("Done with writing! - result - %d\n",result);
#endif


  if(result >= 0) {
    current_write_addr += FLASH_PAGE_SIZE;
    written_image_bytes += cache_level;
  } else {
    return result;
  }

  cache_level = 0;

  return result;
}

int8_t image_Write(uint8_t *data, uint32_t len) {
  uint32_t len_to_cache;
  uint32_t written = 0;
  int8_t result;

  if(current_write_addr == INVALID_ADDR)
    return -1;

  while(written < len) {
    len_to_cache = MIN(FLASH_PAGE_SIZE - cache_level, len - written);
    memcpy(page_cache + cache_level, data + written, len_to_cache);
    written += len_to_cache;
    cache_level += len_to_cache;

    if(cache_level >= FLASH_PAGE_SIZE) {
      result = write_cache_to_page();
#if (DEBUG && DEBUG_UPDATE)
//      log_info("the data has been written to the memory! with result: %d\n",result);
#endif
      if(result < 0)
        return result;
    }
  }

  return 3;
}

int8_t check_checksum(uint16_t orig_checksum) {
  uint16_t my_checksum;
  uint8_t *binary_addr = get_image_binary_addr(other_image_no);

  if(binary_addr == INVALID_ADDR)
    return -1;

  my_checksum = 0xffffU;

  NVMHAL_Checksum(&my_checksum, binary_addr, written_image_bytes);

#if (DEBUG && DEBUG_UPDATE)
  log_info("\n ---------------------> MCU checkSum %d\n",my_checksum );
  log_info("\n ---------------------> Orig checkSum %d\n",orig_checksum );
#endif

  if(my_checksum != orig_checksum)
    return -2;

  return 0;
}

int8_t write_length() {
  uint8_t *len_addr = get_image_length_addr(other_image_no);
  uint32_t read_len;

  if(len_addr == INVALID_ADDR)
    return -1;

  int8_t result = NVMHAL_PageErase(len_addr);

  if(result < 0)
    return result;

  result = NVMHAL_Write(len_addr, &written_image_bytes, sizeof(written_image_bytes));

  if(result < 0)
    return result;

  // check whether length write was successful
  NVMHAL_Read(len_addr, &read_len, sizeof(read_len));

  if(read_len != written_image_bytes)
    return -2;

  return 0;
}

int8_t write_checksum(uint16_t orig_checksum) {
  uint8_t *checksum_addr = get_image_checksum_addr(other_image_no);
  uint16_t my_checksum;

  if(checksum_addr == INVALID_ADDR)
    return -1;

  int8_t result = NVMHAL_PageErase(checksum_addr);

  if(result < 0)
    return result;

  result = NVMHAL_Write(checksum_addr, &orig_checksum, sizeof(orig_checksum));

  if(result < 0)
    return result;

  // check whether checksum write was successful
  NVMHAL_Read(checksum_addr, &my_checksum, sizeof(my_checksum));

  if(my_checksum != orig_checksum)
    return -2;

  return 0;
}

int8_t write_current_image_addr() {
  int8_t result = NVMHAL_PageErase(CURRENT_IMAGE_ADDR);
  uint8_t *new_addr = get_image_binary_addr(other_image_no);
  uint8_t *my_addr = INVALID_ADDR;

  if(result < 0)
    return result;

  result = NVMHAL_Write(CURRENT_IMAGE_ADDR, &new_addr, sizeof(new_addr));

  if(result < 0)
    return result;

  NVMHAL_Read(CURRENT_IMAGE_ADDR, &my_addr, sizeof(my_addr));

  if(my_addr != new_addr)
    return -2;

  return 0;
}

int8_t image_Finish(uint16_t checksum) {
  int8_t result = write_cache_to_page();

  if(result < 0)
    return result;

  result = write_length();

  if(result < 0)
    return result;

  result = check_checksum(checksum);
#if (DEBUG && DEBUG_UPDATE)
  log_info("done- check_checksum result: %d\n",result);
#endif
  if(result < 0)
    return result;

  result = write_checksum(checksum);
#if (DEBUG && DEBUG_UPDATE)
  log_info("done- write_checksum result: %d\n",result);
#endif

  if(result < 0)
    return result;

  result = write_current_image_addr();
#if (DEBUG && DEBUG_UPDATE)
  log_info("done- write_current_image_addr result: %d\n",result);
#endif

  if(result < 0)
    return result;

  current_write_addr = INVALID_ADDR;

  return 1;
}

