#ifndef NVM_IMAGE_H_
#define NVM_IMAGE_H_

#include <stdint.h>

#include "nvm_hal.h"

#include "inc/userTimer.h"
//#include "debug/debug.h"

/******************************************************************************
 * @brief
 *  Used to identify an image
 *****************************************************************************/
typedef enum {
  INVALID_IMAGE = 0xFF, //!< Image could not be identified
  IMAGE_0 = 0,          //!< The first image (within the flash)
  IMAGE_1 = 1           //!< The second image (within the flash)
} ImageNo;

/**
 * Order of sections in NVM:
 * - Bootloader
 * - Current image selector
 * - Image 0 length (of binary only, without checksum)
 * - Image 0 checksum
 * - Image 0 binary
 * - Image 1 length (of binary only, without checksum)
 * - Image 1 checksum
 * - Image 1 binary
 *
 * Flash usage
 * 0x0      - 0x7FFF    bootloader              (32k)
 * 0x8000   - 0x8FFF    current image selector  (4k)
 * 0x9000   - 0x9FFF    image 0 length          (4k)
 * 0xA000   - 0xAFFF    image 0 checksum        (4k)
 * 0xB000   - 0x7BFFF   image 0 binary          (452k)
 * 0x7C000  - 0x7CFFF   image 1 length          (4k)
 * 0x7D000  - 0x7DFFF   image 1 checksum        (4k)
 * 0x7E000  - 0xEEFFF   image 1 binary          (452k)
 * 0xEF000  - 0xEFFFF   SIM PIN                 (4k)
 * 0xF0000  - 0xF0FFF   Serial Number           (4k)
 * 0xF1000  - 0xFFFFF   FREE                    (60k)
 */

#define BOOTLOADER_LEN          0x8000
#define CURRENT_IMAGE_ADDR_LEN  FLASH_PAGE_SIZE // 4k
#define IMAGE_LENGTH_LEN        FLASH_PAGE_SIZE
#define IMAGE_CHECKSUM_LEN      FLASH_PAGE_SIZE
#define IMAGE_BINARY_LEN        0x71000         // 452k
#define IMAGE_LEN               (IMAGE_LENGTH_LEN + IMAGE_CHECKSUM_LEN + IMAGE_BINARY_LEN)

#define BOOTLOADER_ADDR         (uint8_t*)0x00
#define CURRENT_IMAGE_ADDR      (uint8_t*)BOOTLOADER_LEN
#define IMAGE_0_ADDR            (uint8_t*)(CURRENT_IMAGE_ADDR + CURRENT_IMAGE_ADDR_LEN)
#define IMAGE_1_ADDR            (uint8_t*)(IMAGE_0_ADDR + IMAGE_LEN)
#define INVALID_ADDR            (uint8_t*)0xFFFFFFFF

#define IMAGE_0_LENGTH_ADDR     (uint8_t*)IMAGE_0_ADDR
#define IMAGE_0_CHECKSUM_ADDR   (uint8_t*)(IMAGE_0_ADDR + IMAGE_LENGTH_LEN)
#define IMAGE_0_BINARY_ADDR     (uint8_t*)(IMAGE_0_CHECKSUM_ADDR + IMAGE_CHECKSUM_LEN)              // absolute: 0xB000

#define IMAGE_1_LENGTH_ADDR     (uint8_t*)IMAGE_1_ADDR
#define IMAGE_1_CHECKSUM_ADDR   (uint8_t*)(IMAGE_1_ADDR + IMAGE_LENGTH_LEN)
#define IMAGE_1_BINARY_ADDR     (uint8_t*)(IMAGE_1_CHECKSUM_ADDR + IMAGE_CHECKSUM_LEN)              // absolute: 0x7E000

#define PIN_FLASH_ADDR  (IMAGE_1_BINARY_ADDR+IMAGE_BINARY_LEN)
#define SN_FLASH_ADDR   (PIN_FLASH_ADDR + FLASH_PAGE_SIZE)

/******************************************************************************
 * @brief
 *  Initializes the image writing process.
 *
 * @note
 *  NVMHAL_Init() must be called before.
 *  Call this function before using any of the other functions.
 *
 * @return
 *   >= 0 OK
 *   < 0  Error
 *****************************************************************************/
int8_t image_Init();

/******************************************************************************
 * @brief
 *  Returns the number of the image that is currently running.
 *
 * @return
 *   ImageNo of the currently running image
 *****************************************************************************/
ImageNo image_Current();

/******************************************************************************
 * @brief
 *  Writes a part of the image binary to the other (not current) image section
 *  within the flash.
 *
 * @note
 *  Call image_Init() before using this function.
 *  Call image_Finish() after all image binary data is written by using this
 *  function.
 *
 * @param data
 *  Pointer to the bytes that should be written into the flash section.
 *
 * @param len
 *  The length of bytes that should be written into the flash section.
 *
 * @return
 *   >= 0 OK
 *   < 0  Error
 *****************************************************************************/
int8_t image_Write(uint8_t *data, uint32_t len);

/******************************************************************************
 * @brief
 *  Finishes the writing of a new image. It performs the following steps. If one
 *  of the step fails, the function returns immediately.
 *    - Write the (total) length of the written image into the corresponding
 *      section in the flash
 *    - Generates a checksum from the data within the flash section and compares
 *      it to the one from the parameter
 *    - Writes the checksum into the corresponding section in the flash.
 *    - Updates the pointer to the image that should be booted.
 *
 * @param checksum
 *  After finishing the writing process, a checksum is generated from the data
 *  in the flash and compared to the value of this parameter.
 *
 * @return
 *   >= 0 OK
 *   < 0  Error
 *****************************************************************************/
int8_t image_Finish(uint16_t checksum);

#endif /* NVM_IMAGE_H_ */
