/**
 * @file			gpioDefines.h
 * @date			15.03.2017
 *	
 * @details
 */

#ifndef GPIO_DEFINES_H_
#define GPIO_DEFINES_H_

/** @cond DO_NOT_INCLUDE_WITH_DOXYGEN */
#define I2C0_SCL_PIN    				(1)
#define I2C0_SCL_PORT   				(gpioPortA)

#define I2C0_SDA_PIN    				(0)
#define I2C0_SDA_PORT   				(gpioPortA)

#define ISL9110A_MODE_PIN               (5)
#define ISL9110A_MODE_PORT              (gpioPortA)

#define ISL9110A_BAT_PIN                (6)
#define ISL9110A_BAT_PORT               (gpioPortA)

#define ISL9110A_PG_PIN                 (8)
#define ISL9110A_PG_PORT                (gpioPortA)

#define BQ51050B_CHG_PIN                (9)
#define BQ51050B_CHG_PORT               (gpioPortA)

#define BQ51050B_EN2_PIN                (10)
#define BQ51050B_EN2_PORT               (gpioPortA)

#define BQ51050B_TS_CTRL_PIN            (15)
#define BQ51050B_TS_CTRL_PORT           (gpioPortA)

#define LFXO_N_PIN                      (8)
#define LFXO_N_PORT                     (gpioPortB)

#define HFXO_P_PIN                      (13)
#define HFXO_P_PORT                     (gpioPortB)

#define HFXO_N_PIN                      (14)
#define HFXO_N_PORT                     (gpioPortB)

#define PANIC_BUTTON_PIN                (10)
#define PANIC_BUTTON_PORT               (gpioPortC)

#define LED_RED_PIN                     (12)
#define LED_RED_PORT                    (gpioPortC)

#define LED_GREEN_PIN                   (11)
#define LED_GREEN_PORT                  (gpioPortC)

#define ISL9230_PG_PIN                  (8)
#define ISL9230_PG_PORT                 (gpioPortE)

#define ISL9230_CHG_PIN                 (9)
#define ISL9230_CHG_PORT                (gpioPortE)

#define ISL9230_CHGEN_PIN               (15)
#define ISL9230_CHGEN_PORT              (gpioPortE)

#define ISL9230_CONT_PIN                (3)
#define ISL9230_CONT_PORT               (gpioPortF)

#define ISL9230_MODE_PIN                (4)
#define ISL9230_MODE_PORT               (gpioPortF)

#define ISL9230_AC_USB_PIN              (5)
#define ISL9230_AC_USB_PORT             (gpioPortF)



/** @endcond */

#endif /* GPIO_DEFINES_H_ */
