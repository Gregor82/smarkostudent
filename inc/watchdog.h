/**
 * @file			watchdog.h
 * @date			11.08.2017
 *	
 * @details
 */

#ifndef WATCHDOG_H_
#define WATCHDOG_H_

#include <stdbool.h>

#include "em_wdog.h"
#include "userTimer.h"

/**
 * @brief Initializes the watchdog with a timeout of 2s.
 */
void watchdog_init(void);

/**
 * @brief Starts the watchdog.
 *
 * @attention The watchdog must be initialized first.
 */
void watchdog_start(void);


void watchdog_stop(void);

/**
 * @brief Feed for the watchdog.
 *
 * @attention The watchdog must be initialized first.
 */
void watchdog_feed(void);

/**
 * @brief Resets the MCU.
 *
 * The user can check if the MCU is reset by the watchdog when user
 * calls the following function at the beginning of the main function:
 *
 * if (resetCause & RMU_RSTCAUSE_WDOGRST)
 * {
 *  // reset by watchdog
 * }
 * else
 * {
 *  // the reset cause is a different source than the watchdog
 * }
 *
 * @attention The watchdog must be initialized first.
 */
void watchdog_reset(void);

#endif /* WATCHDOG_H_ */
