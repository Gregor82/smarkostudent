/**
 * @file			gpio.h
 * @author			grli
 * @date			11.12.2017
 *	
 * @details
 */

#ifndef GPIO_H_
#define GPIO_H_

#include <stdbool.h>

#include "em_cmu.h"
#include "em_gpio.h"
#include "gpiointerrupt.h"

#include "inc/gpioDefines.h"

/**
 * @brief Definition of the LEDs colours.
 */
typedef enum
{
	LED_OFF = 		0,	//!< Turn off LEDs.
	LED_RED = 		1,	//!< Turn on red LED.
	LED_GREEN = 	2,	//!< Turn on green LED.
	LED_YELLOW =	3,	//!< Turn on yellow LED (red and green LED together).
}LEDColour_t;

/**
 * @brief Initializes the GPIOs that are not directly initialized in there drivers.
 *
 * @param charging
 * @param panic
 */
void gpio_init(bool *charging, bool *panic);

/**
 * @brief Set the LED.
 *
 * @param colour Choose the colour to illuminate.
 */
void gpio_setLED(LEDColour_t colour);

void gpio_setBQ51050B_TS(void);

void gpio_clearBQ51050B_TS(void);

bool gpio_getPanic(void);

bool gpio_getCharging(void);

#endif /* GPIO_H_ */
