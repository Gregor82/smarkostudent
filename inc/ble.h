/**
 * @file			ble.h
 * @date			21.03.2017
 *	
 * @details
 */

#ifndef BLE_H_
#define BLE_H_


//#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "em_usart.h"

#include "uartdrv.h"
#include "inc/userTimer.h"


#define BLE_RESET_PIN                   (6)
#define BLE_RESET_PORT                  (gpioPortD)

#define BLE_USART_TX_PIN                (10)
#define BLE_USART_TX_PORT               (gpioPortE)

#define BLE_USART_RX_PIN                (11)
#define BLE_USART_RX_PORT               (gpioPortE)

#define BLE_USART_CTS_PIN               (12)
#define BLE_USART_CTS_PORT              (gpioPortE)

#define BLE_USART_RTS_PIN               (13)
#define BLE_USART_RTS_PORT              (gpioPortE)


#define RX_BUFFERSIZE 			255
#define TX_BUFFERSIZE 			32

/**
 * @brief Return codes for the BLE.
 */
typedef enum {
	BLE_SUCCESS = 0,			//!< Transfer completed successfully.
	BLE_FAIL = -1,          	//!< BLE error.
	BLE_UART_ERR = -4,       	//!< UART error.
	BLE_NO_DEBUG = -97,			//!< BLE not in debug mode.
	BLE_NOT_CONNECTED = -98,	//!< BLE is not connected with a phone or tablet.
	BLE_NO_INIT = -99       	//!< BLE not initialized.
}BLE_RETURN;

/**
 * @brief BLE commands to update the App.
 */
typedef enum
{
	SET_BATTERY_SOC		= 0x01,		//!< Write all battery level to the App.
	SET_BATTERY_VOLTAGE	= 0x02,		//!< Write all battery voltage to the App.
	SET_BATTERY_CURRENT	= 0x03,		//!< Write all battery current to the App.
	SET_BATTERY_TEMP	= 0x04,		//!< Write all battery temperature to the App.

	SET_PULSE_DATA 		= 0x05,		//!< Write the HRM values to the App.
	SET_SPO2_DATA 		= 0x06,		//!< Write the SpO2 values to the App.

	SET_BODY_TEMP		= 0x07,		//!< Write the skin temperature to the App.
	SET_ENV_TEMP		= 0x08,		//!< Write the environment temperature to the App.
	SET_ENV_HUMIDITY	= 0x09,		//!< Write the humidity to the App.

//	SET_GYRO_X 			= 0x0A,		//!< Write the gyro x-axis value to the App.
//	SET_GYRO_Y 			= 0x0B,		//!< Write the gyro y-axis value to the App.
//	SET_GYRO_Z 			= 0x0C,		//!< Write the gyro z-axis value to the App.
//
//	SET_ACCEL_X			= 0x0D,		//!< Write the acceleration x-axis value to the App.
//	SET_ACCEL_Y 		= 0x0E,		//!< Write the acceleration y-axis value to the App.
//	SET_ACCEL_Z 		= 0x0F,		//!< Write the acceleration z-axis value to the App.

	SET_HAS_SKIN_CONTACT = 0x10,	//!< Write the skin contact to the App.

	SET_GYRO			= 0x1A,		//!< Write the acceleration x-axis value to the App.
	SET_GYRO_TIME		= 0x1B,
	SET_GYRO_LIMIT		= 0x1C,
	SET_ACCEL			= 0x1D,		//!< Write the acceleration x-axis value to the App.
	SET_ACCEL_TIME		= 0x1E,
	SET_ACCEL_LIMIT		= 0x1F,
	SET_MPU9250_INT		= 0x20,
	SET_MAG				= 0x21,
	SET_MAG_TIME		= 0x22,
	SET_MAG_LIMIT		= 0x23,

	GET_CONNECTION		= 0xF0,		//!< Get the BLE connection information.
	GET_DEBUG_MODE		= 0xF1,		//!< Get the debug mode enable or disable.
	GET_ERROR_STATUS_BYTE = 0xE2,
	GET_RESET			= 0xFF,		//!< Resets the MCU.
}BLE_CMD;

typedef void (*callbackUART_t)(uint8_t *data, uint16_t offset);

//extern void diasbleAllSensors(void);
//extern void startAllSensors(void);
//extern void changeGyroTime(uint16_t time);
//extern void changeAccelTime(uint16_t time);
//extern void changeMagTime(uint16_t time);


/**
 *
 * @brief Initializes the BLE reset pin.
 *
 * @attention Call this function before the ble_init(bool *connected, bool *debug)
 * is called.
 *
 * @param reset True initializes the BLE RESET Pin and resets the BLE module.
 * False initializes the BLE RESET Pin but it does  not resets the BLE module.
 *
 * @returnBLE_RETURN
 * @retval BLE_SUCCESS Transfer completed successfully.
 */
BLE_RETURN ble_reset(bool reset);

/**
 * @brief Initializes the BLE module.
 *
 * @attention When an update is running all other functions should be stopped to
 * not interrupt the update process.
 *
 * @param callback
 * 
 * @return BLE_RETURN
 * @retval BLE_SUCCESS Transfer completed successfully.
 */
BLE_RETURN ble_init(callbackUART_t callback);

/**
 * @brief Sends specific sensor data to the BLE module.
 *
 * @param cmd
 * @param data
 *
 * @return BLE_RETURN
 * @retval BLE_SUCCESS Transfer completed successfully.
 * @retval BLE_NO_INIT BLE not initialized.
 */
//BLE_RETURN ble_sendCmd(BLE_CMD cmd, uint16_t data);
BLE_RETURN ble_sendCmd(BLE_CMD cmd, int16_t *data, uint8_t len);

BLE_RETURN ble_sendIMUCmd(BLE_CMD cmd, uint32_t *time, float *data, uint8_t len);

BLE_RETURN ble_returnUpdateCmd(uint8_t *cmd, int16_t ret);

#endif /* BLE_H_ */
