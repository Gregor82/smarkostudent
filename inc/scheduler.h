/**
 * @file 			schedulerd.h
 *
 * @createdBy		grli
 * @dateCreated		27.06.2019
 * $Revision: 29 $
 * $LastChangedBy: grli $
 * $LastChangedDate: 2019-06-27 11:06:20 +0200 (Thu, 27 Jun 2019) $
 *
 *
**/
#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_

#include <stdint.h>

#define MAXTASK 16

typedef void (*funcp)(void);

int scheduler_init(void);

int scheduler_addTask(funcp function, uint32_t delaytime);

void scheduler_removeTask(funcp function);

void scheduler_interruptHandler(void);

#endif /* _SCHEDULER_H_ */


